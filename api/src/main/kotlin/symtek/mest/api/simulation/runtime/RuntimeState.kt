package symtek.mest.api.simulation.runtime

import com.sun.org.apache.xpath.internal.operations.Bool
import symtek.mest.api.common.implementation
import java.util.*

enum class State {
    /**
     * Describes a new simulation that has not been executed.
     */
    NEW {
        override fun transitionTargets(): List<State> = listOf(RUNNING)
    },
    /**
     * Describes a currently running simulation.
     */
    RUNNING {
        override fun transitionTargets(): List<State> = listOf(FAILED, PAUSED, STOPPED)
    },
    /**
     * Describes a currently stopped simulation that has crashed.
     */
    FAILED {
        override fun transitionTargets(): List<State> = listOf()
    },
    /**
     * Describes a paused simulation that can be resumed.
     */
    PAUSED {
        override fun transitionTargets(): List<State> = listOf(RUNNING, STOPPED)
    },
    /**
     * Describes a stopped simulation that can not be resumed.
     */
    STOPPED {
        override fun transitionTargets(): List<State> = listOf()
    };

    abstract fun transitionTargets(): List<State>
    fun isFinal(): Boolean = transitionTargets().isEmpty()
    fun isChangePossible(target: State) : Boolean = target in transitionTargets()
}

interface StateMachineInterface {
    val id: String
    var state : State
}

class StateMachineBaseImpl : StateMachineInterface {
    override val id: String = UUID.randomUUID().toString()
    override var state : State = State.NEW
}

class StateMachine: StateMachineInterface by implementation() {
    fun changeTo(state: State) = when(this.state.isChangePossible(state)) {
        true -> this.state = state
        false -> throw IllegalArgumentException("The given state change from ${this.state} to ${state} is not possible.")
    }
    fun dryChangeTo(state: State) {
        if (!this.state.isChangePossible(state)) {
            throw IllegalArgumentException("The given state change from ${this.state} to ${state} is not possible.")
        }
    }

}

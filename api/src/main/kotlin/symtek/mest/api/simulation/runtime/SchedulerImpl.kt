package symtek.mest.api.simulation.runtime

import kotlinx.coroutines.*
import symtek.mest.api.simulation.Simulation
import java.util.*

class SchedulerImpl(args: SchedulerInterface.Arguments) : SchedulerInterface {
    override val simulation: Simulation = args.simulation
    override val schedulerConfiguration: SchedulerConfiguration = args.schedulerConfiguration

    override val id: String = UUID.randomUUID().toString()
    override val stateMachine = StateMachine()

    val schedulerCache = SchedulerCache(simulation)
    val schedulerRunner = SchedulerRunner(simulation, schedulerCache, schedulerConfiguration)

    var runnerCoroutine: Job? = null

    // steps
    var singleStep: Boolean = false

    val stopReasons : MutableList<Condition> = mutableListOf()

    override fun run() {
        stateMachine.dryChangeTo(State.RUNNING)
        stopReasons.clear()
        singleStep = false
        stateMachine.changeTo(State.RUNNING)
        runnerCoroutine = GlobalScope.launch {
            runLoop(this)
        }
    }

    private fun runLoop(scope: CoroutineScope) {
        while (scope.isActive) { // cancellable computation loop
            val matchingConditions = schedulerConfiguration.conditions.filter {
                it.isMatching(simulation, this)
            }.toList()

            if (matchingConditions.isEmpty()) {
                if (singleStep) {
                    schedulerRunner.executeStep()
                    singleStep = false
                    scope.cancel()
                    stateMachine.changeTo(State.PAUSED)
                } else {
                    schedulerRunner.executeStep()
                }
            } else {
                stopReasons.addAll(matchingConditions)
                scope.cancel()
                singleStep = false
                stateMachine.changeTo(State.STOPPED)
            }
        }
    }

    override fun pause() {
        stateMachine.dryChangeTo(State.PAUSED)
        runBlocking {
            runnerCoroutine?.cancelAndJoin()
        }
        stateMachine.changeTo(State.PAUSED)
    }

    override fun resume() {
        stopReasons.clear()
        stateMachine.changeTo(State.RUNNING)
        singleStep = false
        runnerCoroutine = GlobalScope.launch {
            runLoop(this)
        }
    }

    override fun step() {
        stopReasons.clear()
        stateMachine.dryChangeTo(State.RUNNING)
        singleStep = true
        stateMachine.changeTo(State.RUNNING)
        runnerCoroutine = GlobalScope.launch {
            runLoop(this)
        }
    }

    override fun stop() {
        stateMachine.dryChangeTo(State.STOPPED)
        runBlocking {
            runnerCoroutine?.cancelAndJoin()
        }
        stopReasons.clear()
        singleStep = false
        stateMachine.changeTo(State.STOPPED)
    }

    override fun terminate() {
        stateMachine.dryChangeTo(State.STOPPED)
        singleStep = false
        System.err.println("Termination is currently not supported because Coroutines have to be cooperative... Stopping gracefully instead.")
        stop()
    }
}

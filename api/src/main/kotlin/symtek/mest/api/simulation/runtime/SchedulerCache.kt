package symtek.mest.api.simulation.runtime

import symtek.mest.api.component.*
import symtek.mest.api.simulation.Simulation
import java.time.Duration

/**
 * The [SchedulerCache] will hold [Map]s and [Set]s that can be consumed by other classes. It will be built from the given [simulation].
 * It will provide easy access to common tasks like retrieve a list of ports of a given type.
 *
 * The cache will be built once and will not be updated.
 */
class SchedulerCache(val simulation: Simulation) {

    /**
     * Contains information about the available [CommonPort]s for a component.
     */
    data class AvailablePortTypesForComponent(
        var hasInputPort: Boolean = false,
        var hasDiscreteOutputPort: Boolean = false,
        var hasContinuousOutputPort: Boolean = false,
        var hasEventInputPort: Boolean = false,
        var hasEventOutputPort: Boolean = false
    )

    /**
     * Contains information about the type of the link (what is connected with it? i.e. ContinuousOutputPort <-> InputPort would result in "Continuous"
     */
    data class ComponentLinkInformation(
        var hasDiscreteLink: Boolean = false,
        var hasContinuousLink: Boolean = false,
        var hasEventLink: Boolean = false
    )

    /**
     * Enum describing the types a link can have (what is connected with it?)
     */
    enum class LinkType{
        Continuous,
        Discrete,
        Event
    }

    /**
     * Enum of the given types of ports.
     */
    enum class PortType {
        ContinuousOutputPort,
        DiscreteOutputPort,
        InputPort,
        EventOutputPort,
        EventInputPort
    }

    /**
     * Contains the available [PortType] for every component.
     */
    val componentToAvailablePortTypesMap =
        HashMap<Component, AvailablePortTypesForComponent>()
    /**
     * Contains all components that have at least one of the given [PortType]
     */
    val availablePortTypeToComponentsMap =
        HashMap<PortType, MutableSet<Component>>()

    /**
     * Contains a list of connected ports.
     */
    val portToLinkedPortsMap =
        HashMap<CommonPort<*>, MutableSet<CommonPort<*>>>()

    /**
     * Contains the information about Components (key of the map) that do publish data to other Components (key of the nested map) and how they do it [ComponentLinkInformation] (value of the nested map).
     */
    val outputComponentToLinkedInputComponentsMap =
        HashMap<Component, HashMap<Component, ComponentLinkInformation>>()
    /**
     * Contains the information about Components (key of the map) that do read data to from other Components (key of the nested map) and how they do it [ComponentLinkInformation] (value of the nested map).
     */
    val inputComponentToLinkedOutputComponentsMap =
        HashMap<Component, HashMap<Component, ComponentLinkInformation>>()

    /**
     * Contains a list of all ports for a given [PortType].
     */
    val portTypeToPortsMap: MutableMap<PortType, MutableSet<CommonPort<*>>> = mutableMapOf()

    // special caches

    /**
     * Contains a set of components that will read the continuous output port value of a component.
     *
     * Keep in mind that only connected components will be available, because unconnected will never be requested.
     */
    val runOnRequestComponentWithContinuousOutputPortToLinkedComponentsMap =
        HashMap<Component, MutableSet<Component>>()

    init {
        fillComponentToPortTypesMap()
        fillAvailablePortTypeToComponentsMap()
        fillPortToLinkedPortsMap()
        fillComponentToLinkedComponentsMap()
        fillRunOnRequestComponentWithContinuousOutputPortToLinkedComponentsMap()
        fillPortTypeToPortsMap()
    }

    private fun fillComponentToPortTypesMap() {
        componentToAvailablePortTypesMap.clear()
        simulation.components.forEach {
            val component = it.componentInstance
            val availablePortTypesForComponent = createAvailablePortTypesForComponentObject(component)
            componentToAvailablePortTypesMap[component] = availablePortTypesForComponent
        }
    }

    private fun fillAvailablePortTypeToComponentsMap() {
        availablePortTypeToComponentsMap.clear()

        PortType.values().forEach {
            availablePortTypeToComponentsMap[it] = mutableSetOf()
        }

        simulation.components.forEach {
            val component = it.componentInstance
            val availablePortTypesForComponent = createAvailablePortTypesForComponentObject(component)
            with(availablePortTypesForComponent) {
                if (hasContinuousOutputPort) {
                    availablePortTypeToComponentsMap[PortType.ContinuousOutputPort]!!.add(component)
                }
                if (hasDiscreteOutputPort) {
                    availablePortTypeToComponentsMap[PortType.DiscreteOutputPort]!!.add(component)
                }
                if (hasInputPort) {
                    availablePortTypeToComponentsMap[PortType.InputPort]!!.add(component)
                }
                if (hasEventOutputPort) {
                    availablePortTypeToComponentsMap[PortType.EventOutputPort]!!.add(component)
                }
                if (hasEventInputPort) {
                    availablePortTypeToComponentsMap[PortType.EventInputPort]!!.add(component)
                }
            }
        }
    }

    private fun fillPortToLinkedPortsMap() {
        portToLinkedPortsMap.clear()
        // just fill the map
        simulation.components.forEach {
            val component = it.componentInstance
            component.ports.forEach {port ->
                val linkedPorts = mutableSetOf<CommonPort<*>>()
                portToLinkedPortsMap[port as CommonPort<*>] = linkedPorts
            }
        }
        // update list with link data (bidirectional)
        simulation.links.forEach {
            val link = it.linkInstance
            portToLinkedPortsMap[link.portA]!!.add(link.portB)
            portToLinkedPortsMap[link.portB]!!.add(link.portA)
        }
    }

    private fun fillComponentToLinkedComponentsMap() {
        outputComponentToLinkedInputComponentsMap.clear()
        inputComponentToLinkedOutputComponentsMap.clear()

        // build the base for the map
        simulation.components.forEach {
            outputComponentToLinkedInputComponentsMap[it.componentInstance] = HashMap()
            inputComponentToLinkedOutputComponentsMap[it.componentInstance] = HashMap()
        }
        // add the information about links
        simulation.links.forEach {
            val link = it.linkInstance
            val outputComponent = getOutputComponentForLink(link)
            val inputComponent = getInputComponentForLink(link)

            val linkType = getLinkTypeForLink(link)

            addComponentLinkInformation(inputComponent, outputComponent, linkType)
        }
    }



    private fun fillRunOnRequestComponentWithContinuousOutputPortToLinkedComponentsMap() {
        runOnRequestComponentWithContinuousOutputPortToLinkedComponentsMap.clear()

        simulation.components.forEach {
            val component = it.componentInstance
            if (component.configuration.continuousBehaviour != ContinuousBehaviour.RunOnRequest) {
                return@forEach
            }
            if (!componentToAvailablePortTypesMap[component]!!.hasContinuousOutputPort) {
                return@forEach
            }
            runOnRequestComponentWithContinuousOutputPortToLinkedComponentsMap.putIfAbsent(component, mutableSetOf())
            outputComponentToLinkedInputComponentsMap[component]!!.filter { it.value.hasContinuousLink }.forEach {
                runOnRequestComponentWithContinuousOutputPortToLinkedComponentsMap[component]!!.add(it.key)
            }
        }
    }

    private fun fillPortTypeToPortsMap() {
        portTypeToPortsMap.clear()
        PortType.values().forEach { portTypeToPortsMap[it] = mutableSetOf() }

        simulation.components.forEach {
            val component = it.componentInstance
            component.ports.forEach { port ->
                when (port) {
                    is ContinuousOutputPort<*> -> portTypeToPortsMap[PortType.ContinuousOutputPort]!!.add(port)
                    is DiscreteOutputPort<*> -> portTypeToPortsMap[PortType.DiscreteOutputPort]!!.add(port)
                    is InputPort<*> -> portTypeToPortsMap[PortType.InputPort]!!.add(port)
                    is EventOutputPort<*> -> portTypeToPortsMap[PortType.EventOutputPort]!!.add(port)
                    is EventInputPort<*> -> portTypeToPortsMap[PortType.EventInputPort]!!.add(port)
                }
            }
        }
    }

    private fun addComponentLinkInformation(inputComponent: Component, outputComponent: Component, linkType: LinkType) {
        createOrUpdateComponentLink(inputComponentToLinkedOutputComponentsMap, inputComponent, outputComponent, linkType)
        createOrUpdateComponentLink(outputComponentToLinkedInputComponentsMap, outputComponent, inputComponent, linkType)
    }

    private fun createOrUpdateComponentLink(componentToLinkedComponentsMap: HashMap<Component, HashMap<Component, ComponentLinkInformation>>, componentA: Component, componentB: Component, linkType: LinkType) {
        val linkedComponentsMap = componentToLinkedComponentsMap[componentA]!!
        linkedComponentsMap.putIfAbsent(componentB, ComponentLinkInformation()) // add default if not available yet
        val componentLinkInformation = linkedComponentsMap[componentB]!!
        when (linkType) {
            LinkType.Continuous -> { componentLinkInformation.hasContinuousLink = true }
            LinkType.Discrete -> { componentLinkInformation.hasDiscreteLink = true }
            LinkType.Event -> { componentLinkInformation.hasEventLink = true }
        }
    }

    private fun getOutputComponentForLink(link: Link) : Component {
        return if (link.portA is GeneralOutputPort) {
            link.portA.component
        } else if (link.portB is GeneralOutputPort)  {
            link.portB.component
        } else {
            throw IllegalArgumentException("No input port found in link.")
        }
    }
    private fun getInputComponentForLink(link: Link) : Component {
        return if (link.portA is GeneralInputPort) {
            link.portA.component
        } else if (link.portB is GeneralInputPort)  {
            link.portB.component
        } else {
            throw IllegalArgumentException("No input port found in link.")
        }
    }

    private fun getLinkTypeForLink(link: Link): LinkType {
        val outputPort = if (link.portA is GeneralOutputPort) link.portA else link.portB
        return when (outputPort) {
            is ContinuousOutputPort<*> -> LinkType.Continuous
            is DiscreteOutputPort<*> -> LinkType.Discrete
            is EventOutputPort -> LinkType.Event
            else -> throw IllegalArgumentException("The given Link contains an unknown output port type '${outputPort::class.qualifiedName}'")
        }
    }

    private inline fun <reified T: CommonPort<*>>hasPortOfType(component: Component): Boolean {
        component.ports.find { it is T } ?: return false
        return true
    }

    private fun createAvailablePortTypesForComponentObject(component: Component): AvailablePortTypesForComponent {
        val availablePortTypesForComponent = AvailablePortTypesForComponent()
        availablePortTypesForComponent.hasContinuousOutputPort = hasPortOfType<ContinuousOutputPort<*>>(component)
        availablePortTypesForComponent.hasDiscreteOutputPort = hasPortOfType<DiscreteOutputPort<*>>(component)
        availablePortTypesForComponent.hasInputPort = hasPortOfType<InputPort<*>>(component)
        availablePortTypesForComponent.hasEventOutputPort = hasPortOfType<EventOutputPort<*>>(component)
        availablePortTypesForComponent.hasEventInputPort= hasPortOfType<EventInputPort<*>>(component)
        return availablePortTypesForComponent
    }
}

/**
 * The [StepCache] will hold [Set]s that can be used for the [Scheduler] in order to know, which [Component]'s logic has to be executed in this step.
 *
 * The cache can be updated by calling [updateCache].
 */
class StepCache(val simulation: Simulation, val schedulerCache: SchedulerCache, val componentToNextExecutionTime: MutableMap<Component, Duration?>) {

    val tickLogicsToBeExecuted: MutableSet<Component> = mutableSetOf()
    val eventHandlerLogicsToBeExecuted: MutableSet<EventInputPort<*>> = mutableSetOf()
    val outputRequestLogicsToBeExecuted: MutableSet<Component> = mutableSetOf()

    init {
        updateCache()
    }

    /**
     * Update the step cache for the current step.
     */
    fun updateCache() {
        tickLogicsToBeExecuted.clear()
        eventHandlerLogicsToBeExecuted.clear()
        outputRequestLogicsToBeExecuted.clear()

        findTickLogicsToBeExecuted()
        findEventHandlerLogicsToBeExecuted()
        findOutputRequestLogicsToBeExecuted()
    }

    /**
     * Returns [true] if the given [component] was executed somehow (tick logic, output request logic or event handler logic).
     */
    fun wasExecuted(component: Component): Boolean {
        return when {
            wasTickLogicExecuted(component) -> true
            wasOutputRequestLogicExecuted(component) -> true
            wasEventHandlerLogicExecuted(component) -> true
            else -> false
        }
    }

    /**
     * Returns [true] if the Component's tick logic was executed in this step.
     */
    fun wasTickLogicExecuted(component: Component): Boolean = tickLogicsToBeExecuted.contains(component)
    /**
     * Returns [true] if the Component's output request logic was executed in this step.
     */
    fun wasOutputRequestLogicExecuted(component: Component): Boolean = outputRequestLogicsToBeExecuted.contains(component)
    /**
     * Returns [true] if the Component's event handler logic was executed in this step.
     */
    fun wasEventHandlerLogicExecuted(component: Component): Boolean = eventHandlerLogicsToBeExecuted.count { it.component == component } > 0

    private fun findTickLogicsToBeExecuted() {
        componentToNextExecutionTime.forEach { (component, nextExecutionTime) ->
            if (simulation.environment.timestamp.currentTime == nextExecutionTime) {
                tickLogicsToBeExecuted.add(component)
            }
        }
    }

    private fun findEventHandlerLogicsToBeExecuted() {
        schedulerCache.portTypeToPortsMap[SchedulerCache.PortType.EventInputPort]!!.forEach {
            it as EventInputPort
            val component = it.component
            val nextExecutionTime = componentToNextExecutionTime[component] ?: return@forEach
            if (simulation.environment.timestamp.currentTime == nextExecutionTime && it.events.isNotEmpty()) {
                eventHandlerLogicsToBeExecuted.add(it)
            }
        }
    }

    private fun findOutputRequestLogicsToBeExecuted() {
        schedulerCache.availablePortTypeToComponentsMap[SchedulerCache.PortType.ContinuousOutputPort]!!.forEach { component ->
            when (component.configuration.continuousBehaviour) {
                ContinuousBehaviour.RunAlways -> outputRequestLogicsToBeExecuted.add(component)
                ContinuousBehaviour.RunOnRequest -> {
                    if(willNeighbourBeExecuted(component)) outputRequestLogicsToBeExecuted.add(component)
                }
            }
        }
    }

    private fun willNeighbourBeExecuted(publishComponent: Component): Boolean {
        val readerComponents =
            schedulerCache.runOnRequestComponentWithContinuousOutputPortToLinkedComponentsMap[publishComponent]
                ?: return false
        if (readerComponents.count { readComponent ->
                when {
                    readComponent === publishComponent -> true
                    outputRequestMayBeExecuted(readComponent) -> true
                    tickLogicsToBeExecuted.contains(readComponent) -> true
                    eventHandlerLogicsToBeExecuted.find { it.component == readComponent } != null -> true
                    else -> false
                }
            } > 0) {
            return true
        }
        return false
    }

    private fun outputRequestMayBeExecuted(component: Component): Boolean {
        return (schedulerCache.componentToAvailablePortTypesMap[component]!!.hasContinuousOutputPort)
    }

}
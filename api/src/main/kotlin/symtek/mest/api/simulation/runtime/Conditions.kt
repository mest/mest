package symtek.mest.api.simulation.runtime

import symtek.mest.api.simulation.Simulation
import java.time.Duration

abstract class Condition() {
    abstract fun isMatching(simulation: Simulation, scheduler: SchedulerInterface) : Boolean
    abstract override fun toString(): String
}

class StepCondition(val steps: Long) : Condition() {
    override fun isMatching(simulation: Simulation, scheduler: SchedulerInterface) : Boolean {
        return simulation.environment.timestamp.currentTick >= steps
    }
    override fun toString(): String = "When current tick is greater or equal to $steps"
}

class TimeCondition(val time: Duration) : Condition() {
    override fun isMatching(simulation: Simulation, scheduler: SchedulerInterface) : Boolean {
        return simulation.environment.timestamp.currentTime >= time
    }
    override fun toString(): String = "When current tick is greater or equal to $time"
}
package symtek.mest.api.simulation.runtime

import symtek.mest.api.common.copy
import symtek.mest.api.component.*
import symtek.mest.api.simulation.Simulation
import symtek.mest.api.simulation.Timestamp
import java.time.Duration
import java.util.*

/**
 * The [SchedulerRunner] will perform the execution of a step for a [Scheduler].
 */
class SchedulerRunner(
    val simulation: Simulation,
    val schedulerCache: SchedulerCache,
    val schedulerConfiguration: SchedulerConfiguration
) {
    val componentToNextExecutionTime: MutableMap<Component, Duration?> = mutableMapOf()
    val stepCache =
        StepCache(simulation, schedulerCache, componentToNextExecutionTime)

    init {
        simulation.components.forEach {
            val component = it.componentInstance
            val duration: Duration? = if (component.configuration.periodDuration != null) {
                Duration.ZERO
            } else null
            componentToNextExecutionTime[component] = duration
        }
        stepCache.updateCache()
    }

    fun executeStep() {
        // execute the output request values and commit their new output value in order for the values to be used in other logics.
        executeOutputRequestLogics()
        copyOutputValuesToInputPorts()

        // execute event handlers (consume events before the tick will start)
        executeEventHandlerLogics()

        // execute tick logics
        executeTickLogics()

        // commit the output values again (now including changes done in the tick logic and/or event handler logic) and publish events to be consumed in a later step.
        copyOutputValuesToInputPorts()
        copyEventsToEventInputPorts()

        calculateNextExecutionTimes()
        incrementStep()
        stepCache.updateCache()
    }

    private fun executeOutputRequestLogics() {
        stepCache.outputRequestLogicsToBeExecuted.forEach {
            it.outputTriggered()
        }
    }

    private fun executeEventHandlerLogics() {
        stepCache.eventHandlerLogicsToBeExecuted.forEach { port ->
            port.events.forEach { event ->
                port.eventHandler.invoke(event)
            }
        }
    }

    private fun executeTickLogics() {
        stepCache.tickLogicsToBeExecuted.forEach {
            it.tickTriggered()
        }
    }

    /**
     * Copy output values of [AbstractOutputPort]s to [InputPort]s.
     *
     * This method will start with the input port because the input port will have a list of values, one for each connected output port.
     * Therefor this list has to be cleared (or the correct index must be changed if a partial update would be done).
     * To overcome this, the every input port will be cleared and all the output values will be added again.
     */
    private fun copyOutputValuesToInputPorts() {
        schedulerCache.portTypeToPortsMap[SchedulerCache.PortType.InputPort]!!.forEach { inputPort ->
            inputPort as InputPort

            //TODO: test if this code can be optimized by using this logic. Check if this lookup + find is faster than a copy of the output values.
            // val foundComponent = schedulerCache.inputComponentToLinkedOutputComponentsMap[inputPort.component]!!.keys.find { stepCache.wasExecuted(it) }
            // if (foundComponent != null) {
            //     existing clear() + copy()
            // }

            inputPort.values.clear()
            schedulerCache.portToLinkedPortsMap[inputPort]!!.forEach { outputPort ->
                outputPort as AbstractOutputPort
                inputPort.values.add(copy(outputPort.value))
            }
        }
    }

    /**
     * Copy events from an [EventOutputPort] to connected [EventInputPort]s.
     */
    private fun copyEventsToEventInputPorts() {
        schedulerCache.portTypeToPortsMap[SchedulerCache.PortType.EventOutputPort]!!.forEach { eventOutputPort ->
            eventOutputPort as EventOutputPort
            schedulerCache.portToLinkedPortsMap[eventOutputPort]!!.forEach { eventInputPort ->
                eventInputPort as EventInputPort
                eventInputPort.events.addAll(eventOutputPort.events)
            }

            eventOutputPort.events.clear()
        }
    }

    /**
     * This method will increment the step. The next time will be calculated (next scheduled tick of Components or the default value of the configuration.
     */
    private fun incrementStep() {
        val timestamp = simulation.environment.timestamp as Timestamp
        timestamp.currentTick += 1
        val nextTimeOptional: Optional<Duration?> = componentToNextExecutionTime.values.stream()
            .filter { it != null }
            .min { d1: Duration?, d2: Duration? ->
                d1!!.compareTo(d2!!)
            }

        timestamp.currentTime = if (nextTimeOptional.isPresent) {
            nextTimeOptional.get()
        } else {
            timestamp.currentTime + schedulerConfiguration.defaultIncrementTime
        }
    }

    /**
     * This method will calculate the next execution time for all components.
     *
     * Scenarios:
     * * If a component was somehow executed (tick, output request or event handler) and
     *   * ... has no scheduled execution time but the period duration was set during the last step -> schedule the next execution to currentTime + periodDuration
     *   * ... has a scheduled execution time and the period duration was set to null during the last step -> remove the schedule for the next execution
     *   * ... has a scheduled execution time and the component's tick logic was executed in the last step -> schedule the next execution to currentTime + periodDuration
     */
    private fun calculateNextExecutionTimes() {
        val currentTime = simulation.environment.timestamp.currentTime
        simulation.components.forEach {
            val component = it.componentInstance
            if (stepCache.wasExecuted(component)) {
                val nextExecutionTime = componentToNextExecutionTime[component]
                val periodDuration = component.configuration.periodDuration

                if (nextExecutionTime == null) {
                    if (periodDuration != null) {
                        componentToNextExecutionTime[component] = currentTime + periodDuration
                    }
                } else {
                    if (periodDuration == null) {
                        componentToNextExecutionTime[component] = null
                    } else if (stepCache.wasTickLogicExecuted(component)) {
                        // only update next execution time, if the tick was executed.
                        componentToNextExecutionTime[component] = currentTime + periodDuration
                    }
                }

            }
        }
    }
}
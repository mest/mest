package symtek.mest.api.simulation

import java.time.Duration

/**
 * The [Environment] is the wrapper object for simulation related information for [symtek.mest.api.component.Component]s.
 *
 * It provides a read-only representation of data like the current timestamp.
 */
data class Environment(
    val timestamp: TimestampInterface = Timestamp(
        currentTick = 0,
        currentTime = Duration.ZERO
    )
)

interface TimestampInterface {
    val currentTick: Long
    val currentTime: Duration
}

data class Timestamp(override var currentTick: Long, override var currentTime: Duration) : TimestampInterface
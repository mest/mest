package symtek.mest.api.simulation

import symtek.mest.api.common.callNamed
import symtek.mest.api.component.CommonPort
import symtek.mest.api.component.Component
import symtek.mest.api.component.ComponentConfiguration
import java.util.*
import kotlin.reflect.KClass

/**
 * Represents a [ComponentCandidate], that can be used to create a [Component] instance for a [Simulation] via the [SimulationManager].
 * In addition it will hold the [ports] of the component.
 *
 * When a [ComponentCandidate] is created, the given [componentClass] will be used to create a new instance. The [name] will be stored as the configuration for this [ComponentCandidate].
 * In order to create a new instance (for a new [Simulation]), the method [createInstance] can be used.
 */
class ComponentCandidate<T : Component>(val componentClass: KClass<T>, var name: String, val id: String = UUID.randomUUID().toString()) {

    val ports: List<CommonPort<*>>
    val configuration: ComponentConfiguration
        get() {
            return initComponentInstance.configuration
        }
    val initComponentInstance: Component

    init {
        initComponentInstance = createInstance(id, Environment(), null)
        @Suppress("UNCHECKED_CAST")
        ports = initComponentInstance.ports.toList() as List<CommonPort<*>>
    }

    fun createInstance(environment: Environment, configuration: ComponentConfiguration?): Component {
        return createInstance(name, environment, configuration)
    }

    private fun createInstance(name: String, environment: Environment, configuration: ComponentConfiguration?): Component {
        try {
            val constructor = componentClass.constructors.find {
                it.parameters.find { it.name == "name" } ?: return@find false
                it.parameters.find { it.name == "environment" } ?: return@find false
                val configurationParam = it.parameters.find { it.name == "configuration" } ?: return@find false
                configurationParam.isOptional // only allow configuration to be optional
            } ?: throw InvalidComponentConstructorException(componentClass)

            val constructorArgMap = mutableMapOf<String, Any?>(
                "name" to name,
                "environment" to environment
            )
            if (configuration != null) {
                constructorArgMap["configuration"] = configuration
            }

            return constructor.callNamed(constructorArgMap)
        } catch (ex: NoSuchMethodException) {
            throw InvalidComponentConstructorException(componentClass)
        } catch (ex: SecurityException) {
            throw ex
        } catch (ex: InvalidComponentConstructorException) {
            throw ex
        } catch (ex: Exception) {
            throw ComponentInstantiationException(componentClass, ex)
        }
    }
}

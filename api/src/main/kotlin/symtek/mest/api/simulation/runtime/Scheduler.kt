package symtek.mest.api.simulation.runtime

import symtek.mest.api.common.implementation
import symtek.mest.api.simulation.Simulation
import java.time.Duration
import java.util.*


interface SchedulerInterface {
    data class Arguments(val simulation: Simulation, val schedulerConfiguration: SchedulerConfiguration)

    /**
     * ID of the given scheduler.
     */
    val id: String

    /**
     * Start the simulation
     */
    fun run()

    /**
     * Pause the running simulation
     */
    fun pause()

    /**
     * Resume a paused simulation
     */
    fun resume()

    /**
     * Step the simulation
     */
    fun step()

    /**
     * Stop the simulation gracefully (before the next step will be executed)
     */
    fun stop()

    /**
     * Terminate the simulation forcefully (killing a running simulation step, if needed)
     */
    fun terminate()

    val stateMachine: StateMachine

    val simulation: Simulation

    val schedulerConfiguration: SchedulerConfiguration

}

data class SchedulerConfiguration(
    val id: String = UUID.randomUUID().toString(),
    var defaultIncrementTime: Duration = Duration.ofSeconds(1),
    val conditions: List<Condition> = listOf()
)

class Scheduler(simulation: Simulation, schedulerConfiguration: SchedulerConfiguration) :
    SchedulerInterface by implementation(
        SchedulerInterface.Arguments(
            simulation,
            schedulerConfiguration
        )
    )
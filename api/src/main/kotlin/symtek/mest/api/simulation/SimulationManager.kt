package symtek.mest.api.simulation

import symtek.mest.api.common.copy
import symtek.mest.api.component.*
import symtek.mest.api.simulation.runtime.Scheduler
import symtek.mest.api.simulation.runtime.SchedulerConfiguration
import java.util.stream.Collectors
import kotlin.reflect.KClass
import kotlin.streams.toList

/**
 * The [SimulationManager] can be used to create a new [Simulation] that can be executed.
 *
 * The configuration of the [Simulation] will be done with the [ComponentCandidate]s inside the [SimulationManager].
 * Example: When two ports should be connected together, the [Link] will be established with the [ComponentCandidate]'s [ComponentCandidate.initComponentInstance] instance.
 *
 * When the method [createSimulation] is called, new [Component] instances for all [ComponentCandidate] will be created.
 * Previously created [Link]s from the [ComponentCandidate.initComponentInstance] will be reapplied to the newly created [Component] instances of the corresponding [ComponentCandidate]s.
 */
class SimulationManager {
    val componentCandidates = mutableListOf<ComponentCandidate<*>>()
    val links = mutableListOf<Link>()

    /**
     * Returns a set of [Link]s that are currently available for all [ComponentCandidate]s.
     */
    fun getLinkCandidates(): Set<Link> = componentCandidates.stream().map { it.initComponentInstance.links }.flatMap { it.stream() }.collect(Collectors.toSet())

    fun createSimulation(): Simulation {
        val environment: Environment = Environment()
        val candidateToInstance =
            componentCandidates.stream().map {
                ComponentCandidateToComponent(
                    it,
                    it.createInstance(environment, copy(it.configuration))
                )
            }.toList()

        candidateToInstance.forEach {
            validateInstance(it.componentCandidate, it.componentInstance)
        }
        val candidateLinks: Set<Link> = getLinkCandidates()

        // for each link, get the candidate instance + port. Then find the corresponding instance component and its port. Then create links with the instance ports.
        val linkCandidateToLinkInstanceList = candidateLinks.stream().map {
            // get candidate information for this link
            val candidatePortA = it.portA
            val candidatePortB = it.portB
            val candidateLinkComponentA = candidatePortA.component
            val candidateLinkComponentB = candidatePortB.component

            // find corresponding component instances
            val componentInstanceA = candidateToInstance.stream()
                .filter { it.componentCandidate.initComponentInstance == candidateLinkComponentA }
                .map { it.componentInstance }.findFirst().get()
            val componentInstanceB = candidateToInstance.stream()
                .filter { it.componentCandidate.initComponentInstance == candidateLinkComponentB }
                .map { it.componentInstance }.findFirst().get()

            // find corresponding port instances
            val portInstanceA =
                componentInstanceA.ports.stream().filter { it.name == candidatePortA.name }.findFirst().get()
            val portInstanceB =
                componentInstanceB.ports.stream().filter { it.name == candidatePortB.name }.findFirst().get()

            // create the link for the instance
            LinkCandidateToLink(
                linkCandidate = it, linkInstance = Link(
                    portInstanceA as CommonPort<*>,
                    portInstanceB as CommonPort<*>
                )
            )
        }.toList()

        // connect all instanceLinks
        linkCandidateToLinkInstanceList.forEach { connectLink(candidateToInstance.map { it.componentInstance }.toList(), it.linkInstance) }

        return Simulation(environment, candidateToInstance, linkCandidateToLinkInstanceList)
    }

    fun createSimulationScheduler(configuration: SchedulerConfiguration): Scheduler {
        val simulation = this.createSimulation()
        return Scheduler(simulation, configuration)
    }

    fun addComponentCandidate(componentCandidate: ComponentCandidate<*>) {
        if (componentCandidates.contains(componentCandidate)) throw IllegalArgumentException("The given ComponentCandidate is already in the Simulation")
        componentCandidates.add(componentCandidate)
    }
    fun removeComponentCandidate(componentCandidate: ComponentCandidate<*>) {
        componentCandidates.remove(componentCandidate)
    }

    fun connectLink(link: Link) {
        connectLink(componentCandidates.map { it.initComponentInstance }.toList(), link)
    }

    private fun connectLink(components: List<Component>, link: Link) {
        val componentA = link.portA.component
        val componentB = link.portB.component

        validateLinkConnectability(components, link)

        componentA.links.add(link)
        if (componentA != componentB) {
            componentB.links.add(link)
        }

        links.add(link)
    }

    fun validateLinkConnectability(link: Link) {
        validateLinkConnectability(componentCandidates.map { it.initComponentInstance }.toList(), link)
    }

    private fun validateLinkConnectability(components: List<Component>, link: Link) {
        val componentA = link.portA.component
        val componentB = link.portB.component

        if (!components.contains(componentA)) {
            throw IllegalArgumentException("Component A of the link is not part of this simulation.")
        }
        if (!components.contains(componentB)) {
            throw IllegalArgumentException("Component B of the link is not part of this simulation.")
        }

        if (componentA.links.contains(link) || componentB.links.contains(link)) {
            throw IllegalArgumentException("The given link is already established.")
        }
        if (!link.portA.checkConnectionCompatibilityWith(link.portB)) {
            throw IncompatiblePortException("Port ${link.portA} is not compatible with ${link.portB}")
        }
        if (!link.portB.checkConnectionCompatibilityWith(link.portA)) {
            throw IncompatiblePortException("Port ${link.portB} is not compatible with ${link.portA}")
        }
        if (!link.portA.checkTypeWith(link.portB)) {
            throw IncompatiblePortTypeException("")
        }
        if (!link.portB.checkTypeWith(link.portA)) {
            throw IncompatiblePortTypeException("")
        }

        validateMaxNumberOfConnectionsFor(link.portA, componentA)
        validateMaxNumberOfConnectionsFor(link.portB, componentB)
    }

    private fun validateMaxNumberOfConnectionsFor(port: CommonPort<*>, component: Component) {
        if (port !is InputPort) {
            return
        }
        if (port.maxNumberOfConnections <= 0) {
            return
        }
        val linksForPort = component.links.stream()
            .filter { it.portA == port || it.portB == port }.count()
        if (linksForPort >= port.maxNumberOfConnections) {
            throw ConnectionImpossible("")
        }
    }

    fun disconnectLink(link: Link) {
        val componentA = link.portA.component
        val componentB = link.portB.component

        if (!componentA.links.contains(link) || !componentB.links.contains(link)) {
            throw IllegalArgumentException("This link is currently not connected.")
        }

        componentA.links.remove(link)
        componentB.links.remove(link)

        links.remove(link)
    }

    private fun validateInstance(candidate: ComponentCandidate<*>, instance: Component) {
        if (candidate.initComponentInstance.configuration.periodDuration != instance.configuration.periodDuration)  {
            throw IncompleteComponentConstructionException(instance::class, Component::configuration.name)
        }
        if (candidate.initComponentInstance.configuration.continuousBehaviour != instance.configuration.continuousBehaviour) throw IncompleteComponentConstructionException(instance::class, Component::configuration.name)
        if (candidate.name != instance.name) throw IncompleteComponentConstructionException(instance::class, Component::name.name)
        if (candidate.initComponentInstance.environment != instance.environment) throw IncompleteComponentConstructionException(instance::class, Component::environment.name)
    }
}

/**
 * Data class that will store the relation between an [ComponentCandidate] and one of it's [Component] instances.
 */
data class ComponentCandidateToComponent(
    val componentCandidate: ComponentCandidate<*>,
    val componentInstance: Component
)

data class LinkCandidateToLink(
    val linkCandidate: Link,
    val linkInstance: Link
)

/**
 * The [Simulation] contains the [Environment] and a list of [Component] instances.
 *
 * The [Simulation] is meant to be executed via a [SimulationRuntime]. //TODO: see #7
 */
data class Simulation(val environment: Environment, val components: List<ComponentCandidateToComponent>, val links: List<LinkCandidateToLink>)

class InvalidComponentConstructorException(componentClass: KClass<*>) :
    Exception("The given component class '${componentClass.simpleName}' has no valid constructor.\nEvery component must provide the following constructor parameters:\n  - '${Component::name.name}': ${String::class.qualifiedName}\n  - '${Component::environment.name}': ${Environment::class.qualifiedName}\n  - '${Component::configuration.name}': ${ComponentConfiguration::class.qualifiedName} (can be defined as default paramter).")

class IncompleteComponentConstructionException(componentClass: KClass<*>, variableName: String) :
    Exception("The given component class '${componentClass.simpleName}' could only be constructed partially.\nThe validation for the variable '$variableName' failed.\nThis exception should only occur, if some of the main construcor arguments were not provided to the base Component class.")

class ComponentInstantiationException(componentClass: KClass<*>, exception: Exception) : Exception(
    "There was an error creating an instance of the component class '${componentClass.simpleName}'.\nPlease keep in mind, that you should never assume, that the constructor will only be called at the start of the simulation.\nPlease do never try talk to other components or talk with the environment.",
    exception
)


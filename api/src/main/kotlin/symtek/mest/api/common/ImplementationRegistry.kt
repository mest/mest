package symtek.mest.api.common

import symtek.mest.api.simulation.runtime.*
import kotlin.reflect.KClass

data class InstanceFactory<I : Any, T : I>(val implementationClass: KClass<T>, val factory: (argObject: Any?) -> T)

class ImplementationNotFoundException(cls: KClass<*>) : IllegalArgumentException("The given interface ${cls.simpleName} was not found in the registry.")

/**
 * The main purpose of the [ImplementationRegistry] is to help writing custom implementations for the internal logic (properties and/or functions).
 * The [ImplementationRegistry] can also be used to create Interceptor classes.
 *
 * **Warning:** Please be aware, that every implementation class will have its own state! This may be a problem if you want to change only the implementation of a subset of the properties and/or methods.
 *
 * Example (see the TestInterfaceUIImpl is using delegation to the default implementation):
 * ```
 * interface TestInterface {
 *     val myList: MutableList<String>
 *     fun addToList(elem: String)
 * }
 *
 * class TestInterfaceBaseImpl : TestInterface {
 *     override fun addToList(elem: String) {
 *         myList.add(elem)
 *     }
 *     override val myList = mutableListOf<String>()
 * }
 *
 * class TestInterfaceUIImpl : TestInterface by defaultImplementation() {
 *     override val myList = observableListOf<String>()
 *     init {
 *         myList.onChange { println("After adding...") }
 *     }
 * }
 *
 * class Test : TestInterface by implementation()
 *
 * fun main() {
 *     ImplementationRegistry.registerImplementation(TestInterface::class) { TestInterfaceBaseImpl() }
 *     val base = Test()
 *     ImplementationRegistry.registerImplementation(TestInterface::class) { TestInterfaceUIImpl() }
 *     val multiDelegation = Test()
 *
 *     println("Default:")
 *     base.addToList("default")
 *     println("Using multi delegation for single interface:")
 *     multiDelegation.addToList("multi")
 * }
 * ```
 * Here you may expect 'After adding...' to be printed after the `multiDelegation.addToList("multi")` call.
 * But this is not the case because when `multiDelegation.addToList("multi")` is called, the call will actually happen on an instance of the class TestInterfaceBaseImpl.
 * This instance has it's own implementation of `myList`.
 *
 * As you can see, **you should not use this concept to only exchange parts of an existing implementation**. If you want this, please refer to Interceptor classes mentioned below.
 *
 * **Sample 1: Exchange properties**
 * ```
 * // Here comes the API code
 * interface TestInterface {
 *     val myList: MutableList<String>
 * }
 *
 * class TestInterfaceBaseImpl : TestInterface {
 *     override val myList = mutableListOf<String>()
 * }
 *
 * class Test : TestInterface by implementation()
 *
 * // Here comes the UI code
 * class TestInterfaceUIImpl : TestInterface {
 *     override val myList = observableListOf<String>()
 *     init {
 *         myList.onChange { println("After adding...") }
 *     }
 * }
 *
 * fun main() {
 *     ImplementationRegistry.registerImplementation(TestInterface::class) { TestInterfaceBaseImpl() }
 *     val defaultImplementedInstance = Test()
 *     ImplementationRegistry.registerImplementation(TestInterface::class) { TestInterfaceUIImpl() }
 *     val exchangedImplementatedInstance = Test()
 *
 *     println("Now using default impl (should be no output):")
 *     defaultImplementedInstance.myList.add("default")
 *     println("Now using extended impl (there should be 'After adding...'")
 *     exchangedImplementatedInstance.myList.add("exchange")
 * }
 * ```
 * Will return
 * ```
 * Now using default impl (should be no output):
 * Now using extended impl (there should be 'After adding...'
 * After adding...
 * ```
 *
 * **Sample 2: Interceptor classes**
 *
 * Keep in mind that an interceptor can be built easily and that you are able to just override specific methods (see the interceptor class below and the use of `by baseImpl`).
 * ```
 * interface RunnerInterface {
 *   fun start()
 *   fun stop()
 * }
 *
 * class RunnerBaseImpl : RunnerInterface {
 *   override fun start() {
 *     println("start")
 *   }
 *   override fun stop() {
 *     println("stop")
 *   }
 * }
 *
 * class RunnerInterceptor(val baseImpl: RunnerInterface = defaultImplementation()) : RunnerInterface by baseImpl {
 *   override fun start() {
 *     println("before start")
 *     baseImpl.start()
 *   }
 * }
 *
 * class Runner : RunnerInterface by implementation()
 *
 * fun main() {
 *   ImplementationRegistry.registerImplementation(RunnerInterface::class) { RunnerBaseImpl() }
 *   val runner1 = Runner()
 *   ImplementationRegistry.registerImplementation(RunnerInterface::class) { RunnerInterceptor() }
 *   val runner2 = Runner()
 *   println("Default:")
 *   runner1.start()
 *   runner1.stop()
 *   println("Using interception:")
 *   runner2.start()
 *   runner2.stop()
 * }
 * ```
 * Will print:
 * ```
 * Default:
 * start
 * stop
 * Using interception:
 * before start
 * start
 * stop
 * ```
 *
 * **Sample 3: Constructor parameter**
 *
 * When an implementation requires constructor arguments, this can be solved in multiple ways. The best practice is to create a local data class inside the related interface.
 * This data class than be used to call by implementation(dataClassInstance) which can then be used to build the implementation class.
 *
 * ```
 * interface InterfaceWithRequiredParameter {
 *     data class Arguments(val world: String)
 *     fun hello()
 * }
 *
 * class InterfaceWithRequiredParameterBaseImpl(args: InterfaceWithRequiredParameter.Arguments) : InterfaceWithRequiredParameter {
 *     val world = args.world
 *     override fun hello() {
 *         println("Hello $world")
 *     }
 * }
 *
 * class InterfaceWithRequiredParameterOverrideImpl(args: InterfaceWithRequiredParameter.Arguments) : InterfaceWithRequiredParameter {
 *     val world = args.world
 *     override fun hello() {
 *         println("Hello $world override")
 *     }
 * }
 *
 * class RequiredParameter(args: InterfaceWithRequiredParameter.Arguments):  InterfaceWithRequiredParameter by implementation(args)
 *
 * fun main() {
 *     ImplementationRegistry.registerImplementation(InterfaceWithRequiredParameter::class) {
 *         InterfaceWithRequiredParameterBaseImpl(it as InterfaceWithRequiredParameter.Arguments)
 *     }
 *     val baseImpl = RequiredParameter(InterfaceWithRequiredParameter.Arguments("world"))
 *     ImplementationRegistry.registerImplementation(InterfaceWithRequiredParameter::class) {
 *         InterfaceWithRequiredParameterOverrideImpl(it as InterfaceWithRequiredParameter.Arguments)
 *     }
 *     val overrideImpl = RequiredParameter(InterfaceWithRequiredParameter.Arguments("world2"))
 *
 *     println("Default:")
 *     baseImpl.hello()
 *     println("Extended:")
 *     overrideImpl.hello()
 * }
 * ```
 * Will return:
 * ```
 * Default:
 * Hello world
 * Extended:
 * Hello world2 override
 * ```
 */
object ImplementationRegistry {

    /**
     * Will contain the default implementations used by the application.
     *
     * The key will represent the interface [KClass] and the value will represent the [KClass] of the implementation.
     */
    private val defaultImplementations: MutableMap<KClass<*>, InstanceFactory<*, *>> = mutableMapOf()

    /**
     * Registering the default implementations.
     */
    init {
        registerDefaultImplementation(SchedulerInterface::class) {
            SchedulerImpl(it as SchedulerInterface.Arguments)
        }
        registerDefaultImplementation(StateMachineInterface::class) {
            StateMachineBaseImpl()
        }
    }

    /**
     * Returns a [Map] of [KClass]s that are registered as implementations for [KClass]s.
     * The key will represent the interface [KClass] and the value will represent the [KClass] of the implementation.
     *
     * This property should not be used directly. Please see [implementation].
     */
    var implementations: MutableMap<KClass<*>, InstanceFactory<*, *>> = defaultImplementations.toMutableMap()
        private set

    /**
     * Helper method to receive the current implementation for the given interface [cls].
     * This function should not be used directly. Please see [implementation].
     */
    fun getImplementationForClass(cls: KClass<*>) = implementations[cls] ?: throw ImplementationNotFoundException(
        cls
    )

    /**
     * Helper method to receive the default implementation for the given interface [cls].
     * This function should not be used directly. Please see [defaultImplementation].
     */
    fun getDefaultImplementationForClass(cls: KClass<*>) = defaultImplementations[cls] ?: throw ImplementationNotFoundException(
        cls
    )

    /**
     * This method will register the [Implementation] factory method [block] as the new implementation generator of the given [Interface].
     *
     * Example: `ImplementationRegistry.registerImplementation(TestInterface::class) { TestInterfaceUIImpl>() }`
     */
    inline fun <reified Interface : Any, reified Implementation: Interface> registerImplementation(int: KClass<Interface>, noinline block: (Any?) -> Implementation) {
        if (Implementation::class == Interface::class) throw IllegalArgumentException("The interface and implementation classes are identical (${Interface::class.simpleName} == ${Implementation::class.simpleName})")
        if (!implementations.contains(Interface::class)) System.err.println("A custom implementation (${Implementation::class.simpleName}) for the unknown interface  (${Interface::class.simpleName}) will be registered. This implementation will never be used.")
        println("Custom  Registration: ${Implementation::class.simpleName} for ${Interface::class.simpleName}")
        implementations[int] =
            InstanceFactory<Interface, Implementation>(Implementation::class, block)
    }

    /**
     * This method will register a default [Implementation] for the [Interface].
     */
    private inline fun <reified Interface : Any, reified Implementation: Interface> registerDefaultImplementation(int: KClass<Interface>, noinline block: (Any?) -> Implementation) {
        if (Implementation::class == Interface::class) throw IllegalArgumentException("The interface and implementation classes are identical (${Interface::class.simpleName} == ${Implementation::class.simpleName})")
        if (defaultImplementations.containsKey(Interface::class)) throw IllegalArgumentException("The ${Interface::class.simpleName} already has the default implementaiton ${defaultImplementations[Interface::class]!!.implementationClass.simpleName}.")
        println("Default Registration: ${Implementation::class.simpleName} for ${Interface::class.simpleName}")
        defaultImplementations[Interface::class] =
            InstanceFactory<Interface, Implementation>(Implementation::class, block)
    }
}

/**
 * Will retrieve the implementation [KClass] for the interface [T] and creates a new instance that can be used for implementation by delegation.
 *
 * Example:
 * ```
 * class Test : TestInterface by implementation()
 * ```
 *
 * This will retrieve the current implementation [KClass] for `TestInterface` and creates a new instance that will be used for the delegation.
 */
inline fun <reified T> implementation(args: Any?) : T = ImplementationRegistry.getImplementationForClass(
    T::class
).factory.invoke(args) as T

inline fun <reified T> implementation() : T = implementation(null)

/**
 * Like [implementation] but will return the originally designed default implementation and not the currently active. Can be used for interceptor classes:
 *
 * Example:
 * ```
 *  class RunnerInterceptor(val baseImpl: RunnerInterface = defaultImplementation()) : RunnerInterface {
 *    override fun start() {
 *      println("before start")
 *      baseImpl.start()
 *    }
 *  }
 * ```
 */
inline fun <reified T> defaultImplementation(args: Any?) : T = ImplementationRegistry.getDefaultImplementationForClass(
    T::class
).factory.invoke(args) as T
inline fun <reified T> defaultImplementation() : T = defaultImplementation(null)

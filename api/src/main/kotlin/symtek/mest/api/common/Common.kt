package symtek.mest.api.common

import com.sun.xml.internal.fastinfoset.util.StringArray
import kotlinx.serialization.*
import kotlinx.serialization.cbor.Cbor
import kotlinx.serialization.internal.EnumSerializer
import kotlinx.serialization.internal.SerialClassDescImpl
import java.time.Duration
import kotlin.reflect.KFunction
import kotlin.reflect.KParameter
import kotlin.reflect.full.*


@Serializer(forClass = Duration::class)
object DurationSerializer : KSerializer<Duration> {
    override val descriptor: SerialDescriptor = object : SerialClassDescImpl("BinaryPayload") {
        init {
            addElement("seconds")
            addElement("nano")
        }
    }

    override fun deserialize(decoder: Decoder): Duration {
        val dec: CompositeDecoder = decoder.beginStructure(descriptor)
        var seconds: Long? = null // consider using flags or bit mask if you
        var nano: Long? = null // need to read nullable non-optional properties
        loop@ while (true) {
            when (val i = dec.decodeElementIndex(descriptor)) {
                CompositeDecoder.READ_DONE -> break@loop
                0 -> seconds = dec.decodeStringElement(descriptor, i).toLong()
                1 -> nano = dec.decodeStringElement(descriptor, i).toLong()
                else -> throw SerializationException("Unknown index $i")
            }
        }
        dec.endStructure(descriptor)
        return Duration.ofSeconds(
            seconds ?: throw MissingFieldException("seconds"),
            nano ?: throw MissingFieldException("nano")
        )
    }

    override fun serialize(encoder: Encoder, obj: Duration) {
        val compositeOutput = encoder.beginStructure(descriptor)
        compositeOutput.encodeStringElement(descriptor, 0, obj.seconds.toString())
        compositeOutput.encodeStringElement(descriptor, 1, obj.nano.toString())
        compositeOutput.endStructure(descriptor)
    }

}


fun <R> KFunction<R>.callNamed(params: Map<String, Any?>, self: Any? = null, extSelf: Any? = null): R {
    val map: MutableMap<KParameter, Any?> = mutableMapOf()
    params.entries.forEach { entry ->
        map.put(parameters.find { it.name == entry.key }!!, entry.value)
    }

    if (self != null) map += instanceParameter!! to self
    if (extSelf != null) map += extensionReceiverParameter!! to extSelf
    return callBy(map.toMap())
}


val supportedPrimitives = listOf(
    Double::class,
    Float::class,
    Long::class,
    Int::class,
    Short::class,
    Byte::class,
    Char::class,
    String::class,
    Boolean::class
)

val supportedPrimitivesArrays = listOf(
    DoubleArray::class,
    FloatArray::class,
    LongArray::class,
    IntArray::class,
    ShortArray::class,
    ByteArray::class,
    CharArray::class,
    StringArray::class,
    BooleanArray::class
)


/**
 * Will create a copy of the given instance.
 */
fun <T: Any> copy(instance: T): T {
    val type = instance::class
    val value: Any = if (supportedPrimitives.contains(type)) {
        instance
    } else if (supportedPrimitivesArrays.contains(type)) {
        (instance as List<*>).toList() // create new list with previous list content
    } else if (type.isSubclassOf(Enum::class)){
        instance // Enums are singletons anyway so no serialization required.
    } else {
        deepCopy(instance)
    }

    @Suppress("UNCHECKED_CAST")
    return value as T
}

/**
 * Will create a deep copy of the given instance.
 * The copy will be made with [kotlinx.serialization].
 *
 * When the instance is an enum, a standard [EnumSerializer] for the given instance type will be used.
 */
private fun <T : Any> deepCopy(instance: T): T {
    val cbor = Cbor()
    val cls = instance::class
    val serializerFunction = cls.companionObject?.memberFunctions?.first { it.name == "serializer" }
        ?: throw java.lang.IllegalArgumentException("No Serializer found!")
    val serializerFunctionInstanceParam = serializerFunction.instanceParameter!!
    @Suppress("UNCHECKED_CAST") val serializer = serializerFunction.callBy(mapOf(serializerFunctionInstanceParam to cls.companionObjectInstance)) as KSerializer<T>
    return cbor.load(serializer, cbor.dump(serializer, instance)) // serialize & deserialize
}
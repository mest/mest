package symtek.mest.api.component

import kotlin.reflect.KClass

interface OutputPortInterface<T> : CommonPortInterface {
    var value: T
}

abstract class AbstractOutputPort<T : Any>(component: Component, id: String, name: String, initialValue: T, type: KClass<T>) :
    CommonPort<T>(component, id, name, type), OutputPortInterface<T> {
    override fun checkConnectionCompatibilityWith(port: CommonPort<*>): Boolean {
        return port is InputPort
    }

    override var value: T = initialValue
}

class DiscreteOutputPort<T : Any>(component: Component, id: String, name: String, initialValue: T, type: KClass<T>) :
    AbstractOutputPort<T>(component, id, name, initialValue, type),
    GeneralOutputPort

class ContinuousOutputPort<T : Any>(component: Component, id: String, name: String, initialValue: T, type: KClass<T>) :
    AbstractOutputPort<T>(component, id, name, initialValue, type),
    GeneralOutputPort

interface EventOutputPortInterface<T> : CommonPortInterface {
    fun publish(event: T)
}

class EventOutputPort<T : Any>(component: Component, id: String, name: String, type: KClass<T>) :
    CommonPort<T>(component, id, name, type), GeneralOutputPort, EventOutputPortInterface<T> {

    val events = mutableListOf<Any>()

    override fun checkConnectionCompatibilityWith(port: CommonPort<*>): Boolean {
        return port is EventInputPort
    }

    override fun publish(event: T) {
        events.add(event)
    }

}


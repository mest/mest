package symtek.mest.api.component

import com.sun.xml.internal.fastinfoset.util.StringArray
import kotlinx.serialization.Serializable
import symtek.mest.api.common.supportedPrimitives
import symtek.mest.api.common.supportedPrimitivesArrays
import java.time.Duration
import kotlin.reflect.KClass
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.isSubclassOf

/**
 * Two ports can not be connected to another because they are not compatible by definition (ex. InputPort to InputPort)
 */
class IncompatiblePortException(text: String) : java.lang.IllegalArgumentException(text)

/**
 * Two ports can not be connected to another because the generic type is incompatible (ex. PortA\<String\> to PortB\<Int\>)
 */
class IncompatiblePortTypeException(text: String) : java.lang.IllegalArgumentException(text)

/**
 * The given port type is not supported because it is not a data class or a primitive type (or array of them).
 */
class UnsupportedPortTypeException(text: String) : java.lang.IllegalArgumentException(text)

/**
 * It is not possible to connect any more ports to an port. Example: Input port only supports one connection and one port is already connected.
 */
class ConnectionImpossible(text: String) : java.lang.IllegalArgumentException(text)

interface CommonPortInterface {
    val id: String
    val name: String
}

interface GeneralInputPort
interface GeneralOutputPort

abstract class CommonPort<T : Any>(
    val component: Component,
    override val id: String,
    override val name: String,
    val type: KClass<T>
) : CommonPortInterface {
    fun checkTypeWith(port: CommonPort<*>): Boolean {
        return this.type == port.type
    }

    abstract fun checkConnectionCompatibilityWith(port: CommonPort<*>): Boolean
}

/**
 * The [PortFactory] can be used to create Ports for a user defined type.
 *
 * You can import every method of the class with `import ...PortFactory.*`. This way ports can be created only writing the method names.
 */
class PortFactory {
    companion object {

        /**
         * Validates if the type of [T] is supported. Currently the following types are supported:
         * - Any [Number]
         *   - [Double]
         *   - [Float]
         *   - [Long]
         *   - [Int]
         *   - [Short]
         *   - [Byte]
         * - [Char]
         * - [String]
         * - [Boolean]
         * - Arrays of the above like [IntArray], [BooleanArray], [StringArray], ...
         * - Serializable Classes
         * - Enums
         *
         * If the type of [T] doesn't match any of the supported types, a [UnsupportedPortTypeException] will be thrown.
         *
         */
        inline fun <reified T> validateClass() {
            if (supportedPrimitives.contains(T::class)) return
            if (supportedPrimitivesArrays.contains(T::class)) return
            if (T::class.findAnnotation<Serializable>() != null) return
            if (T::class.isSubclassOf(Enum::class)) return
            throw UnsupportedPortTypeException("The given type '${T::class.simpleName}' is not supported.\nPlease use a primitive type, primitive typed array or a class with the annotation ${Serializable::class.qualifiedName}.\nThe following primitive types are supported: $supportedPrimitives\nThe following primitve typed arrays are supported: $supportedPrimitivesArrays")
        }

        /**
         * This method can be used to create a new [InputPort]. The type of the port will be defined by the generic type argument.
         */
        inline fun <reified T : Any> inputPort(
            component: Component,
            id: String,
            name: String,
            maxNumberOfConnections: Int = 1
        ): InputPortInterface<T> {
            validateClass<T>()
            return InputPort<T>(
                component,
                id,
                name,
                T::class,
                maxNumberOfConnections = maxNumberOfConnections
            )
        }

        /**
         * This method can be used to create a new [DiscreteOutputPort]. The type of the port will be defined by the generic type argument.
         */
        inline fun <reified T : Any> discreteOutputPort(
            component: Component,
            id: String,
            name: String,
            initialValue: T
        ): OutputPortInterface<T> {
            validateClass<T>()
            return DiscreteOutputPort<T>(component, id, name, initialValue, T::class)
        }

        /**
         * This method can be used to create a new [ContinuousOutputPort]. The type of the port will be defined by the generic type argument.
         */
        inline fun <reified T : Any> continuousOutputPort(
            component: Component,
            id: String,
            name: String,
            initialValue: T
        ): OutputPortInterface<T> {
            validateClass<T>()
            return ContinuousOutputPort<T>(component, id, name, initialValue, T::class)
        }

        /**
         * This method can be used to create a new [EventInputPort]. The type of the port will be defined by the generic type argument.
         */
        inline fun <reified T : Any> eventInputPort(component: Component, id: String, name: String, noinline eventHandler: (event: T) -> Unit): EventInputPortInterface {
            @kotlin.Suppress("UNCHECKED_CAST")
            return EventInputPort<T>(
                component,
                id,
                name,
                T::class,
                eventHandler as ( T: Any) -> Unit
            )
        }

        /**
         * This method can be used to create a new [EventOutputPort]. The type of the port will be defined by the generic type argument.
         */
        inline fun <reified T : Any> eventOutputPort(component: Component, id: String, name: String): EventOutputPortInterface<T> {
            return EventOutputPort<T>(component, id, name, T::class)
        }

    }
}
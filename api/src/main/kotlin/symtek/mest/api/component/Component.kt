package symtek.mest.api.component

import symtek.mest.api.simulation.Environment

abstract class Component(
    val name: String,
    val environment: Environment,
    val configuration: ComponentConfiguration
) {
    val links: MutableList<Link> = mutableListOf()

    abstract val ports: List<CommonPortInterface>



    fun validatePorts() {
        var duplicate: String? = null
        val duplicates = ports.stream().filter {
            val currentPort = it
            if (ports.stream().filter { it != currentPort && it.name == currentPort.name }.count() > 0) {
                duplicate = it.name
                true
            } else {
                false
            }
        }.count()

        if (duplicates > 0) {
            throw IllegalStateException("There are at least two ports with the identical name $duplicate")
        }
    }

    open fun tickTriggered() {}

    open fun outputTriggered() {}

    inline fun <reified T : Any> inputPort(name: String, id: String = name, maxNumberOfConnections: Int = 1) : InputPortInterface<T> = PortFactory.inputPort(this, name, id, maxNumberOfConnections)
    inline fun <reified T : Any> discreteOutputPort(name: String, id: String = name, initialValue: T) : OutputPortInterface<T> = PortFactory.discreteOutputPort(this, name, id, initialValue)
    inline fun <reified T : Any> continuousOutputPort(name: String, id: String = name, initialValue: T) : OutputPortInterface<T> = PortFactory.continuousOutputPort(this, name, id, initialValue)
    inline fun <reified T : Any> eventInputPort(name: String, id: String = name, noinline eventHandler: (event: T) -> Unit) : EventInputPortInterface = PortFactory.eventInputPort(this, name, id, eventHandler)
    inline fun <reified T : Any> eventOutputPort(name: String, id: String = name) : EventOutputPortInterface<T> = PortFactory.eventOutputPort(this, name, id)
}


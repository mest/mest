package symtek.mest.api.component.examples

import symtek.mest.api.component.*
import symtek.mest.api.simulation.Environment

class LogicAndComponent(name: String, environment: Environment, configuration: ComponentConfiguration = ComponentConfiguration(continuousBehaviour = ContinuousBehaviour.RunOnRequest)) : AbstractLogicComponent(name, environment, configuration) {
    override fun operation(b1: Boolean, b2: Boolean): Boolean = b1 == b2
}

class LogicOrComponent(name: String, environment: Environment, configuration: ComponentConfiguration = ComponentConfiguration(continuousBehaviour = ContinuousBehaviour.RunOnRequest)) : AbstractLogicComponent(name, environment, configuration) {
    override fun operation(b1: Boolean, b2: Boolean): Boolean = b1 || b2
}

abstract class AbstractLogicComponent(
    name: String,
    environment: Environment,
    configuration: ComponentConfiguration = ComponentConfiguration()
) : Component(name, environment, configuration) {
    val inputPort = PortFactory.inputPort<Boolean>(this, "input", "Input", 0)
    val outputPort = PortFactory.continuousOutputPort(this, "output" , "Output", false)

    override val ports: List<CommonPortInterface> = mutableListOf(
        inputPort,
        outputPort
    )

    override fun outputTriggered() {
        val inputs = inputPort.retrieveAll()
        val result = inputs.stream().reduce { b1, b2 -> operation(b1!!, b2!!) }.orElse(false)
        outputPort.value = result
    }

    abstract fun operation(b1: Boolean, b2: Boolean): Boolean
}
package symtek.mest.api.component

import kotlinx.serialization.Serializable
import symtek.mest.api.common.DurationSerializer
import java.time.Duration

@Serializable
data class ComponentConfiguration(
    @Serializable(with = DurationSerializer::class) var periodDuration: Duration? = null,
    val continuousBehaviour: ContinuousBehaviour = ContinuousBehaviour.RunAlways,
    val customComponentConfiguration: CustomComponentConfiguration? = null
)

enum class ContinuousBehaviour {
    /**
     * Specifies that this [Component] (if containing a [ContinuousOutputPort]) is only executed, when any connected [Component] will be executed in the step.
     */
    RunOnRequest,
    /**
     * Specifies that this [Component] (if containing a [ContinuousOutputPort]) is executed in every step.
     */
    RunAlways
}

@Serializable
class CustomComponentConfiguration

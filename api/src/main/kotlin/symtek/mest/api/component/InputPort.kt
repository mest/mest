package symtek.mest.api.component

import java.time.Duration
import kotlin.reflect.KClass

interface InputPortInterface<T> : CommonPortInterface {
    fun retrieve(): T?
    fun retrieveAll(): List<T>
}

class InputPort<T : Any>(
    component: Component,
    id: String,
    name: String,
    type: KClass<T>,
    val maxNumberOfConnections: Int = 1
) : CommonPort<T>(component, id, name, type), GeneralInputPort, InputPortInterface<T> {

    val values = mutableListOf<Any>()

    override fun checkConnectionCompatibilityWith(port: CommonPort<*>): Boolean {
        return port is AbstractOutputPort<*>
    }

    override fun retrieve(): T? {
        return if (values.isEmpty()) {
            null
        } else {
            @Suppress("UNCHECKED_CAST")
            values[0] as T
        }
    }

    override fun retrieveAll(): List<T> {
        @Suppress("UNCHECKED_CAST")
        return values as List<T>
    }
}

interface EventInputPortInterface : CommonPortInterface

class EventInputPort<T : Any>(component: Component, id: String, name: String, type: KClass<T>, val eventHandler: (T : Any) -> Unit) : CommonPort<T>(component, id, name, type),
    GeneralInputPort, EventInputPortInterface {
    override fun checkConnectionCompatibilityWith(port: CommonPort<*>): Boolean {
        return port is EventOutputPort<*>
    }

    val events : MutableList<Any> = mutableListOf()
}

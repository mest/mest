package symtek.mest.api.component

import java.util.*

data class Link(val portA: CommonPort<*>, val portB: CommonPort<*>, val id: String = UUID.randomUUID().toString()) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Link

        if (portA != other.portA && portA != other.portB) return false
        if (portB != other.portB && portB != other.portA) return false

        return true
    }

    override fun hashCode(): Int {
        var result = portA.hashCode()
        result = 31 * result + portB.hashCode()
        return result
    }
}
package symtek.mest.api.component

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.assertThrows
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import symtek.mest.api.simulation.ComponentCandidate
import symtek.mest.api.simulation.Environment
import symtek.mest.api.simulation.SimulationManager

class TestComponent(name: String, environment: Environment, override val ports: List<CommonPort<*>>, configuration: ComponentConfiguration = ComponentConfiguration()) :
    Component(name, environment, configuration)


fun createAndConnectLink(simulationManager: SimulationManager, portA: CommonPort<*>, portB: CommonPort<*>): Link {
    val componentA = portA.component
    val componentB = portB.component
    val portsA = componentA.ports as MutableList
    val portsB = componentB.ports as MutableList
    addPortToComponent(componentA, portA)
    if (componentA != componentB) {
        addPortToComponent(componentB, portB)
    }
    val link = Link(portA, portB)
    simulationManager.connectLink(link)
    return link
}

fun addPortToComponent(component: Component, port: CommonPort<*>) {
    if (!component.ports.contains(port)) {
        (component.ports as MutableList).add(port)
    }
}

object ComponentUtilTest : Spek({

    describe("ComponentUtil") {

        val environment by memoized { mockk<Environment>() }

        val simulationManager by memoized { SimulationManager() }

        val c1_ports by memoized { mutableListOf<CommonPort<*>>() }
        val c1: Component by memoized {
            TestComponent(
                name = "MyComponent1",
                environment = environment,
                ports = c1_ports
            )
        }

        val c2_ports by memoized { mutableListOf<CommonPort<*>>() }
        val c2: Component by memoized {
            TestComponent(
                name = "MyComponent2",
                environment = environment,
                ports = c2_ports
            )
        }

        val mockPort1 by memoized { mockk<CommonPort<*>>() }
        val mockPort2 by memoized { mockk<CommonPort<*>>() }

        beforeEach {
            listOf(mockPort1, mockPort2).forEach {
                every { it.checkConnectionCompatibilityWith(any()) } returns true
                every { it.checkTypeWith(any()) } returns true
            }
            every { mockPort1.component } returns c1
            every { mockPort2.component } returns c2

            val c1c = mockk<ComponentCandidate<*>>()
            val c2c = mockk<ComponentCandidate<*>>()

            every { c1c.initComponentInstance } returns c1
            every { c1c.componentClass } returns c1::class

            every { c2c.initComponentInstance } returns c2
            every { c2c.componentClass } returns c2::class

            simulationManager.addComponentCandidate(c1c)
            simulationManager.addComponentCandidate(c2c)
        }

        it("calls the correct port methods when connecting two ports via a link") {
            createAndConnectLink(simulationManager, mockPort1, mockPort2)

            verify(exactly = 1) {
                mockPort1.checkConnectionCompatibilityWith(eq(mockPort2))
                mockPort2.checkConnectionCompatibilityWith(eq(mockPort1))

                // check if type check was called
                mockPort1.checkTypeWith(eq(mockPort2))
                mockPort2.checkTypeWith(eq(mockPort1))
            }
        }

        it("will add the link-to-be-connected to both components") {
            val link = createAndConnectLink(simulationManager, mockPort1, mockPort2)

            assertEquals(1, c1.links.size)
            assertEquals(link, c1.links[0])
            assertEquals(1, c2.links.size)
            assertEquals(link, c2.links[0])
        }
        it("will add the link only once if input and output port are on the same component") {
            every { mockPort1.component } returns c1
            every { mockPort2.component } returns c1

            val link = createAndConnectLink(simulationManager, mockPort1, mockPort2)

            assertEquals(1, c1.links.size)
            assertEquals(link, c1.links[0])
        }

        it("will not add a link twice") {
            val link = createAndConnectLink(simulationManager, mockPort1, mockPort2)
            assertThrows<java.lang.IllegalArgumentException>(message = "The given link is already established.") {
                simulationManager.connectLink(
                    link
                )
            }
        }
        it("will not add incompatible ports") {
            // test when port a will fail only
            every { mockPort1.checkConnectionCompatibilityWith(any()) } returns false
            assertThrows<IncompatiblePortException> {
                createAndConnectLink(simulationManager, 
                    mockPort1,
                    mockPort2
                )
            }

            // test if both ports will fail
            every { mockPort2.checkConnectionCompatibilityWith(any()) } returns false
            assertThrows<IncompatiblePortException> {
                createAndConnectLink(simulationManager, 
                    mockPort1,
                    mockPort2
                )
            }

            // test if port b will fail only
            every { mockPort1.checkConnectionCompatibilityWith(any()) } returns true
            assertThrows<IncompatiblePortException> {
                createAndConnectLink(simulationManager, 
                    mockPort1,
                    mockPort2
                )
            }
        }

        it("will not add non-matching port value-types") {
            // test when port a will fail only
            every { mockPort1.checkTypeWith(any()) } returns false
            assertThrows<IncompatiblePortTypeException> {
                createAndConnectLink(simulationManager, 
                    mockPort1,
                    mockPort2
                )
            }

            // test if both ports will fail
            every { mockPort2.checkTypeWith(any()) } returns false
            assertThrows<IncompatiblePortTypeException> {
                createAndConnectLink(simulationManager, 
                    mockPort1,
                    mockPort2
                )
            }

            // test if port b will fail only
            every { mockPort1.checkTypeWith(any()) } returns true
            assertThrows<IncompatiblePortTypeException> {
                createAndConnectLink(simulationManager, 
                    mockPort1,
                    mockPort2
                )
            }
        }
    }
})
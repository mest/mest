package symtek.mest.api.component.port

import io.mockk.every
import io.mockk.mockk
import kotlinx.serialization.Serializable
import org.junit.jupiter.api.Assertions.*
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import symtek.mest.api.simulation.Environment
import symtek.mest.api.simulation.Timestamp
import symtek.mest.api.component.*
import java.time.Duration
import kotlin.reflect.KClass
import kotlin.reflect.full.primaryConstructor

object PortTest : Spek({

    describe("Ports") {
        val environment by memoized {
            mockk<Environment> {
                every { timestamp } returns Timestamp(
                    currentTick = 0,
                    currentTime = Duration.ZERO
                )
            }
        }

        val c1_ports by memoized { mutableListOf<CommonPort<*>>() }
        val c1: Component by memoized {
            TestComponent(
                name = "MyComponent1",
                environment = environment,
                ports = c1_ports
            )
        }
        val c1_singleInputPort1 by memoized { PortFactory.inputPort<Int>(c1, "singleInputPort1", "singleInputPort1") }
        val c1_multiInputPort1 by memoized { PortFactory.inputPort<Int>(c1, "multiInputPort1", "multiInputPort1", 0) }
        val c1_discreteOutputPort1 by memoized { PortFactory.discreteOutputPort<Int>(c1, "discreteOutputPort1", "discreteOutputPort1", 0) }
        val c1_continuousOutputPort1 by memoized { PortFactory.continuousOutputPort<Int>(c1, "continuousOutputPort1", "continuousOutputPort1", 0) }
        val c1_eventInputPort by memoized { PortFactory.eventInputPort<Int>(c1, "EventInputPort1", "EventInputPort1") {} }
        val c1_eventOutputPort by memoized { PortFactory.eventOutputPort<Int>(c1, "EventOutputPort1", "EventOutputPort1") }

        beforeEach {
        }

        describe("PortInterface Implementations") {

            fun <T : CommonPort<*>> createInstanceOf(component: Component, name: String, portClass: KClass<T>): T =
                portClass.primaryConstructor!!.call(component, name)

            fun <T : CommonPort<*>> testGetNameFor(portClass: KClass<T>) {
                val name = "TestName"
                val testPort = createInstanceOf(c1, name, portClass)
                assertEquals(name, testPort.name)
            }

            it("returns the correct type and name") {
                @Serializable data class A(val x: Double = 0.0)
                @Serializable data class B(val x: Double = 0.0)
                @Serializable data class C(val x: Double = 0.0)
                @Serializable data class D(val x: Double = 0.0)
                @Serializable data class E(val x: Double = 0.0)

                val testname = "test"

                @Suppress("UNCHECKED_CAST")
                val l : List<CommonPort<*>> = listOf(
                    PortFactory.inputPort<A>(c1, "t1", testname),
                    PortFactory.discreteOutputPort<B>(c1, "t2", testname, B()),
                    PortFactory.continuousOutputPort<C>(c1, "t3", testname, C()),
                    PortFactory.eventInputPort<D>(c1, "t4", testname) {  },
                    PortFactory.eventOutputPort<E>(c1, "t5", testname)
                ) as List<CommonPort<*>>

                l.forEach {
                    assertEquals(testname, it.name)
                }
                assertEquals(A::class, l[0].type)
                assertEquals(B::class, l[1].type)
                assertEquals(C::class, l[2].type)
                assertEquals(D::class, l[3].type)
                assertEquals(E::class, l[4].type)
            }

            describe("InputPort") {
                // TODO: Fix after SimulationManager update.
                /*it("is connectable to the max number of connections specified") {
                    val negative = PortFactory.inputPort<Int>(c1, "negative", "negative", -1)
                    val zero = PortFactory.inputPort<Int>(c1, "zero", "zero", 0)
                    val one = PortFactory.inputPort<Int>(c1, "one", "one", 1)
                    val two = PortFactory.inputPort<Int>(c1, "two", "two", 2)


                    checkIfConnectionIsAlwaysPossibleFor(negative)
                    checkIfConnectionIsAlwaysPossibleFor(zero)

                    assertTrue(negative.isConnectionPossible())
                    assertTrue(zero.isConnectionPossible())

                    one.connectTo(mockk())
                    assertFalse(one.isConnectionPossible())

                    two.connectTo(mockk())
                    assertTrue(two.isConnectionPossible())
                    two.connectTo(mockk())
                    assertFalse(two.isConnectionPossible())
                }*/
                it("is compatible with a discrete and continuous output port") {
                    with(c1_singleInputPort1 as InputPort) {
                        // general
                        assertTrue(checkConnectionCompatibilityWith(mockk<DiscreteOutputPort<*>>()))
                        assertTrue(checkConnectionCompatibilityWith(mockk<ContinuousOutputPort<*>>()))
                        // exmaples
                        assertTrue(checkConnectionCompatibilityWith(c1_discreteOutputPort1 as CommonPort<*>))
                        assertTrue(checkConnectionCompatibilityWith(c1_continuousOutputPort1 as CommonPort<*>))
                    }
                }
                it("is incompatible with other input ports and event ports") {
                    with(c1_singleInputPort1 as InputPort) {
                        // general
                        assertFalse(checkConnectionCompatibilityWith(mockk<InputPort<*>>()))
                        assertFalse(checkConnectionCompatibilityWith(mockk<EventInputPort<*>>()))
                        assertFalse(checkConnectionCompatibilityWith(mockk<EventOutputPort<*>>()))
                        // examples
                        assertFalse(checkConnectionCompatibilityWith(c1_singleInputPort1 as InputPort<Int>))
                        assertFalse(checkConnectionCompatibilityWith(c1_multiInputPort1 as InputPort<Int>))
                        assertFalse(checkConnectionCompatibilityWith(c1_eventInputPort as EventInputPort<*>))
                        assertFalse(checkConnectionCompatibilityWith(c1_eventOutputPort as EventOutputPort))
                    }
                }
            }
            describe("DiscreteOutputPort") {
                it("is compatible with a input port") {
                    with(c1_discreteOutputPort1 as AbstractOutputPort) {
                        // general
                        assertTrue(checkConnectionCompatibilityWith(mockk<InputPort<*>>()))
                        // examples
                        assertTrue(checkConnectionCompatibilityWith(c1_singleInputPort1 as InputPort))
                        assertTrue(checkConnectionCompatibilityWith(c1_multiInputPort1 as InputPort))
                    }
                }
                it("is incompatible with other output ports and event ports") {
                    with(c1_discreteOutputPort1 as AbstractOutputPort) {
                        // general
                        assertFalse(checkConnectionCompatibilityWith(mockk<ContinuousOutputPort<*>>()))
                        assertFalse(checkConnectionCompatibilityWith(mockk<DiscreteOutputPort<*>>()))
                        assertFalse(checkConnectionCompatibilityWith(mockk<EventInputPort<*>>()))
                        assertFalse(checkConnectionCompatibilityWith(mockk<EventOutputPort<*>>()))
                        // examples
                        assertFalse(checkConnectionCompatibilityWith(c1_discreteOutputPort1 as AbstractOutputPort))
                        assertFalse(checkConnectionCompatibilityWith(c1_eventInputPort as EventInputPort<*>))
                        assertFalse(checkConnectionCompatibilityWith(c1_eventOutputPort as EventOutputPort))
                        assertFalse(checkConnectionCompatibilityWith(c1_continuousOutputPort1 as AbstractOutputPort))
                    }
                }
            }
            describe("ContinuousOutputPort") {
            }
            describe("EventInputPort") {
            }
            describe("EventOutputPort") {
            }
        }
    }
})
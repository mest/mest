package symtek.mest.api.component.port

import com.sun.xml.internal.fastinfoset.util.StringArray
import kotlinx.serialization.*
import org.junit.jupiter.api.Assertions.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import symtek.mest.api.component.PortFactory
import symtek.mest.api.component.UnsupportedPortTypeException

enum class TestEnum { A, B, C }
enum class SerializableTestEnum { A, B, C }

internal class PortFactoryTest : Spek({
    describe("PortFactory") {

        describe("method validateClass()") {
            it("accepts Numbers") {
                assertDoesNotThrow { PortFactory.validateClass<Double>() }
                assertDoesNotThrow { PortFactory.validateClass<Float>() }
                assertDoesNotThrow { PortFactory.validateClass<Long>() }
                assertDoesNotThrow { PortFactory.validateClass<Int>() }
                assertDoesNotThrow { PortFactory.validateClass<Short>() }
                assertDoesNotThrow { PortFactory.validateClass<Byte>() }
            }
            it("accepts Chars") {
                assertDoesNotThrow { PortFactory.validateClass<Char>() }
            }
            it("accepts Strings") {
                assertDoesNotThrow { PortFactory.validateClass<String>() }
            }
            it("accepts Boolean") {
                assertDoesNotThrow { PortFactory.validateClass<Boolean>() }
            }
            it("accepts primitive typed arrays") {
                // Numbers
                assertDoesNotThrow { PortFactory.validateClass<DoubleArray>() }
                assertDoesNotThrow { PortFactory.validateClass<FloatArray>() }
                assertDoesNotThrow { PortFactory.validateClass<LongArray>() }
                assertDoesNotThrow { PortFactory.validateClass<IntArray>() }
                assertDoesNotThrow { PortFactory.validateClass<ShortArray>() }
                assertDoesNotThrow { PortFactory.validateClass<ByteArray>() }
                // Chars
                assertDoesNotThrow { PortFactory.validateClass<CharArray>() }
                // String
                assertDoesNotThrow { PortFactory.validateClass<StringArray>() }
                // Boolean
                assertDoesNotThrow { PortFactory.validateClass<BooleanArray>() }
            }
            it("accepts enums") {
                assertDoesNotThrow { PortFactory.validateClass<TestEnum>() }
            }
            class TestClass(val x: Double)
            data class TestDataClass(val x: Double)
            it("doesnt't accept non-serializable classes") {
                assertThrows<UnsupportedPortTypeException> { PortFactory.validateClass<TestClass>() }
            }
            it("doesnt't accept non-serializable data classes") {
                assertThrows<UnsupportedPortTypeException> { PortFactory.validateClass<TestDataClass>() }
            }
            @Serializable class SerializableTestClass(val x: Double)
            @Serializable data class SerializableTestDataClass(val x: Double)
            it("accepts serializable classes") {
                assertDoesNotThrow { PortFactory.validateClass<SerializableTestClass>() }
            }
            it("accepts serializable data classes") {
                assertDoesNotThrow { PortFactory.validateClass<SerializableTestDataClass>() }
            }
            it("doesn't support anything else") {
                assertThrows<UnsupportedPortTypeException> { PortFactory.validateClass<Exception>() }
                assertThrows<UnsupportedPortTypeException> { PortFactory.validateClass<TestClass>() }
            }
        }

    }
})
package symtek.mest.api.component

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.assertThrows
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import symtek.mest.api.component.PortFactory.Companion.discreteOutputPort
import symtek.mest.api.component.PortFactory.Companion.inputPort
import symtek.mest.api.simulation.*

object SimulationManagerTest : Spek({

})

const val c1Name = "c1"
const val c1PortName = "input"

class TestComponent1(name: String, environment: Environment, configuration: ComponentConfiguration = ComponentConfiguration()) : Component(name, environment, configuration) {
    override val ports: List<CommonPortInterface> = listOf(inputPort<Int>(this, c1PortName, c1PortName))
}

const val c2Name = "c2"
const val c2PortName = "output"

class TestComponent2(name: String, environment: Environment, configuration: ComponentConfiguration = ComponentConfiguration()) : Component(name, environment, configuration) {
    override val ports: List<CommonPortInterface> = listOf(discreteOutputPort<Int>(this, c2PortName, c2PortName, 0))
}

class TestComponentWithPortOverrideInConstructor(
    name: String,
    environment: Environment,
    configuration: ComponentConfiguration = ComponentConfiguration(),
    override val ports: List<CommonPortInterface> = listOf()
) : Component(name, environment, configuration)

class TestInvalidComponent(
    environment: Environment,
    override val ports: List<CommonPortInterface> = listOf()
) : Component("abc", environment, ComponentConfiguration())

class TestComponentWithConstructorException(name: String, environment: Environment, configuration: ComponentConfiguration = ComponentConfiguration()) : Component(name, environment, configuration) {
    init {
        throw Exception()
    }

    override val ports: List<CommonPortInterface> = listOf()
}

object ComponentCandidateTest : Spek({

    val simulationManager by memoized { SimulationManager() }
    val cc1 by memoized { ComponentCandidate(TestComponent1::class, c1Name) }
    val cc2 by memoized { ComponentCandidate(TestComponent2::class, c2Name) }

    describe("ComponentCandidate") {
        it("uses the declared ports of a component") {
            assertEquals(c1PortName, cc1.ports[0].name)
            assertEquals(c2PortName, cc2.ports[0].name)
        }
        it("creates an initial component instance that doesn't have the same name as the instance would have") {
            //val componentCandidate = ComponentCandidate(TestComponentForCandidateTest::class.java, "test")
            assertNotEquals(c1Name, cc1.initComponentInstance.name)
            assertNotEquals(c2Name, cc2.initComponentInstance.name)
        }

        it("doesn't accept components with non-conformant constructor") {
            assertThrows<InvalidComponentConstructorException> {
                ComponentCandidate(TestInvalidComponent::class, "invalid")
            }
        }

        it("does accept components with ports as override parameter") {
            assertDoesNotThrow {
                ComponentCandidate(
                    TestComponentWithPortOverrideInConstructor::class,
                    "invalid"
                )
            }
        }

        it("will fail to create an instance when the constructor will throw an exception") {
            assertThrows<ComponentInstantiationException> {
                ComponentCandidate(
                    TestComponentWithConstructorException::class,
                    "invalid"
                )
            }
        }

        it("will not share environments between candidates") {
            assertFalse(cc1.initComponentInstance.environment === cc2.initComponentInstance.environment)
        }
    }

    describe("SimulationManager") {

        describe("createSimulation()") {

            val simulation by memoized {
                simulationManager.addComponentCandidate(cc1)
                simulationManager.addComponentCandidate(cc2)
                simulationManager.connectLink(Link(cc1.ports[0], cc2.ports[0]))
                simulationManager.createSimulation()
            }

            it("will contain all component candidates") {
                assertEquals(2, simulation.components.size)
                val c1 = simulation.components.first { it.componentCandidate == cc1 }
                val c2 = simulation.components.first { it.componentCandidate == cc2 }
                assertEquals(c1.componentCandidate.name, c1.componentInstance.name)
                assertEquals(c2.componentCandidate.name, c2.componentInstance.name)
            }

            it("Will reconnect existing links") {
                val ci1 =
                    simulation.components.stream().filter { it.componentCandidate == cc1 }.map { it.componentInstance }
                        .findFirst().get()
                val ci2 =
                    simulation.components.stream().filter { it.componentCandidate == cc2 }.map { it.componentInstance }
                        .findFirst().get()

                assertEquals(1, ci1.links.size)
                assertEquals(1, ci2.links.size)

                assertEquals(ci1.links[0], ci2.links[0])
            }

            it("will always create a new environment for every simulation") {
                val simulation2 = simulationManager.createSimulation()
                assertFalse(simulation.environment === simulation2.environment)
            }
            it("will share one environment between all components") {
                val c1 = simulation.components.first { it.componentCandidate == cc1 }
                val c2 = simulation.components.first { it.componentCandidate == cc2 }

                assertTrue(c1.componentInstance.environment === c2.componentInstance.environment)
            }

            it("will always create new copies of the configuration with identical content") {
                simulationManager.createSimulation().components.forEach { candidateToComponent ->
                    val initConfiguration = candidateToComponent.componentCandidate.initComponentInstance.configuration
                    val instanceConfiguration = candidateToComponent.componentInstance.configuration

                    assertFalse(initConfiguration === instanceConfiguration)
                    assertEquals(initConfiguration.periodDuration, instanceConfiguration.periodDuration)
                    assertEquals(initConfiguration.continuousBehaviour, instanceConfiguration.continuousBehaviour)
                }
            }
        }
    }
})



package symtek.mest.api

import org.spekframework.spek2.Spek

import org.junit.jupiter.api.Assertions.*
import org.spekframework.spek2.style.specification.describe
import symtek.mest.api.common.ImplementationRegistry
import symtek.mest.api.common.InstanceFactory
import symtek.mest.api.common.defaultImplementation
import symtek.mest.api.common.implementation
import kotlin.reflect.KClass
import kotlin.reflect.KProperty
import kotlin.reflect.jvm.isAccessible

interface SinglePropertyInterface


interface DataAndMethod {
    var data: Int
    fun increase()
    fun retrieve(): Int
}
class DataAndMethodBaseImpl : DataAndMethod {
    companion object {
        val increase = 2
        val initial = 1
    }
    override var data: Int = initial
    override fun increase() {
        data += increase
    }
    override fun retrieve(): Int = data
}
class DataAndMethodFullOverrideImpl : DataAndMethod {
    companion object {
        val increase = 2
        val initial = 2
    }
    override var data: Int = initial
    override fun increase() {
        data += increase
    }
    override fun retrieve(): Int = data
}

class DataAndMethodMethodInterceptorImpl(val baseImpl: DataAndMethod = defaultImplementation()) : DataAndMethod by baseImpl {
    companion object {
        val increase = 5
    }
    override fun increase() {
        data += increase
        baseImpl.increase()
    }
}

class DataAndMethodStateClashImpl(val baseImpl: DataAndMethod = defaultImplementation()) : DataAndMethod by baseImpl {
    companion object {
        val initial = 5
    }
    override var data: Int = initial
}

class SimpleDataAndMethod : DataAndMethod by implementation()
class DataAndMethodWithDataOverride : DataAndMethod by implementation() {
    companion object {
        val initial = 15
    }
    override var data: Int = initial
}

fun retrieveProperty() = ImplementationRegistry::class.members.first { it.name == "defaultImplementations" } as KProperty<*>

fun setDefaultImplementations(originalDefaults: MutableMap<KClass<*>, InstanceFactory<*, *>>) {
    val defaultImplementations = getDefaultImplementations()
    defaultImplementations.clear()
    defaultImplementations.putAll(originalDefaults)
}

fun getDefaultImplementations(): MutableMap<KClass<*>, InstanceFactory<*, *>> {
    val defaultImplementationsCallable = retrieveProperty()
    defaultImplementationsCallable.isAccessible = true
    defaultImplementationsCallable.getter.isAccessible = true
    @Suppress("UNCHECKED_CAST")
    return defaultImplementationsCallable.getter.call() as MutableMap<KClass<*>, InstanceFactory<*, *>>
}

fun revertToDefaultImplementations() {
    val impls = ImplementationRegistry.implementations
    impls.clear()
    val defaults = getDefaultImplementations()
    impls.putAll(defaults)
}
internal class ImplementationRegistryTest : Spek({

    describe("ImplementationRegistry") {
        val realDefaultImplementations = getDefaultImplementations().toMutableMap()

        before{
            // fresh start with custom default implementation made in other tests
            revertToDefaultImplementations()

            val defaultImplementations = getDefaultImplementations()
            assertEquals(getDefaultImplementations(), ImplementationRegistry.implementations)

            defaultImplementations[DataAndMethod::class] =
                InstanceFactory(DataAndMethod::class) {
                    DataAndMethodBaseImpl()
                }
            revertToDefaultImplementations()
        }
        afterEachTest {
            // fresh start for every test
            revertToDefaultImplementations()
            assertEquals(getDefaultImplementations(), ImplementationRegistry.implementations)
        }
        after{
            // revert back to real default implementations
            setDefaultImplementations(realDefaultImplementations)
            revertToDefaultImplementations()
            assertEquals(realDefaultImplementations, ImplementationRegistry.implementations)
        }

        it("its active implementations are equal to their default implementations") {
            with(ImplementationRegistry) {
                implementations.forEach {
                    val implFactory = it.value
                    val defaultFactory = getDefaultImplementationForClass(it.key)
                    assertEquals(defaultFactory, implFactory)
                }
            }
            val defaultImpls = getDefaultImplementations()
            val impls = ImplementationRegistry.implementations
            assertEquals(defaultImpls, impls)
        }

        describe("Registration") {
            it("will not accept implementation that have identical classes") {
                class IdenticalClass
                assertThrows(IllegalArgumentException::class.java) {
                    ImplementationRegistry.registerImplementation(IdenticalClass::class) {
                        IdenticalClass()
                    }
                }
            }
            it("will accept an custom implementation") {
                ImplementationRegistry.registerImplementation(DataAndMethod::class) {
                    DataAndMethodBaseImpl()
                }
                ImplementationRegistry.registerImplementation(DataAndMethod::class) {
                    DataAndMethodFullOverrideImpl()
                }
            }
        }

        describe("Concept") {

            it("will use the default implementation") {
                val damBase = SimpleDataAndMethod()
                assertEquals(DataAndMethodBaseImpl.initial, damBase.data)
                damBase.increase()
                assertEquals(DataAndMethodBaseImpl.initial + DataAndMethodBaseImpl.increase, damBase.data)
            }

            it("will use the new implementation instead of the old") {
                ImplementationRegistry.registerImplementation(DataAndMethod::class) { DataAndMethodFullOverrideImpl() }
                val damFullOverride = SimpleDataAndMethod()
                assertEquals(DataAndMethodFullOverrideImpl.initial, damFullOverride.data)
                damFullOverride.increase()
                assertEquals(
                    DataAndMethodFullOverrideImpl.initial + DataAndMethodFullOverrideImpl.increase,
                    damFullOverride.data
                )
            }
            it("will support method implementations intercepting methods") {
                ImplementationRegistry.registerImplementation(DataAndMethod::class) {
                    DataAndMethodMethodInterceptorImpl()
                }
                val damMethodInterceptor = SimpleDataAndMethod()
                assertEquals(DataAndMethodBaseImpl.initial, damMethodInterceptor.data)
                damMethodInterceptor.increase()
                assertEquals(
                    DataAndMethodBaseImpl.initial + DataAndMethodMethodInterceptorImpl.increase + DataAndMethodBaseImpl.increase,
                    damMethodInterceptor.data
                )
            }
            it("will not prohibit implementation property collision") {
                ImplementationRegistry.registerImplementation(DataAndMethod::class) {
                    DataAndMethodStateClashImpl()
                }
                val damStateClashInImplementations = SimpleDataAndMethod()
                assertEquals(DataAndMethodStateClashImpl.initial, damStateClashInImplementations.data)
                damStateClashInImplementations.increase()
                // because the state is colliding (both base impl and the StateClashInterceptor provide the data property),
                // both implementations will use their own data property.
                assertNotEquals(
                    DataAndMethodStateClashImpl.initial + DataAndMethodBaseImpl.increase,
                    damStateClashInImplementations.data
                )
                assertEquals(DataAndMethodStateClashImpl.initial, damStateClashInImplementations.data)
            }
            it("will not prohibit target class property collision") {
                ImplementationRegistry.registerImplementation(DataAndMethod::class) {
                    DataAndMethodBaseImpl()
                }
                val damStateClashInEndClass = DataAndMethodWithDataOverride()
                assertEquals(DataAndMethodWithDataOverride.initial, damStateClashInEndClass.data)
                damStateClashInEndClass.increase()
                // because the state is colliding (both base impl and the targe class DataAndMethodWithDataOverride provide the data property),
                // both implementations will use their own data property.
                assertNotEquals(
                    DataAndMethodWithDataOverride.initial + DataAndMethodBaseImpl.increase,
                    damStateClashInEndClass.data
                )
                assertEquals(DataAndMethodWithDataOverride.initial, damStateClashInEndClass.data)
            }
        }
    }
})
package symtek.mest.api.simulation.runtime

import io.mockk.every
import io.mockk.internalSubstitute
import io.mockk.mockk
import symtek.mest.api.component.*
import symtek.mest.api.simulation.*

class SimulationBuilder {
    private var environment : Environment = Environment()
    private val components  = mutableListOf<ComponentCandidateToComponent>()
    private val links = mutableListOf<LinkCandidateToLink>()
    private val simulationManager = SimulationManager()

    fun addComponent(component: Component, candidate: ComponentCandidate<*>? = null): SimulationBuilder {
        if (candidate != null) {
            components.add(
                ComponentCandidateToComponent(
                    componentCandidate = candidate,
                    componentInstance = component
            ))
        } else {
            val candidateToComponent = mockk<ComponentCandidateToComponent>() {
                every { componentInstance } returns component
            }
            components.add(candidateToComponent)
        }
        return this
    }

    fun connectLink(linkInstance: Link, linkCandidate: Link? = null): SimulationBuilder {
        //simulationManager.validateLinkConnectability(linkInstance)
        val componentA = linkInstance.portA.component
        val componentB = linkInstance.portB.component

        componentA.links.add(linkInstance)
        if (componentA != componentB) {
            componentB.links.add(linkInstance)
        }

        if (linkCandidate != null) {
            links.add(
                LinkCandidateToLink(
                    linkCandidate = linkCandidate,
                    linkInstance = linkInstance
                ))
        } else {
            val candidateToLink = mockk<LinkCandidateToLink>() {
                every { this@mockk.linkInstance } returns linkInstance
            }
            links.add(candidateToLink)
        }
        return this
    }

    fun getComponentByName(name: String): Component {
        return components.first { it.componentInstance.name == name }.componentInstance
    }
    fun getTestComponentByName(name: String): TestComponent {
        return components.first { it.componentInstance.name == name }.componentInstance as TestComponent
    }

    fun build() : Simulation {
        return Simulation(
            environment = environment,
            components = components,
            links = links
            )
    }
}

class TestComponent(
    name: String,
    configuration: ComponentConfiguration = ComponentConfiguration(),
    override val ports: MutableList<CommonPortInterface> = mutableListOf(),
    val tickLogic: () -> Unit = {},
    val outputRequestLogic: () -> Unit = {}
) : Component(name, Environment(), configuration = configuration){
    override fun tickTriggered() {
        tickLogic.invoke()
    }
    override fun outputTriggered() {
        outputRequestLogic.invoke()
    }

    fun getPortByName(name: String) : CommonPort<*> {
        return ports.first { it.name == name } as CommonPort<*>
    }
}
package symtek.mest.api.simulation.runtime

import org.spekframework.spek2.Spek
import org.junit.jupiter.api.Assertions.*
import org.spekframework.spek2.style.specification.describe
import symtek.mest.api.component.*
import symtek.mest.api.simulation.Simulation

internal class SchedulerCacheTest : Spek({
    describe("SchedulerCache") {

        /* Scenario A description:
            Component 1     Component 2     Component 4
                [c]             [d]            [eo]              +---+
                 ^               ^              ^                v   |
                 +-------+-------+              |           [c] [d] [i]
                         |                      |           Component 6
                        [i]                    [ei]         [eo]   [ei]
                    Component 3             Component 5
        */
        val scenarioABuilder: SimulationBuilder by memoized {
            SimulationBuilder()
                .addComponent(
                    // Component1
                    //   - 1 ContinuousOutputPort
                    TestComponent(
                        "c1"
                    ).apply {
                        ports.add(continuousOutputPort("c1_co1", initialValue = 0))
                    }
                )
                .addComponent(
                    // Component2
                    //   - 1 DiscreteOutputPort
                    TestComponent(
                        "c2"
                    ).apply {
                        ports.add(discreteOutputPort("c2_do1", initialValue = 0))
                    }
                )
                .addComponent(
                    // Component3
                    //   - 1 InputPort
                    TestComponent(
                        "c3"
                    ).apply {
                        ports.add(inputPort<Int>("c3_i1"))
                    }
                )
                .addComponent(
                    // Component4
                    //   - 1 EventOutputPort
                    TestComponent(
                        "c4"
                    ).apply {
                        ports.add(eventOutputPort<Int>("c4_eo1"))
                    }
                )
                .addComponent(
                    // Component5
                    //   - 1 EventInputPort
                    TestComponent(
                        "c5"
                    ).apply {
                        ports.add(eventInputPort<Int>("c5_ei1") {})
                    }
                )
                .addComponent(
                    // Component6
                    //   - 1 ContinuousOutputPort
                    //   - 1 DiscreteOutputPort
                    //   - 1 InputPort
                    //   - 1 EventOutputPort
                    //   - 1 EventInputPort
                    TestComponent(
                        "c6"
                    ).apply {
                        ports.add(continuousOutputPort("c6_co1", initialValue = 0))
                        ports.add(discreteOutputPort("c6_do1", initialValue = 0))
                        ports.add(inputPort<Int>("c6_i1"))
                        ports.add(eventOutputPort<Int>("c6_eo1"))
                        ports.add(eventInputPort<Int>("c6_ei1") {})
                    }
                )
                .apply {
                    // Component1:ContinuousOutputPort to Component3:InputPort
                    connectLink(
                        Link(
                            portA = getTestComponentByName("c1").getPortByName("c1_co1"),
                            portB = getTestComponentByName("c3").getPortByName("c3_i1")
                        )
                    )
                    // Component2:DiscreteOutputPort to Component3:InputPort
                    connectLink(
                        Link(
                            portA = getTestComponentByName("c2").getPortByName("c2_do1"),
                            portB = getTestComponentByName("c3").getPortByName("c3_i1")
                        )
                    )
                    // Component4:EventOutputPort to Component5:EventInputPort
                    connectLink(
                        Link(
                            portA = getTestComponentByName("c4").getPortByName("c4_eo1"),
                            portB = getTestComponentByName("c5").getPortByName("c5_ei1")
                        )
                    )
                    // Component6:DiscreteOutputPort to Component6:InputPort
                    connectLink(
                        Link(
                            portA = getTestComponentByName("c6").getPortByName("c6_do1"),
                            portB = getTestComponentByName("c6").getPortByName("c6_i1")
                        )
                    )
                }
        }
        val scenarioA: Simulation by memoized { scenarioABuilder.build() }
        val schedulerCacheA: SchedulerCache by memoized { SchedulerCache(scenarioA) }

        /* Scenario B description:
               (RA)                (ROR)              (ROR)
            Component 1  -->[c] Component 2  [d]<-- Component 3 [c]<-- Nothing
                [c]
                 ^
                 |                 (ROR)
            Component 4         Component 5 [c]<-+
               (RA)                 |            |
                                    +------------+
        */
        val scenarioBBuilder: SimulationBuilder by memoized {
            SimulationBuilder()
                .addComponent(
                    TestComponent(
                        name = "c1",
                        configuration = ComponentConfiguration(
                            continuousBehaviour = ContinuousBehaviour.RunAlways
                        )
                    ).apply {
                        ports.add(inputPort<Int>("i"))
                        ports.add(continuousOutputPort("co", initialValue = 0))
                    }
                )
                .addComponent(
                    TestComponent(
                        name = "c2",
                        configuration = ComponentConfiguration(
                            continuousBehaviour = ContinuousBehaviour.RunOnRequest
                        )
                    ).apply {
                        ports.add(continuousOutputPort("co", initialValue = 0))
                        ports.add(discreteOutputPort("do", initialValue = 0))
                    }
                )
                .addComponent(
                    TestComponent(
                        name = "c3",
                        configuration = ComponentConfiguration(
                            continuousBehaviour = ContinuousBehaviour.RunOnRequest
                        )
                    ).apply {
                        ports.add(inputPort<Int>("i"))
                        ports.add(continuousOutputPort("co", initialValue = 0))
                    }
                )
                .addComponent(
                    TestComponent(
                        name = "c4",
                        configuration = ComponentConfiguration(
                            continuousBehaviour = ContinuousBehaviour.RunAlways
                        )
                    ).apply {
                        ports.add(inputPort<Int>("i"))
                    }
                )
                .addComponent(
                    TestComponent(
                        name = "c5",
                        configuration = ComponentConfiguration(
                            continuousBehaviour = ContinuousBehaviour.RunOnRequest
                        )
                    ).apply {
                        ports.add(inputPort<Int>("i"))
                        ports.add(continuousOutputPort("co", initialValue = 0))
                    }
                )
                .apply {
                    // Component1:ContinuousOutputPort to Component4:InputPort
                    connectLink(
                        Link(
                            portA = getTestComponentByName("c1").getPortByName("co"),
                            portB = getTestComponentByName("c4").getPortByName("i")
                        )
                    )
                    // Component1:InputPort to Component2:ContinuousOutputPort
                    connectLink(
                        Link(
                            portA = getTestComponentByName("c1").getPortByName("i"),
                            portB = getTestComponentByName("c2").getPortByName("co")
                        )
                    )
                    // Component2:DiscreteOutputPort to Component3:InputPort
                    connectLink(
                        Link(
                            portA = getTestComponentByName("c2").getPortByName("do"),
                            portB = getTestComponentByName("c3").getPortByName("i")
                        )
                    )
                    // Component5:ContinuousOutputPort to Component5:InputPort
                    connectLink(
                        Link(
                            portA = getTestComponentByName("c5").getPortByName("co"),
                            portB = getTestComponentByName("c5").getPortByName("i")
                        )
                    )
                }
        }
        val scenarioB: Simulation by memoized { scenarioBBuilder.build() }
        val schedulerCacheB: SchedulerCache by memoized { SchedulerCache(scenarioB) }

        /**
        This part will test the [SchedulerCache.componentToAvailablePortTypesMap] which includes information about the available port types for a given Component.
         */
        describe("Cache 'componentToAvailablePortTypesMap'") {
            it("contains the same amount of components as the simulation") {
                assertEquals(scenarioA.components.size, schedulerCacheA.componentToAvailablePortTypesMap.keys.size)
            }
            it("has the correct port type list for a Component1 with a ContinuousOutputPort") {
                val component = scenarioABuilder.getTestComponentByName("c1")
                val expected = SchedulerCache.AvailablePortTypesForComponent(
                    hasContinuousOutputPort = true,
                    hasDiscreteOutputPort = false,
                    hasInputPort = false,
                    hasEventOutputPort = false,
                    hasEventInputPort = false
                )
                assertEquals(expected, schedulerCacheA.componentToAvailablePortTypesMap[component])
            }
            it("has the correct port type list for a Component2 with a DiscreteOutputPort") {
                val component = scenarioABuilder.getTestComponentByName("c2")
                val expected = SchedulerCache.AvailablePortTypesForComponent(
                    hasContinuousOutputPort = false,
                    hasDiscreteOutputPort = true,
                    hasInputPort = false,
                    hasEventOutputPort = false,
                    hasEventInputPort = false
                )
                assertEquals(expected, schedulerCacheA.componentToAvailablePortTypesMap[component])
            }
            it("has the correct port type list for a Component3 with a InputPort") {
                val component = scenarioABuilder.getTestComponentByName("c3")
                val expected = SchedulerCache.AvailablePortTypesForComponent(
                    hasContinuousOutputPort = false,
                    hasDiscreteOutputPort = false,
                    hasInputPort = true,
                    hasEventOutputPort = false,
                    hasEventInputPort = false
                )
                assertEquals(expected, schedulerCacheA.componentToAvailablePortTypesMap[component])
            }
            it("has the correct port type list for a Component4 with a EventOututPort") {
                val component = scenarioABuilder.getTestComponentByName("c4")
                val expected = SchedulerCache.AvailablePortTypesForComponent(
                    hasContinuousOutputPort = false,
                    hasDiscreteOutputPort = false,
                    hasInputPort = false,
                    hasEventOutputPort = true,
                    hasEventInputPort = false
                )
                assertEquals(expected, schedulerCacheA.componentToAvailablePortTypesMap[component])
            }
            it("has the correct port type list for a Component5 with a EventInputPort") {
                val component = scenarioABuilder.getTestComponentByName("c5")
                val expected = SchedulerCache.AvailablePortTypesForComponent(
                    hasContinuousOutputPort = false,
                    hasDiscreteOutputPort = false,
                    hasInputPort = false,
                    hasEventOutputPort = false,
                    hasEventInputPort = true
                )
                assertEquals(expected, schedulerCacheA.componentToAvailablePortTypesMap[component])
            }
            it("has the correct port type for a Component with multiple ports") {
                val component = scenarioABuilder.getTestComponentByName("c6")
                val expected = SchedulerCache.AvailablePortTypesForComponent(
                    hasContinuousOutputPort = true,
                    hasDiscreteOutputPort = true,
                    hasInputPort = true,
                    hasEventOutputPort = true,
                    hasEventInputPort = true
                )
                assertEquals(expected, schedulerCacheA.componentToAvailablePortTypesMap[component])
            }
        }
        describe("Cache 'availablePortTypeToComponentsMap'") {
            fun validate(components: List<Component>, portType: SchedulerCache.PortType) {
                assertEquals(components.size, schedulerCacheA.availablePortTypeToComponentsMap[portType]!!.size)
                components.forEach {
                    assertTrue(schedulerCacheA.availablePortTypeToComponentsMap[portType]!!.contains(it))
                }
            }

            it("Contains a list of components for all Port tpyes") {
                SchedulerCache.PortType.values().forEach {
                    assertNotNull(schedulerCacheA.availablePortTypeToComponentsMap[it])
                }
            }

            it("Contains all Components with a ContinuousOutputPort") {
                validate(
                    listOf(
                        scenarioABuilder.getComponentByName("c6"),
                        scenarioABuilder.getComponentByName("c1")
                    ), SchedulerCache.PortType.ContinuousOutputPort
                )
            }
            it("Contains all Components with a DiscreteOutputPort") {
                validate(
                    listOf(
                        scenarioABuilder.getComponentByName("c6"),
                        scenarioABuilder.getComponentByName("c2")
                    ), SchedulerCache.PortType.DiscreteOutputPort
                )
            }
            it("Contains all Components with a InputPort") {
                validate(
                    listOf(
                        scenarioABuilder.getComponentByName("c6"),
                        scenarioABuilder.getComponentByName("c3")
                    ), SchedulerCache.PortType.InputPort
                )
            }
            it("Contains all Components with a EventOutputPort") {
                validate(
                    listOf(
                        scenarioABuilder.getComponentByName("c6"),
                        scenarioABuilder.getComponentByName("c4")
                    ), SchedulerCache.PortType.EventOutputPort
                )
            }
            it("Contains all Components with a EventInputPort") {
                validate(
                    listOf(
                        scenarioABuilder.getComponentByName("c6"),
                        scenarioABuilder.getComponentByName("c5")
                    ), SchedulerCache.PortType.EventInputPort
                )
            }
        }
        describe("Cache 'portToLinkedPortsMap'") {
            fun validate(portA: CommonPort<*>, portB: List<CommonPort<*>>) {
                val linkedPorts = schedulerCacheA.portToLinkedPortsMap[portA]
                assertNotNull(linkedPorts)
                linkedPorts!!
                assertEquals(portB.size, linkedPorts.size)
                portB.forEach {
                    assertTrue(linkedPorts.contains(it))
                }
            }
            it("will contain a list for every port") {
                val totalPorts = scenarioA.components.sumBy {
                    it.componentInstance.ports.size
                }
                assertEquals(totalPorts, schedulerCacheA.portToLinkedPortsMap.keys.size)
            }
            it("contains connected ports as from portA to portB of the Link") {
                validate(
                    scenarioABuilder.getTestComponentByName("c1").getPortByName("c1_co1"), listOf(
                        scenarioABuilder.getTestComponentByName("c3").getPortByName("c3_i1")
                    )
                )
                validate(
                    scenarioABuilder.getTestComponentByName("c2").getPortByName("c2_do1"), listOf(
                        scenarioABuilder.getTestComponentByName("c3").getPortByName("c3_i1")
                    )
                )
                validate(
                    scenarioABuilder.getTestComponentByName("c4").getPortByName("c4_eo1"), listOf(
                        scenarioABuilder.getTestComponentByName("c5").getPortByName("c5_ei1")
                    )
                )
                validate(
                    scenarioABuilder.getTestComponentByName("c6").getPortByName("c6_do1"), listOf(
                        scenarioABuilder.getTestComponentByName("c6").getPortByName("c6_i1")
                    )
                )
            }
            it("contains connected ports as from portB to portA of the Link") {
                validate(
                    scenarioABuilder.getTestComponentByName("c3").getPortByName("c3_i1"), listOf(
                        scenarioABuilder.getTestComponentByName("c1").getPortByName("c1_co1"),
                        scenarioABuilder.getTestComponentByName("c2").getPortByName("c2_do1")
                    )
                )
                validate(
                    scenarioABuilder.getTestComponentByName("c5").getPortByName("c5_ei1"), listOf(
                        scenarioABuilder.getTestComponentByName("c4").getPortByName("c4_eo1")
                    )
                )
                validate(
                    scenarioABuilder.getTestComponentByName("c6").getPortByName("c6_i1"), listOf(
                        scenarioABuilder.getTestComponentByName("c6").getPortByName("c6_do1")
                    )
                )
            }

        }
        describe("Cache 'outputComponentToLinkedInputComponentsMap' & 'inputComponentToLinkedOutputComponentsMap'") {
            it("will answer with a hashmap for every component") {
                assertEquals(
                    scenarioA.components.size,
                    schedulerCacheA.outputComponentToLinkedInputComponentsMap.keys.size
                )
                assertEquals(
                    scenarioA.components.size,
                    schedulerCacheA.inputComponentToLinkedOutputComponentsMap.keys.size
                )
            }

            fun validate(
                map: Map<Component, HashMap<Component, SchedulerCache.ComponentLinkInformation>>,
                component: Component,
                connectedComponents: Map<Component, SchedulerCache.ComponentLinkInformation>
            ) {
                assertEquals(connectedComponents.size, map[component]!!.keys.size)
                connectedComponents.forEach {
                    assertTrue(map[component]!!.keys.contains(it.key))
                    assertNotNull(map[component]!![it.key])
                    assertEquals(it.value, map[component]!![it.key])
                }
            }

            it("contains all connected components for input components") {
                validate(
                    schedulerCacheA.inputComponentToLinkedOutputComponentsMap,
                    scenarioABuilder.getComponentByName("c3"),
                    mapOf(
                        scenarioABuilder.getComponentByName("c1") to SchedulerCache.ComponentLinkInformation(
                            hasContinuousLink = true,
                            hasDiscreteLink = false,
                            hasEventLink = false
                        ),
                        scenarioABuilder.getComponentByName("c2") to SchedulerCache.ComponentLinkInformation(
                            hasContinuousLink = false,
                            hasDiscreteLink = true,
                            hasEventLink = false
                        )
                    )
                )
                validate(
                    schedulerCacheA.inputComponentToLinkedOutputComponentsMap,
                    scenarioABuilder.getComponentByName("c5"),
                    mapOf(
                        scenarioABuilder.getComponentByName("c4") to SchedulerCache.ComponentLinkInformation(
                            hasContinuousLink = false,
                            hasDiscreteLink = false,
                            hasEventLink = true
                        )
                    )
                )
                validate(
                    schedulerCacheA.inputComponentToLinkedOutputComponentsMap,
                    scenarioABuilder.getComponentByName("c6"),
                    mapOf(
                        scenarioABuilder.getComponentByName("c6") to SchedulerCache.ComponentLinkInformation(
                            hasContinuousLink = false,
                            hasDiscreteLink = true,
                            hasEventLink = false
                        )
                    )
                )
            }
            it("contains all connected components for output components") {
                validate(
                    schedulerCacheA.outputComponentToLinkedInputComponentsMap,
                    scenarioABuilder.getComponentByName("c1"),
                    mapOf(
                        scenarioABuilder.getComponentByName("c3") to SchedulerCache.ComponentLinkInformation(
                            hasContinuousLink = true,
                            hasDiscreteLink = false,
                            hasEventLink = false
                        )
                    )
                )
                validate(
                    schedulerCacheA.outputComponentToLinkedInputComponentsMap,
                    scenarioABuilder.getComponentByName("c2"),
                    mapOf(
                        scenarioABuilder.getComponentByName("c3") to SchedulerCache.ComponentLinkInformation(
                            hasContinuousLink = false,
                            hasDiscreteLink = true,
                            hasEventLink = false
                        )
                    )
                )
                validate(
                    schedulerCacheA.outputComponentToLinkedInputComponentsMap,
                    scenarioABuilder.getComponentByName("c4"),
                    mapOf(
                        scenarioABuilder.getComponentByName("c5") to SchedulerCache.ComponentLinkInformation(
                            hasContinuousLink = false,
                            hasDiscreteLink = false,
                            hasEventLink = true
                        )
                    )
                )
                validate(
                    schedulerCacheA.outputComponentToLinkedInputComponentsMap,
                    scenarioABuilder.getComponentByName("c6"),
                    mapOf(
                        scenarioABuilder.getComponentByName("c6") to SchedulerCache.ComponentLinkInformation(
                            hasContinuousLink = false,
                            hasDiscreteLink = true,
                            hasEventLink = false
                        )
                    )
                )
            }
        }
        describe("Cache 'portTypeToPortsMap'") {
            fun validate(portType: SchedulerCache.PortType) {
                val expectedPortList = mutableListOf<CommonPort<*>>()
                scenarioA.components.forEach {
                    it.componentInstance.ports.forEach {
                        if (portType == SchedulerCache.PortType.ContinuousOutputPort && it is ContinuousOutputPort<*>) {
                            expectedPortList.add(it)
                        } else if (portType == SchedulerCache.PortType.DiscreteOutputPort && it is DiscreteOutputPort<*>) {
                            expectedPortList.add(it)
                        } else if (portType == SchedulerCache.PortType.InputPort && it is InputPort<*>) {
                            expectedPortList.add(it)
                        } else if (portType == SchedulerCache.PortType.EventOutputPort && it is EventOutputPort<*>) {
                            expectedPortList.add(it)
                        } else if (portType == SchedulerCache.PortType.EventInputPort && it is EventInputPort<*>) {
                            expectedPortList.add(it)
                        }
                    }
                }
                val portsForPortType = schedulerCacheA.portTypeToPortsMap[portType]!!
                assertEquals(expectedPortList.size, portsForPortType.size)
                expectedPortList.forEach {
                    assertTrue(portsForPortType.contains(it))
                }
            }
            it("contains all ContinuousOutputPorts") {
                validate(SchedulerCache.PortType.ContinuousOutputPort)
            }
            it("contains all DiscreteOutputPorts") {
                validate(SchedulerCache.PortType.DiscreteOutputPort)
            }
            it("contains all InputPorts") {
                validate(SchedulerCache.PortType.InputPort)
            }
            it("contains all EventOutputPorts") {
                validate(SchedulerCache.PortType.EventOutputPort)
            }
            it("contains all EventInputPorts") {
                validate(SchedulerCache.PortType.EventInputPort)
            }

        }
        describe("Cache 'runOnRequestComponentWithContinuousOutputPortToLinkedComponentsMap'") {
            val rormap by memoized { schedulerCacheB.runOnRequestComponentWithContinuousOutputPortToLinkedComponentsMap }
            it("will only contain Components with the RunOnRequest continuous behaviour configuration and at least one continuous output port.") {
                val scenarioBComponentsWithRunOnRequest = listOf(
                    scenarioBBuilder.getComponentByName("c2"),
                    scenarioBBuilder.getComponentByName("c3"),
                    scenarioBBuilder.getComponentByName("c5")
                )
                assertEquals(scenarioBComponentsWithRunOnRequest.size, rormap.keys.size)
                scenarioBComponentsWithRunOnRequest.forEach {
                    assertTrue(rormap.keys.contains(it))
                }
            }
            it("will contain an empty list for Component 3 because no one will read it.") {
                assertEquals(0, rormap[scenarioBBuilder.getComponentByName("c3")]!!.size)
            }
            it("will contain a list for Component 2 containing Component 1 only, because Component 3 will only read the discrete output port.") {
                val c2List = rormap[scenarioBBuilder.getComponentByName("c2")]!!
                assertEquals(1, c2List.size)
                assertTrue(c2List.contains(scenarioBBuilder.getComponentByName("c1")))
            }
            it("will contain a list for Component 5 containing Component 5 because it will read itself.") {
                val c5List = rormap[scenarioBBuilder.getComponentByName("c5")]!!
                assertEquals(1, c5List.size)
                assertTrue(c5List.contains(scenarioBBuilder.getComponentByName("c5")))
            }
        }
    }
})
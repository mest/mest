package symtek.mest.api.simulation.runtime

import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.*
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import symtek.mest.api.component.*
import symtek.mest.api.simulation.ComponentCandidateToComponent
import symtek.mest.api.simulation.Environment
import symtek.mest.api.simulation.Simulation
import symtek.mest.api.simulation.Timestamp
import java.time.Duration


fun modifyTimestamp(simulation: Simulation, duration: Duration) {
    val timestamp = simulation.environment.timestamp as Timestamp
    timestamp.currentTick += 1
    timestamp.currentTime = duration
}

fun setNextExecutionTimeForAllByMultiple(
    simulation: Simulation,
    componentToNextExecutionTime: MutableMap<Component, Duration?>,
    multiple: Long = 0
) {
    componentToNextExecutionTime.clear()
    simulation.components.forEach {
        val component = it.componentInstance
        val period = component.configuration.periodDuration
        val newPeriod: Duration? = period?.multipliedBy(multiple)
        componentToNextExecutionTime[component] = newPeriod
    }
}


internal class StepCacheTest : Spek({
    describe("StepCache") {
        context("Helper methods") {
            describe("wasExecuted(component)") {

            }
        }
        context("Cache lists") {

            /**
             *
             *  # Component to test the eventHandlerLogicsToBeExecuted list.
             *
             *  Component 1 (period=2s)
             *  ei1  ei2 eo1
             *   ^    ^   |
             *   +----+---+
             *
             *  # Components for the outputRequestLogicsToBeExecuted
             *
             *  ## Component 2 is not connected
             *
             *  (RunOnRequest)
             *  Component 2
             *      co1
             *
             *  ## Component 3 is connected to itself
             *
             *  (RunOnRequest)
             *  Component 3
             *   co1  i1
             *    |   ^
             *    +---+
             *
             *  ## Component 4 will always be executed
             *  ## Component 5 will be read by a component that has at least one continuous output port.
             *
             *  (RunAlways)
             *  Component 4
             *   co1  i1
             *        ^
             *        |
             *        co1
             *  Component 5
             *  (RunOnRequest)
             *
             *  ## Component 6 will be read by Component 7, that is executed periodically
             *
             *  (RunOnRequest)
             *  Component 6
             *      co1
             *       |
             *       v
             *       i1
             *   Component 7
             *   (period=1s)
             *
             *  ## Component 8 will be read by Component 9 that received an event
             *
             *  (RunOnRequest)
             *  Component 8
             *      co1
             *       |
             *       v
             *       i1
             *   Component 9 (period=1s)
             *    eo1  ei1
             *     |    ^
             *     +----+
             *
             *   ## Component 10 is connected to an period component that is not executed in this step
             *
             *  (RunOnRequest)
             *  Component 10
             *      co1
             *       |
             *       v
             *       i1
             *   Component 11
             *   (period=200s)
             *
             *
             *   ## Component 12 without continuous output port
             *
             *   (RunOnRequest)
             *   Component 12
             *
             */

            val simulationBuilderA by memoized {
                SimulationBuilder()
                    .addComponent(
                        TestComponent(
                            "c1",
                            configuration = ComponentConfiguration(
                                periodDuration = Duration.ofSeconds(2)
                            )
                        ).apply {
                            ports.addAll(
                                listOf(
                                    eventInputPort<Int>("ei1") {},
                                    eventInputPort<Int>("ei2") {},
                                    eventOutputPort<Int>("eo1")
                                )
                            )
                        }
                    ).apply {
                        connectLink(
                            Link(
                                getTestComponentByName("c1").getPortByName("ei1"),
                                getTestComponentByName("c1").getPortByName("eo1")
                            )
                        )
                        connectLink(
                            Link(
                                getTestComponentByName("c1").getPortByName("ei2"),
                                getTestComponentByName("c1").getPortByName("eo1")
                            )
                        )
                    }

                    .addComponent(
                        TestComponent(
                            "c2",
                            configuration = ComponentConfiguration(continuousBehaviour = ContinuousBehaviour.RunOnRequest)
                        ).apply {
                            ports.addAll(
                                listOf(
                                    continuousOutputPort<Int>("co1", initialValue = 0)
                                )
                            )
                        }
                    )


                    .addComponent(
                        TestComponent(
                            "c3",
                            configuration = ComponentConfiguration(continuousBehaviour = ContinuousBehaviour.RunOnRequest)
                        ).apply {
                            ports.addAll(
                                listOf(
                                    continuousOutputPort<Int>("co1", initialValue = 0),
                                    inputPort<Int>("i1")
                                )
                            )
                        }
                    ).apply {
                        connectLink(
                            Link(
                                getTestComponentByName("c3").getPortByName("co1"),
                                getTestComponentByName("c3").getPortByName("i1")
                            )
                        )
                    }


                    .addComponent(
                        TestComponent(
                            "c4",
                            configuration = ComponentConfiguration(continuousBehaviour = ContinuousBehaviour.RunAlways)
                        ).apply {
                            ports.addAll(
                                listOf(
                                    continuousOutputPort<Int>("co1", initialValue = 0),
                                    inputPort<Int>("i1")
                                )
                            )
                        }
                    )
                    .addComponent(
                        TestComponent(
                            "c5",
                            configuration = ComponentConfiguration(continuousBehaviour = ContinuousBehaviour.RunOnRequest)
                        ).apply {
                            ports.addAll(
                                listOf(
                                    continuousOutputPort<Int>("co1", initialValue = 0)
                                )
                            )
                        }
                    ).apply {
                        connectLink(
                            Link(
                                getTestComponentByName("c4").getPortByName("i1"),
                                getTestComponentByName("c5").getPortByName("co1")
                            )
                        )
                    }


                    .addComponent(
                        TestComponent(
                            "c6",
                            configuration = ComponentConfiguration(continuousBehaviour = ContinuousBehaviour.RunOnRequest)
                        ).apply {
                            ports.addAll(
                                listOf(
                                    continuousOutputPort<Int>("co1", initialValue = 0)
                                )
                            )
                        }
                    )
                    .addComponent(
                        TestComponent(
                            "c7",
                            configuration = ComponentConfiguration(periodDuration = Duration.ofSeconds(1))
                        ).apply {
                            ports.addAll(
                                listOf(
                                    inputPort<Int>("i1")
                                )
                            )
                        }
                    ).apply {
                        connectLink(
                            Link(
                                getTestComponentByName("c6").getPortByName("co1"),
                                getTestComponentByName("c7").getPortByName("i1")
                            )
                        )
                    }


                    .addComponent(
                        TestComponent(
                            "c8",
                            configuration = ComponentConfiguration(continuousBehaviour = ContinuousBehaviour.RunOnRequest)
                        ).apply {
                            ports.addAll(
                                listOf(
                                    continuousOutputPort<Int>("co1", initialValue = 0)
                                )
                            )
                        }
                    )
                    .addComponent(
                        TestComponent(
                            "c9",
                            configuration = ComponentConfiguration(periodDuration = Duration.ofSeconds(1))
                        ).apply {
                            ports.addAll(listOf(
                                inputPort<Int>("i1"),
                                eventOutputPort<Int>("eo1"),
                                eventInputPort<Int>("ei1") {}
                            ))
                        }
                    ).apply {
                        connectLink(
                            Link(
                                getTestComponentByName("c8").getPortByName("co1"),
                                getTestComponentByName("c9").getPortByName("i1")
                            )
                        )
                        connectLink(
                            Link(
                                getTestComponentByName("c9").getPortByName("ei1"),
                                getTestComponentByName("c9").getPortByName("eo1")
                            )
                        )
                    }


                    .addComponent(
                        TestComponent(
                            "c10",
                            configuration = ComponentConfiguration(continuousBehaviour = ContinuousBehaviour.RunOnRequest)
                        ).apply {
                            ports.addAll(
                                listOf(
                                    continuousOutputPort<Int>("co1", initialValue = 0)
                                )
                            )
                        }
                    )
                    .addComponent(
                        TestComponent(
                            "c11",
                            configuration = ComponentConfiguration(periodDuration = Duration.ofSeconds(200))
                        ).apply {
                            ports.addAll(
                                listOf(
                                    inputPort<Int>("i1")
                                )
                            )
                        }
                    ).apply {
                        connectLink(
                            Link(
                                getTestComponentByName("c10").getPortByName("co1"),
                                getTestComponentByName("c11").getPortByName("i1")
                            )
                        )
                    }


                    .addComponent(
                        TestComponent(
                            "c12",
                            configuration = ComponentConfiguration(continuousBehaviour = ContinuousBehaviour.RunOnRequest)
                        )
                    )
            }
            val simulationA by memoized { simulationBuilderA.build() }
            val schedulerCacheA by memoized { SchedulerCache(simulationA) }
            val componentToNextExecutionTimeA by memoized { mutableMapOf<Component, Duration?>() }
            val stepCacheA by memoized { StepCache(simulationA, schedulerCacheA, componentToNextExecutionTimeA) }

            /**
             * This list will contain each component of the componentToNextExecutionTime map for which the next execution time is matching the simulation time.
             */
            context("step cache list: tickLogicsToBeExecuted") {

                //TODO: Remove these variables and reuse an existing simulation!
                val myTimestamp = Timestamp(0, Duration.ZERO)
                val myEnvironment = mockk<Environment> {
                    every { timestamp } returns myTimestamp
                }
                val c1 = mockk<Component> {
                    every { ports } returns emptyList()
                    every { configuration } returns ComponentConfiguration()
                }
                val c2 = mockk<Component> {
                    every { ports } returns emptyList()
                    every { configuration } returns ComponentConfiguration()
                }
                val c3 = mockk<Component> {
                    every { ports } returns emptyList()
                    every { configuration } returns ComponentConfiguration()
                }
                val c4 = mockk<Component> {
                    every { ports } returns emptyList()
                    every { configuration } returns ComponentConfiguration()
                }
                val simulation = mockk<Simulation> {
                    every { environment } returns myEnvironment
                    every { components } returns listOf<ComponentCandidateToComponent>(
                        ComponentCandidateToComponent(mockk(), c1),
                        ComponentCandidateToComponent(mockk(), c2),
                        ComponentCandidateToComponent(mockk(), c3),
                        ComponentCandidateToComponent(mockk(), c4)
                    )
                    every { links } returns emptyList()
                }

                val componentToNextExecutionTime = mutableMapOf<Component, Duration?>(
                    c1 to null,
                    c2 to Duration.ZERO,
                    c3 to Duration.ZERO
                )

                val stepCache = StepCache(simulation, SchedulerCache(simulation), componentToNextExecutionTime)

                /**
                 * Lets say the current simulation time is 1 second.
                 * The first component is not periodically executed (or disabled) and its next execution time is zero.
                 * The second component has the next execution time set to 1s -> should be executed.
                 * The second component has the next execution time set to 2s -> should not be executed.
                 */
                it("contains only components whose next execution time matches the current time") {
                    componentToNextExecutionTime[c2] = Duration.ofSeconds(1)
                    componentToNextExecutionTime[c3] = Duration.ofSeconds(2)
                    myTimestamp.currentTime = Duration.ofSeconds(1)
                    stepCache.updateCache()

                    assertEquals(1, stepCache.tickLogicsToBeExecuted.size)
                    assertTrue(stepCache.tickLogicsToBeExecuted.contains(c2))
                }

            }
            context("step cache list: eventHandlerLogicsToBeExecuted") {
                val c1 by memoized { simulationBuilderA.getTestComponentByName("c1") }
                val ei1 by memoized { c1.getPortByName("ei1") as EventInputPort }
                val ei2 by memoized { c1.getPortByName("ei2") as EventInputPort }

                it("will not contain ports from components whose next execution time is unequal to the current time") {
                    modifyTimestamp(simulationA, Duration.ofSeconds(1))
                    stepCacheA.updateCache()

                    assertFalse(stepCacheA.eventHandlerLogicsToBeExecuted.contains(ei1))
                    assertFalse(stepCacheA.eventHandlerLogicsToBeExecuted.contains(ei2))
                }
                context("Ports of components whose next execution time equal to now ...") {
                    beforeEachTest {
                        modifyTimestamp(simulationA, Duration.ofSeconds(2))
                        setNextExecutionTimeForAllByMultiple(simulationA, componentToNextExecutionTimeA, multiple = 1)
                        ei2.events.add(1)
                        stepCacheA.updateCache()
                    }
                    it("... will not be added if they have no events in their queue") {
                        assertFalse(stepCacheA.eventHandlerLogicsToBeExecuted.contains(ei1))
                    }
                    it("... will be added if they have at least one event in their queue") {
                        println(stepCacheA.simulation.environment.timestamp)
                        assertTrue(stepCacheA.eventHandlerLogicsToBeExecuted.contains(ei2))
                    }
                }

            }
            context("step cache list: outputRequestLogicsToBeExecuted") {

                val c2 by memoized { simulationBuilderA.getTestComponentByName("c2") }
                val c3 by memoized { simulationBuilderA.getTestComponentByName("c3") }
                val c4 by memoized { simulationBuilderA.getTestComponentByName("c4") }
                val c5 by memoized { simulationBuilderA.getTestComponentByName("c5") }
                val c6 by memoized { simulationBuilderA.getTestComponentByName("c6") }
                val c7 by memoized { simulationBuilderA.getTestComponentByName("c7") }
                val c8 by memoized { simulationBuilderA.getTestComponentByName("c8") }
                val c9 by memoized { simulationBuilderA.getTestComponentByName("c9") }
                val c10 by memoized { simulationBuilderA.getTestComponentByName("c10") }
                val c11 by memoized { simulationBuilderA.getTestComponentByName("c11") }
                val c12 by memoized { simulationBuilderA.getTestComponentByName("c12") }


                it("will not contain a component without a continuous output port") {
                    assertFalse(stepCacheA.outputRequestLogicsToBeExecuted.contains(c12))
                }
                context("Components with 'RunAlways' behaviour") {
                    it("will always be added") {
                        assertTrue(stepCacheA.outputRequestLogicsToBeExecuted.contains(c4))
                    }
                }
                context("Components with 'RunOnRequest' behaviour ...") {

                    beforeEachTest {
                        modifyTimestamp(simulationA, Duration.ofSeconds(1))
                        setNextExecutionTimeForAllByMultiple(simulationA, componentToNextExecutionTimeA, 1)
                        stepCacheA.updateCache()
                    }

                    it("... will be added if one of the reading components is itself") {
                        assertTrue(stepCacheA.outputRequestLogicsToBeExecuted.contains(c3))
                    }
                    it("... will be added if one of the reading components is in the tickLogicsToBeExecuted list (will be executed in this step)") {
                        assertTrue(stepCacheA.tickLogicsToBeExecuted.contains(c7))
                        assertTrue(stepCacheA.outputRequestLogicsToBeExecuted.contains(c6))
                    }
                    it("... will be added if one of the reading components is in the eventHandlerLogicsToBeExecuted list (will be executed in this step)") {
                        val ei1 = c9.getPortByName("ei1") as EventInputPort
                        ei1.events.add(1)
                        stepCacheA.updateCache()
                        assertTrue(stepCacheA.eventHandlerLogicsToBeExecuted.contains(ei1))
                        assertTrue(stepCacheA.outputRequestLogicsToBeExecuted.contains(c8))
                    }
                    it("... will be added if one of the reading components has a continuous output port, too") {
                        assertTrue(stepCacheA.outputRequestLogicsToBeExecuted.contains(c5))
                    }
                    it("will not be added if none of the reading components conditions are matching (the ones mentioned above)") {
                        // here: Component 10, where the tickLogicsToBeExecuted list could lead to an output request but the current time doesn't match the connected time of 200s
                        assertFalse(stepCacheA.outputRequestLogicsToBeExecuted.contains(c10))
                    }
                }
            }
        }
    }
})
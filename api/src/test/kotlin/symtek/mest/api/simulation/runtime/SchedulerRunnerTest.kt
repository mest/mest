package symtek.mest.api.simulation.runtime

import io.mockk.every
import io.mockk.mockk
import org.spekframework.spek2.Spek

import org.junit.jupiter.api.Assertions.*
import org.spekframework.spek2.style.specification.describe
import symtek.mest.api.component.Component
import symtek.mest.api.component.ComponentConfiguration
import symtek.mest.api.component.ContinuousBehaviour
import symtek.mest.api.component.EventInputPort
import java.time.Duration

enum class ExecutionType {
    TICK, OUTPUT_REQUEST, EVENT
}

internal class SchedulerRunnerTest : Spek({
    describe("SchedulerRunner") {

        /**
         * Component 1 (period=null)
         * Component 2 (period=2s)
         * Component 3 (period=5s)
         */
        val simulationBuilder by memoized {
            SimulationBuilder()
                .addComponent(
                    TestComponent(
                        "c1",
                        configuration = ComponentConfiguration(
                            periodDuration = null,
                            continuousBehaviour = ContinuousBehaviour.RunOnRequest
                        )
                    ).apply {
                        ports.addAll(listOf(
                            continuousOutputPort("co1", initialValue = 0),
                            eventInputPort<Int>("ei1") {}
                        ))
                    }
                )
                .addComponent(
                    TestComponent(
                        "c2",
                        configuration = ComponentConfiguration(
                            periodDuration = Duration.ofSeconds(2),
                            continuousBehaviour = ContinuousBehaviour.RunOnRequest
                        )
                    ).apply {
                        ports.addAll(listOf(
                            continuousOutputPort("co1", initialValue = 0),
                            eventInputPort<Int>("ei1") {}
                        ))
                    }
                )
                .addComponent(
                    TestComponent(
                        "c3",
                        configuration = ComponentConfiguration(
                            periodDuration = Duration.ofSeconds(5),
                            continuousBehaviour = ContinuousBehaviour.RunOnRequest
                        )
                    ).apply {
                        ports.addAll(listOf(
                            continuousOutputPort("co1", initialValue = 0),
                            eventInputPort<Int>("ei1") {}
                        ))
                    }
                )
        }
        val simulation by memoized { simulationBuilder.build() }
        val schedulerCache by memoized { SchedulerCache(simulation) }
        val schedulerConfiguration by memoized { SchedulerConfiguration() }
        val schedulerRunner by memoized { SchedulerRunner(simulation, schedulerCache, schedulerConfiguration) }

        val c1 by memoized { simulationBuilder.getTestComponentByName("c1") }
        val c2 by memoized { simulationBuilder.getTestComponentByName("c2") }
        val c3 by memoized { simulationBuilder.getTestComponentByName("c3") }

        it("will populate the next execution time for component map with 0s for all components with a duration and null otherwise.") {
            val map = schedulerRunner.componentToNextExecutionTime
            assertNull(map[c1])
            assertNotNull(map[c2])
            assertNotNull(map[c3])
            assertTrue(map[c2] == Duration.ZERO)
            assertTrue(map[c3] == Duration.ZERO)
        }

        context("Step increment") {
            it("will always increase the step by 1") {
                val timestamp = simulation.environment.timestamp
                for (i in 0L..10L) {
                    assertTrue(i == timestamp.currentTick)
                    schedulerRunner.executeStep()
                }
            }
            context("No component with duration") {
                it("will increase the simulation time by the value of the scheduler configuration") {
                    val sim = SimulationBuilder().build()
                    val schedulerConf = SchedulerConfiguration()
                    val runner = SchedulerRunner(sim, SchedulerCache(sim), schedulerConf)

                    val timestamp = sim.environment.timestamp

                    var expectedTime = Duration.ZERO
                    assertTrue(expectedTime == timestamp.currentTime)
                    runner.executeStep()
                    expectedTime += schedulerConf.defaultIncrementTime
                    assertTrue(expectedTime == timestamp.currentTime)
                    schedulerConf.defaultIncrementTime = Duration.ofSeconds(500)
                    expectedTime += schedulerConf.defaultIncrementTime
                    runner.executeStep()
                    assertTrue(expectedTime == timestamp.currentTime)
                }
            }
            context("Components with duration") {
                it("will increase the simulation time to the nearest execution time") {
                    val timestamp = simulation.environment.timestamp
                    assertTrue(Duration.ofSeconds(0) == timestamp.currentTime)
                    schedulerRunner.executeStep()
                    assertTrue(Duration.ofSeconds(2) == timestamp.currentTime)
                    schedulerRunner.executeStep()
                    assertTrue(Duration.ofSeconds(4) == timestamp.currentTime)
                    schedulerRunner.executeStep()
                    assertTrue(Duration.ofSeconds(5) == timestamp.currentTime)
                    schedulerRunner.executeStep()
                    assertTrue(Duration.ofSeconds(6) == timestamp.currentTime)
                }
                it("will recalculate the correct next execution time for the components") {
                    val map = schedulerRunner.componentToNextExecutionTime
                    schedulerRunner.executeStep()
                    assertTrue(Duration.ofSeconds(2) == map[c2])
                    assertTrue(Duration.ofSeconds(5) == map[c3])
                    schedulerRunner.executeStep()
                    assertTrue(Duration.ofSeconds(4) == map[c2])
                    assertTrue(Duration.ofSeconds(5) == map[c3])
                    schedulerRunner.executeStep()
                    assertTrue(Duration.ofSeconds(6) == map[c2])
                    assertTrue(Duration.ofSeconds(5) == map[c3])
                    schedulerRunner.executeStep()
                    assertTrue(Duration.ofSeconds(6) == map[c2])
                    assertTrue(Duration.ofSeconds(10) == map[c3])
                    schedulerRunner.executeStep()
                    assertTrue(Duration.ofSeconds(8) == map[c2])
                    assertTrue(Duration.ofSeconds(10) == map[c3])
                }
            }
        }
        context("Next execution time calculation") {
            beforeEachTest {
                schedulerRunner.executeStep() // initial run
            }
            it("will never update the next execution time for a component that wasn't executed in this step") {
                val nextExecutionTimeForC3 = schedulerRunner.componentToNextExecutionTime[c3]
                assertNotNull(nextExecutionTimeForC3)
                schedulerRunner.executeStep() // first run
                assertFalse(schedulerRunner.stepCache.wasExecuted(c3)) // just to be sure it was not executed
                assertEquals(nextExecutionTimeForC3, schedulerRunner.componentToNextExecutionTime[c3])
            }
            it("will never update the next execution time for a component that wasn't executed in this step even if it's period duration changed") {
                val nextExecutionTimeForC1 = schedulerRunner.componentToNextExecutionTime[c1]
                assertNull(nextExecutionTimeForC1)
                c1.configuration.periodDuration = Duration.ofSeconds(20)
                schedulerRunner.executeStep() // first run
                assertFalse(schedulerRunner.stepCache.wasExecuted(c1)) // just to be sure it was not executed
                assertEquals(nextExecutionTimeForC1, schedulerRunner.componentToNextExecutionTime[c1])
            }
            context("Component was somehow executed") {
                fun markComponentAsExecuted(component: Component, type: ExecutionType) {
                    when (type) {
                        ExecutionType.TICK -> schedulerRunner.stepCache.tickLogicsToBeExecuted.add(component)
                        ExecutionType.OUTPUT_REQUEST -> schedulerRunner.stepCache.outputRequestLogicsToBeExecuted.add(
                            component
                        )
                        ExecutionType.EVENT -> schedulerRunner.stepCache.eventHandlerLogicsToBeExecuted.add(
                            (component as TestComponent).getPortByName(
                                "ei1"
                            ) as EventInputPort<*>
                        )
                    }
                }
                context("Component doesn't have a scheduled next execution time yet") {
                    fun testForExecutionType(type: ExecutionType) {
                        val newPeriodDuration = Duration.ofSeconds(15)
                        val nextExecutionTimeForC1 = schedulerRunner.componentToNextExecutionTime[c1]
                        assertNull(nextExecutionTimeForC1) // just to be sure
                        schedulerRunner.executeStep() // first run
                        val timeAfterFirstRun = simulation.environment.timestamp.currentTime
                        c1.configuration.periodDuration = newPeriodDuration
                        markComponentAsExecuted(c1, type)
                        schedulerRunner.executeStep() // second run

                        val newExecutionTimeForC1 = timeAfterFirstRun + newPeriodDuration
                        assertEquals(newExecutionTimeForC1, schedulerRunner.componentToNextExecutionTime[c1])
                    }

                    it("it will receive a new execution time if its period duration was set and Component tick logic was executed") {
                        testForExecutionType(ExecutionType.TICK)
                    }
                    it("it will receive a new execution time if its period duration was set and Component output request logic was executed") {
                        testForExecutionType(ExecutionType.OUTPUT_REQUEST)
                    }
                    it("it will receive a new execution time if its period duration was set and Component event handler logic was executed") {
                        testForExecutionType(ExecutionType.EVENT)
                    }
                }
                context("Component already has a scheduled execution time") {
                    fun testPeriodDurationRemove(type: ExecutionType) {
                        val nextExecutionTimeForC3 = schedulerRunner.componentToNextExecutionTime[c3]
                        assertNotNull(nextExecutionTimeForC3)

                        schedulerRunner.executeStep() // first run
                        assertFalse(schedulerRunner.stepCache.wasExecuted(c3)) // just to be sure
                        markComponentAsExecuted(c3, type)
                        c3.configuration.periodDuration = null
                        schedulerRunner.executeStep()
                        assertNull(schedulerRunner.componentToNextExecutionTime[c3])
                    }
                    it("will remove the scheduled execution time if the period duration was removed (tick logic executed)") {
                        testPeriodDurationRemove(ExecutionType.TICK)
                    }
                    it("will remove the scheduled execution time if the period duration was removed (output request logic executed)") {
                        testPeriodDurationRemove(ExecutionType.OUTPUT_REQUEST)
                    }
                    it("will remove the scheduled execution time if the period duration was removed (event handler logic executed)") {
                        testPeriodDurationRemove(ExecutionType.EVENT)
                    }

                    fun testChangedPeriodDuration(type: ExecutionType, shouldBeChanged: Boolean) {
                        val newPeriodDuration = Duration.ofSeconds(30)
                        val nextExecutionTimeForC3 = schedulerRunner.componentToNextExecutionTime[c3]
                        assertNotNull(nextExecutionTimeForC3)

                        schedulerRunner.executeStep() // first run
                        c3.configuration.periodDuration = newPeriodDuration
                        markComponentAsExecuted(c3, type)
                        val newExecutionTime = simulation.environment.timestamp.currentTime + newPeriodDuration
                        schedulerRunner.executeStep() // second run
                        if (shouldBeChanged) {
                            assertEquals(newExecutionTime, schedulerRunner.componentToNextExecutionTime[c3])
                        } else {
                            assertEquals(nextExecutionTimeForC3, schedulerRunner.componentToNextExecutionTime[c3])
                        }
                    }

                    it("will not receive a new execution time if the component was not ticked (only event handler) in the last step (even if the period has changed)") {
                        testChangedPeriodDuration(ExecutionType.OUTPUT_REQUEST, false)
                    }
                    it("will not receive a new execution time if the component was not ticked (only output request) in the last step (even if the period has changed)") {
                        testChangedPeriodDuration(ExecutionType.EVENT, false)
                    }
                    it("will receive a new execution time if the component was ticked in the last step") {
                        testChangedPeriodDuration(ExecutionType.TICK, true)
                    }
                }
            }
        }
    }
})
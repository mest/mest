package symtek.mest.project.schema

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import symtek.mest.project.BaseProjectDAO
import symtek.mest.project.serializer.DurationSerializer
import java.time.Duration

class ProjectSchemav1 {
    @Serializable
    @SerialName("v1")
    data class Project(
        val projectName: String,
        val components: List<Component>,
        val links: List<Link>,
        val simulationProfiles: List<SimulationProfile>,
        val activeSimulationProfileId: String?
    ) : BaseProjectDAO()

    @Serializable
    data class Component(
        val name: String,
        val id: String,
        val componentClass: String,
        val position: ComponentPosition,
        val minimized: Boolean
    )

    @Serializable
    data class ComponentPosition(val x: Double, val y: Double)

    @Serializable
    data class Link(
        val componentAId: String,
        val portAId: String,
        val componentBId: String,
        val portBId: String
    )

    @Serializable
    data class SimulationProfile(
        val name: String,
        val id: String,
        val stepConditionEnabled: Boolean = false,
        val stepConditionSteps: Long = 0,
        val timeConditionEnabled: Boolean = false,
        @Serializable(with = DurationSerializer::class)
        val timeConditionTime: Duration = Duration.ZERO

    )
}
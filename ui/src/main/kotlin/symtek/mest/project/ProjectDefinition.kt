package symtek.mest.project

import kotlinx.serialization.Polymorphic
import kotlinx.serialization.Serializable
import symtek.mest.api.simulation.SimulationManager
import java.io.File

data class Project(
    var name: String,
    var file: File,
    val simulationManager: SimulationManager = SimulationManager()
)

@Serializable
data class ProjectDAOContainer(val project: BaseProjectDAO)

@Polymorphic
@Serializable
abstract class BaseProjectDAO


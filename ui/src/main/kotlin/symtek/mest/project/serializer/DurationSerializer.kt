package symtek.mest.project.serializer

import kotlinx.serialization.*
import kotlinx.serialization.internal.SerialClassDescImpl
import java.time.Duration

@Serializer(forClass = Duration::class)
object DurationSerializer : KSerializer<Duration> {
    override val descriptor: SerialDescriptor = object : SerialClassDescImpl("Duration") {
        init {
            addElement("nano") // req will have index 0
            addElement("seconds") // res will have index 1
        }
    }
    override fun serialize(encoder: Encoder, obj: Duration) {
        val compositeOutput = encoder.beginStructure(descriptor)
        compositeOutput.encodeStringElement(descriptor, 0, obj.nano.toString())
        compositeOutput.encodeStringElement(descriptor, 1, obj.seconds.toString())
        compositeOutput.endStructure(descriptor)
    }

    override fun deserialize(decoder: Decoder): Duration {
        val dec: CompositeDecoder = decoder.beginStructure(descriptor)
        var nano: Long? = null
        var seconds: Long? = null
        loop@ while (true) {
            when (val i = dec.decodeElementIndex(descriptor)) {
                CompositeDecoder.READ_DONE -> break@loop
                0 -> nano = dec.decodeStringElement(descriptor, i).toLong()
                1 -> seconds = dec.decodeStringElement(descriptor, i).toLong()
                else -> throw SerializationException("Unknown index $i")
            }
        }
        dec.endStructure(descriptor)

        return Duration.ofSeconds(
            seconds ?: throw MissingFieldException("req"),
            nano ?: throw MissingFieldException("res")
        )
    }
}
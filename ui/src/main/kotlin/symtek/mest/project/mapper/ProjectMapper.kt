package symtek.mest.project.mapper

import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleLongProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import symtek.mest.api.component.Component
import symtek.mest.api.component.Link
import symtek.mest.api.simulation.ComponentCandidate
import symtek.mest.api.simulation.SimulationManager
import symtek.mest.project.Project
import symtek.mest.project.schema.ProjectSchemav1
import symtek.mest.ui.models.simulation.SimulationProfile
import symtek.mest.ui.scope.ProjectScope
import symtek.mest.ui.util.JavaFXHelper
import symtek.mest.ui.views.workbench.componentstage.ComponentStage
import symtek.mest.ui.views.workbench.simulationconfiguration.SimulationConfigurationController
import tornadofx.find
import java.io.File
import kotlin.reflect.KClass

class ProjectToSchemaMapper(val projectScope: ProjectScope) {

    private val project = projectScope.project
    private val simulationManager = project.simulationManager
    private val componentStage = find<ComponentStage>(projectScope)
    val simulationConfigurationController = find<SimulationConfigurationController>(projectScope)

    fun map(): ProjectSchemav1.Project {
        val components = simulationManager.componentCandidates.map { toSchema(it) }
        val links = simulationManager.getLinkCandidates().map { toSchema(it) }
        val simulationProfiles = simulationConfigurationController.profilesProperty
            .filter { it != simulationConfigurationController.defaultSimulationProfile }
            .map { toSchema(it) }

        val activeProfileId =
            if (simulationConfigurationController.activeProfileProperty.value == simulationConfigurationController.defaultSimulationProfile) {
                null
            } else {
                simulationConfigurationController.activeProfileProperty.value.id
            }

        return ProjectSchemav1.Project(
            projectName = project.name,
            components = components,
            links = links,
            simulationProfiles = simulationProfiles,
            activeSimulationProfileId = activeProfileId
        )
    }

    private fun toSchema(componentCandidate: ComponentCandidate<*>): ProjectSchemav1.Component {
        val componentFragment = componentStage.findComponentFragment(componentCandidate)!!
        val position = JavaFXHelper.getPositionInsideComponentStage(componentFragment.root, componentStage)

        return ProjectSchemav1.Component(
            name = componentCandidate.name,
            id = componentCandidate.id,
            componentClass = componentCandidate.componentClass.java.canonicalName,
            position = ProjectSchemav1.ComponentPosition(
                x = position.x,
                y = position.y
            ),
            minimized = componentFragment.hideUnusedPorts
        )
    }

    private fun toSchema(link: Link): ProjectSchemav1.Link {
        return ProjectSchemav1.Link(
            componentAId = link.portA.component.name,
            portAId = link.portA.id,
            componentBId = link.portB.component.name,
            portBId = link.portB.id
        )
    }

    private fun toSchema(simulationProfile: SimulationProfile): ProjectSchemav1.SimulationProfile {
        return ProjectSchemav1.SimulationProfile(
            name = simulationProfile.name.value,
            id = simulationProfile.id,
            stepConditionEnabled = simulationProfile.stepConditionEnabled.value,
            stepConditionSteps = simulationProfile.stepConditionSteps.value,
            timeConditionEnabled = simulationProfile.timeConditionEnabled.value,
            timeConditionTime = simulationProfile.timeConditionTime.value
        )
    }

}

class SchemaToProjectMapper(val file: File, val schemaProject: ProjectSchemav1.Project) {

    fun createModel(): Project {
        val simulationManager = SimulationManager()

        schemaProject.components.forEach {
            @Suppress("UNCHECKED_CAST")
            simulationManager.addComponentCandidate(
                ComponentCandidate(
                    componentClass = Class.forName(it.componentClass).kotlin as KClass<Component>,
                    name = it.name,
                    id = it.id
                )
            )
        }

        schemaProject.links.forEach { schemaLink ->
            val componentA = findComponentFor(simulationManager.componentCandidates, schemaLink.componentAId)
            val componentB = findComponentFor(simulationManager.componentCandidates, schemaLink.componentBId)

            val portA = componentA.ports.find { it.id == schemaLink.portAId }!!
            val portB = componentB.ports.find { it.id == schemaLink.portBId }!!

            val link = Link(portA = portA, portB = portB)
            simulationManager.connectLink(link)
        }
        return Project(
            name = schemaProject.projectName,
            file = file,
            simulationManager = simulationManager
        )
    }

    private fun findComponentFor(componentCandidates: List<ComponentCandidate<*>>, componentId: String) =
        componentCandidates.find { it.id == componentId }!!

    fun createProfiles(): List<SimulationProfile> {
        return schemaProject.simulationProfiles.map { toModel(it) }
    }

    fun toModel(simulationProfile: ProjectSchemav1.SimulationProfile): SimulationProfile {
        return SimulationProfile(
            name = SimpleStringProperty(simulationProfile.name),
            id = simulationProfile.id,
            stepConditionEnabled = SimpleBooleanProperty(simulationProfile.stepConditionEnabled),
            stepConditionSteps = SimpleLongProperty(simulationProfile.stepConditionSteps),
            timeConditionEnabled = SimpleBooleanProperty(simulationProfile.timeConditionEnabled),
            timeConditionTime = SimpleObjectProperty(simulationProfile.timeConditionTime)
        )
    }
}

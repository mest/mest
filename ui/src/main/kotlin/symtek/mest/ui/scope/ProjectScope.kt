package symtek.mest.ui.scope

import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import kotlinx.serialization.modules.SerializersModule
import symtek.mest.api.simulation.SimulationManager
import symtek.mest.api.simulation.runtime.Scheduler
import symtek.mest.project.BaseProjectDAO
import symtek.mest.project.Project
import symtek.mest.project.ProjectDAOContainer
import symtek.mest.project.mapper.ProjectToSchemaMapper
import symtek.mest.project.mapper.SchemaToProjectMapper
import symtek.mest.project.schema.ProjectSchemav1
import symtek.mest.ui.views.workbench.simulationconfiguration.SimulationConfigurationController
import tornadofx.Scope
import tornadofx.find
import java.io.File

class ProjectScope private constructor(val project: Project, val schemaProject: ProjectSchemav1.Project?) : Scope() {
    companion object {
        private val json = Json(
            configuration = JsonConfiguration.Stable.copy(prettyPrint = true),
            context = SerializersModule {
                polymorphic(BaseProjectDAO::class) {
                    // v1 support
                    ProjectSchemav1.Project::class with ProjectSchemav1.Project.serializer()
                }
            }
        )

        fun fromFile(file: File): ProjectScope {
            val jsonData = file.readText()
            val projectDAOContainer = json.parse(ProjectDAOContainer.serializer(), jsonData)
            val schemaProject = projectDAOContainer.project as ProjectSchemav1.Project
            val mapper = SchemaToProjectMapper(file, schemaProject = schemaProject)
            val project = mapper.createModel()
            val projectScope = ProjectScope(project, schemaProject)
            val simulationConfigurationController = find<SimulationConfigurationController>(projectScope)

            // Simulation Profiles
            val simulationProfiles = mapper.createProfiles()
            simulationConfigurationController.profilesProperty.addAll(simulationProfiles)
            val activeProfileId = mapper.schemaProject.activeSimulationProfileId
            if (activeProfileId != null) {
                val activeProfile = simulationProfiles.find { it.id == activeProfileId }
                if (activeProfile != null) {
                    simulationConfigurationController.activeProfileProperty.set(activeProfile)
                }
            }

            return projectScope
        }

        fun newProject(name: String, file: File): ProjectScope {
            return ProjectScope(
                project = Project(
                    name = name,
                    file = file,
                    simulationManager = SimulationManager()
                ),
                schemaProject = null
            )
        }
    }

    var simulationScheduler: Scheduler? = null

    fun save() {
        val projectDAO = ProjectToSchemaMapper(this).map()
        val projectDAOContainer = ProjectDAOContainer(projectDAO)
        val jsonData = json.stringify(ProjectDAOContainer.serializer(), projectDAOContainer)
        project.file.writeText(jsonData)
    }

    fun saveAs(file: File) {
        project.file = file
        save()
    }

}
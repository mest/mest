package symtek.mest.ui.events

import symtek.mest.api.simulation.runtime.Scheduler
import symtek.mest.api.simulation.runtime.State
import symtek.mest.ui.scope.ProjectScope

// Scheduler events
//   auto throw
data class SchedulerCreated(override var eventScope: ProjectScope?, val scheduler: Scheduler) : BaseEvent()
data class SchedulerStateChanged(override var eventScope: ProjectScope?, val oldState: State, val newState: State) : BaseEvent()
data class SchedulerStateChangeRequested(override var eventScope: ProjectScope?, val state: State) : BaseEvent()
//   request-response
data class SchedulerStateQuery(override var eventScope: ProjectScope?, override val caller: Any) : EventWithCaller()
data class SchedulerStateResponse(override var eventScope: ProjectScope?, override val caller: Any, val state: State?) : EventWithCaller()

package symtek.mest.ui.events

import symtek.mest.ui.scope.ProjectScope
import tornadofx.Controller

/**
 * This Controller must be added to each [symtek.mest.ui.workbench.Workbench] in order to receive specific unscoped events ([UnscopedEventWrapper]s) that should be routed to a specific scope.
 *
 * Keep in mind, that these [UnscopedEventWrapper] are created from custom implementations of model classes (see [symtek.mest.api.ImplementationRegistry]).
 * Because the creation of these exchanged implementations is done in the api part of the application, the [ProjectScope] is unknown.
 * Therefor events created from the custom implementation can only be sent to all scopes.
 *
 * This event handler will look at the given [UnscopedEventWrapper] created by the custom implementation.
 * It will check wether or not, the contained entity (specified with the [UnscopedEventWrapper.type] and [UnscopedEventWrapper.id]) is matching the current scope.
 * If matching, the event wrapped event will be extracted and redirected to the correct scope.
 */
class ModelToUIScopeEventRouter : Controller() {
    override val scope = super.scope as ProjectScope
    init {
        subscribe<UnscopedEventWrapper> {
            fun fireInScope(event: BaseEvent) {
                event.eventScope = scope
                fire(event)
            }
            when(it.type) {
                EntityTypes.Scheduler -> if (it.id == scope.simulationScheduler!!.id) fireInScope(it.event)
                EntityTypes.SchedulerStateMachine -> if (it.id == scope.simulationScheduler!!.stateMachine.id) fireInScope(it.event)
            }
        }
    }
}
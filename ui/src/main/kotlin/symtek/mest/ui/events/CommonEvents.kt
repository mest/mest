package symtek.mest.ui.events

import symtek.mest.ui.scope.ProjectScope
import tornadofx.FX
import tornadofx.FXEvent
import tornadofx.Scope

/**
 * Base class for all UI events. This class adds support to change the scope of an event at runtime by changing the scope via the [eventScope] variable.
 */
abstract class BaseEvent(open var eventScope: ProjectScope? = null) : FXEvent() {
    final override val scope: Scope?
        get() {
            return eventScope
        }
}


/**
 * Enum containing the entities that may throw events with custom implementations (see [symtek.mest.api.ImplementationRegistry]).
 *
 * Will be used by [UnscopedEventWrapper] and [symtek.mest.ui.workbench.scope.ModelToUIScopeEventRouter].
 */
enum class EntityTypes {
    Scheduler, SchedulerStateMachine
}


/**
 * Event containing an unscoped event that can be routed by the [symtek.mest.ui.workbench.scope.ModelToUIScopeEventRouter].
 */
data class UnscopedEventWrapper(val type: EntityTypes, val id: String, val event: BaseEvent) : BaseEvent()


/**
 * Helper method to fire [UnscopedEventWrapper] events to the application outside a standard tornadofx class.
 */
fun fireWithUnscopedWrapper(type: EntityTypes, id: String, event: BaseEvent) {
    FX.eventbus.fire(UnscopedEventWrapper(type, id, event))
}


/**
 * Event containing the initiator of the request event via [caller].
 *
 * This event can be used for request-response events where only the caller of the event is interested for the response.
 */
abstract class EventWithCaller() : BaseEvent() {
    abstract val caller: Any
}
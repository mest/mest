package symtek.mest.ui.views.workbench.componentstage

import symtek.mest.ui.util.InteroperabilityHelper
import tornadofx.Controller
import kotlin.math.roundToInt

class ComponentStageZoomController : Controller() {

    val componentStage : ComponentStage by param()

    private val zoomRange = javafx.geometry.Point2D(0.4, 2.9)

    init {
        with(componentStage) {
            zoomTarget.setOnDragDetected {
                /*var target: Node? = it.target as Node
                while (target != zoomTarget && target != null) {
                    target = target.parent
                }
                if (target != null) {
                    target.startFullDrag()
                }*/
            }

            root.setOnScrollStarted {
                val isKeyDown =
                    (!InteroperabilityHelper.isMacOS() && it.isControlDown) || (InteroperabilityHelper.isMacOS() && it.isAltDown)

                if (isKeyDown) {
                    it.consume()
                }
            }
            root.content.setOnScroll {
                val isKeyDown =
                    (!InteroperabilityHelper.isMacOS() && it.isControlDown) || (InteroperabilityHelper.isMacOS() && it.isAltDown)

                if (isKeyDown) {
                    it.consume()

                    var zoomFactor = if (it.deltaY > 0) 1.2 else 1 / 1.2
                    val oldScale = zoomTarget.scaleX
                    var newScale = ((oldScale * zoomFactor) * 10.0).roundToInt()/10.0;

                    when {
                        newScale < zoomRange.x -> newScale = zoomRange.x
                        newScale > zoomRange.y -> newScale = zoomRange.y
                    }

                    zoomFactor = newScale / oldScale

                    var groupBounds = zoomGroup.layoutBounds
                    val viewportBounds = root.viewportBounds

                    // calculate pixel offsets from [0, 1] range
                    val valX = root.hvalue * (groupBounds.width - viewportBounds.width)
                    val valY = root.vvalue * (groupBounds.height - viewportBounds.height)

                    // convert componentContainer coordinates to zoomTarget coordinates
                    val mouse = javafx.geometry.Point2D(it.x, it.y)
                    val mouseInLocal = zoomGroup.parentToLocal(mouse).subtract(
                        javafx.geometry.Point2D(
                            zoomTarget.layoutBounds.minX * zoomTarget.scaleX,
                            zoomTarget.layoutBounds.minY * zoomTarget.scaleY
                        )
                    )
                    val posInZoomTarget = zoomTarget.parentToLocal(mouseInLocal)

                    // calculate adjustment of scroll position (pixels)


                    val multiply = posInZoomTarget.multiply(zoomFactor - 1)
                    val adjustment = zoomTarget.localToParentTransform.deltaTransform(multiply)

                    // do the resizing
                    zoomTarget.scaleX = newScale
                    zoomTarget.scaleY = newScale

                    // refresh ScrollPane scroll positions & componentContainer bounds
                    root.layout()

                    // convert back to [0, 1] range
                    // (too large/small values are automatically corrected by ScrollPane)
                    groupBounds = zoomGroup.layoutBounds
                    root.hvalue = (valX + adjustment.x) / (groupBounds.width - viewportBounds.width)
                    root.vvalue = (valY + adjustment.y) / (groupBounds.height - viewportBounds.height)
                }
            }
        }
    }
}
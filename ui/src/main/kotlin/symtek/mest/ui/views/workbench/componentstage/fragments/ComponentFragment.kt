package symtek.mest.ui.views.workbench.componentstage.fragments

import com.sun.javafx.scene.control.skin.TitledPaneSkin
import javafx.beans.property.SimpleBooleanProperty
import javafx.scene.Node
import javafx.scene.layout.BorderPane
import javafx.scene.layout.VBox
import symtek.mest.api.component.CommonPort
import symtek.mest.api.component.GeneralInputPort
import symtek.mest.api.component.GeneralOutputPort
import symtek.mest.api.simulation.ComponentCandidate
import symtek.mest.ui.MestStyles
import symtek.mest.ui.views.workbench.componentstage.*
import symtek.mest.ui.scope.ProjectScope
import tornadofx.*

class ComponentFragment : Fragment(), SelectableNode,
    MovableNode {

    val component: ComponentCandidate<*> by param()

    override val scope = super.scope as ProjectScope
    val viewController =
        tornadofx.find<ComponentStageViewController>(scope)

    override fun getNode(): Node = root

    override fun select() {
        this.root.addPseudoClass("selected")
    }
    override fun deselect() {
        this.root.removePseudoClass("selected")
    }

    val center = BorderPane()
    val portFragments : MutableList<PortFragment> = mutableListOf()

    val leftPorts = VBox()
    val rightPorts = VBox()

    val hideUnusedPortsProperty = SimpleBooleanProperty()
    var hideUnusedPorts by hideUnusedPortsProperty

    val maximizedImage = imageview("icons/baseline_keyboard_arrow_down_black_18dp.png") {
        isPreserveRatio = true
        fitHeight = MestStyles.componentPortMinimizerButtonImageSize.value
    }
    val minimizedImage = imageview("icons/baseline_keyboard_arrow_right_black_18dp.png") {
        isPreserveRatio = true
        fitHeight = MestStyles.componentPortMinimizerButtonImageSize.value
    }

    fun createPort(port: CommonPort<*>, direction: PortDirection) = find<ComponentStage>().createPortFragment(port, direction)

    override val root = titledpane {
        // styling
        setId(component.id)
        addClass(MestStyles.componentFragment)

        // do not consume mouse events
        skin = object: TitledPaneSkin(this) {
            init{
                this.consumeMouseEvents(false)
            }
        }

        isCollapsible = false

        text = "${component.name} <${component.componentClass.simpleName}>"

        with(center) {

            component.ports.filter {
                when (it) {
                    is GeneralInputPort -> true
                    is GeneralOutputPort -> false
                    else -> throw IllegalArgumentException()
                }
            }.forEach {
                val portFragment = createPort(it, PortDirection.LEFT)
                portFragments.add(portFragment)
                leftPorts.children.add(portFragment.root)
            }

            component.ports.filter {
                when (it) {
                    is GeneralInputPort -> false
                    is GeneralOutputPort -> true
                    else -> throw IllegalArgumentException()
                }
            }.forEach {
                val portFragment = createPort(it, PortDirection.RIGHT)
                portFragments.add(portFragment)
                rightPorts.children.add(portFragment.root)
            }

            left = leftPorts
            right = rightPorts
        }
        this.content = center

        graphic = button {
            addClass(MestStyles.componentPortMinimizerButton)
            isFocusTraversable = false
            graphic = maximizedImage
            hideUnusedPortsProperty.addListener(ChangeListener { _, _, newValue ->
                graphic = when (newValue) {
                    true -> minimizedImage
                    false -> maximizedImage
                }
            })
            action {
                if (viewController.mode == Mode.SELECT_AND_DRAG) {
                    hideUnusedPortsProperty.set(!hideUnusedPorts)
                }
            }
        }

        hideUnusedPortsProperty.addListener(ChangeListener { observable, oldValue, newValue ->
            if (oldValue != newValue) {
                // is switched
                val connectedPorts = mutableSetOf<CommonPort<*>>()
                component.initComponentInstance.links.forEach {
                    if (component.ports.contains(it.portA)) {
                        connectedPorts.add(it.portA)
                    }
                    if (component.ports.contains(it.portB)) {
                        connectedPorts.add(it.portB)
                    }
                }

                portFragments.forEach {
                    it.hide = newValue && !connectedPorts.contains(it.portObject)
                }
            }
        })
    }
}
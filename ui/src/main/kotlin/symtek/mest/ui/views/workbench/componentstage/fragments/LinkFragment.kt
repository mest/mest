package symtek.mest.ui.views.workbench.componentstage.fragments

import javafx.scene.Node
import javafx.scene.Parent
import javafx.scene.shape.Circle
import javafx.scene.shape.Line
import symtek.mest.api.component.Link
import symtek.mest.ui.MestStyles
import symtek.mest.ui.util.JavaFXHelper
import symtek.mest.ui.views.workbench.componentstage.ComponentStage
import symtek.mest.ui.scope.ProjectScope
import tornadofx.*

class LinkFragment : Fragment(), SelectableNode {
    override fun getNode(): Node { return root }

    override fun select() {
        lines.forEach { it.addPseudoClass("selected") }
    }

    override fun deselect() {
        lines.forEach { it.removePseudoClass("selected") }
    }

    var active = true


    val line = Line()
    val portALine = Line()
    val portBLine = Line()
    val lines = listOf(line, portALine, portBLine)

    // TODO: Inject these to the ComponentFragment of the port for them to not overlap
    val portACircle = Circle()
    val portBCircle = Circle()

    val link: Link by param()

    override val scope = super.scope as ProjectScope

    val componentStage = find<ComponentStage>(scope)

    val componentAFragment: ComponentFragment
    val portAFragment: PortFragment
    val componentBFragment: ComponentFragment
    val portBFragment: PortFragment

    init {
        lines.forEach { it.addClass(MestStyles.linkLine) }
        with(portACircle) {
            radius = 6.0
        }
        with(portBCircle) {
            radius = 6.0
        }

        portAFragment = componentStage.findPortFragment(link.portA)
        componentAFragment = componentStage.findComponentFragment(link.portA.component)

        portBFragment = componentStage.findPortFragment(link.portB)
        componentBFragment = componentStage.findComponentFragment(link.portB.component)

        componentAFragment.root.localToParentTransformProperty().addListener(ChangeListener { observable, oldValue, newValue ->
            updatePosition()
        })
        componentBFragment.root.localToParentTransformProperty().addListener(ChangeListener { observable, oldValue, newValue ->
            updatePosition()
        })

        updatePosition()

    }

    override val root = pane {
        setId(link.id)
        isPickOnBounds = false
        this += portALine
        this += portBLine
        this += line
    }

    fun activate() {
        val hideA = componentAFragment.hideUnusedPorts
        val hideB = componentBFragment.hideUnusedPorts

        componentAFragment.hideUnusedPorts = false
        componentBFragment.hideUnusedPorts = false

        portAFragment.connector.children.add(portACircle)
        portBFragment.connector.children.add(portBCircle)
        active = true
        updatePosition()

        componentAFragment.hideUnusedPorts = hideA
        componentBFragment.hideUnusedPorts = hideB
    }
    fun deactivate() {
        portAFragment.connector.children.remove(portACircle)
        portBFragment.connector.children.remove(portBCircle)
        active = false
    }

    fun updatePosition() {
        if (!active) return

        val positionA = JavaFXHelper.getMiddlePositionOfNodeInsideComponentStage(node = portAFragment.getConnectorNode() as Parent, componentStage = componentStage)
        val positionB = JavaFXHelper.getMiddlePositionOfNodeInsideComponentStage(node = portBFragment.getConnectorNode() as Parent, componentStage = componentStage)

        portACircle.centerX = positionA.x
        portACircle.centerY = positionA.y

        portBCircle.centerX = positionB.x
        portBCircle.centerY = positionB.y

        val hoffset = 35.0
        val voffset = 0.0

        val offsetA = if (portAFragment.direction == PortDirection.LEFT) (-1*hoffset) else hoffset
        portALine.startX = positionA.x
        portALine.startY = positionA.y
        portALine.endX = positionA.x + offsetA
        portALine.endY = positionA.y + voffset

        val offsetB = if (portAFragment.direction == PortDirection.RIGHT) (-1*hoffset) else hoffset
        portBLine.startX = positionB.x
        portBLine.startY = positionB.y
        portBLine.endX = positionB.x + offsetB
        portBLine.endY = positionB.y + voffset

        line.startX = positionA.x + offsetA
        line.startY = positionA.y + voffset

        line.endX = positionB.x + offsetB
        line.endY = positionB.y + voffset

    }


}
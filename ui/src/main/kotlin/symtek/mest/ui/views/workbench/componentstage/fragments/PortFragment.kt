package symtek.mest.ui.views.workbench.componentstage.fragments

import javafx.beans.property.SimpleBooleanProperty
import javafx.geometry.Pos
import javafx.scene.Group
import javafx.scene.Node
import javafx.scene.layout.HBox
import javafx.scene.layout.StackPane
import javafx.scene.paint.Color
import javafx.scene.shape.Circle
import javafx.scene.text.Text
import symtek.mest.api.component.CommonPort
import symtek.mest.ui.MestStyles
import tornadofx.*

interface PortNode {
    fun getNode() : Node
    fun getConnectorNode() : Node
    fun getPort(): CommonPort<*>
    fun markConnactability(connectability: Boolean?)
}

enum class PortDirection{
    LEFT, RIGHT
}

class PortFragment: Fragment(), PortNode {

    val portObject: CommonPort<*> by param()
    val direction: PortDirection by param()

    val hideProperty = SimpleBooleanProperty()
    var hide by hideProperty

    override fun getNode() = content

    override fun getConnectorNode(): Node = connector

    override fun getPort(): CommonPort<*> = portObject

    override fun markConnactability(connectability: Boolean?) {
        with (content) {
            removePseudoClass(MestStyles.portValid.name)
            removePseudoClass(MestStyles.portInvalid.name)

            if (connectability != null) {
                if (connectability) {
                    addPseudoClass(MestStyles.portValid.name)
                } else {
                    addPseudoClass(MestStyles.portInvalid.name)
                }
            }
        }
    }

    val text: Text
    val connector: StackPane
    val innerCircle = Circle()
    val outerCircle = Circle()

    override val root: Group
    val content = HBox()

    init {
        text = text { text = portObject.name }

        with(outerCircle) {
            addClass(MestStyles.portFragmentConnectorOuterCircle)
            radius = 8.0
        }
        with(innerCircle) {
            addClass(MestStyles.portFragmentConnectorInnerCircle)
            radius = 6.0
            fill = Color.TRANSPARENT
        }
        connector = stackpane {
            this += outerCircle
            this += innerCircle
        }

        with(content) {
            // styling
            addClass(MestStyles.portFragment)

            alignment = Pos.CENTER

            if (direction == PortDirection.LEFT) {
                this += connector
                this += text
            } else if (direction == PortDirection.RIGHT) {
                this += text
                this += connector
            }
        }

        root = group {
            setId(portObject.id)

            this += content
        }
        hideProperty.addListener(ChangeListener { observable, oldValue, newValue ->
            if (oldValue != newValue) {
                root.children.clear()
                if (!newValue) {
                    root.children.add(content)
                }
            }
        })
    }
}
package symtek.mest.ui.views.workbench.simulationconfiguration

import javafx.beans.binding.Bindings
import javafx.scene.control.ButtonBar
import javafx.scene.control.ListView
import javafx.util.StringConverter
import javafx.util.converter.NumberStringConverter
import symtek.mest.ui.models.simulation.SimulationProfile
import symtek.mest.ui.models.simulation.SimulationProfileModel
import tornadofx.*
import java.time.Duration
import java.time.format.DateTimeParseException

class SimulationConfigurationView : View(title = "Edit Simulation Configurations") {
    val simulationConfigurationViewController: SimulationConfigurationController = find(scope)

    val profileModel = find<SimulationProfileModel>(scope)

    var profileListView: ListView<SimulationProfile> by singleAssign()

    override val root = borderpane {
        center = vbox {
            hbox {
                button("Add") {
                    action {
                        simulationConfigurationViewController.addProfile()
                    }
                }
                button("Remove") {
                    enableWhen(
                        Bindings.and(
                            profileModel.itemProperty.isNotEqualTo(simulationConfigurationViewController.defaultSimulationProfile),
                            profileModel.itemProperty.isNotNull
                        )
                    )
                    action {
                        simulationConfigurationViewController.removeProfilte(profileModel.item)
                    }
                }
            }
            profileListView = listview(simulationConfigurationViewController.profilesProperty) {
                disableWhen(
                    Bindings.and(
                        profileModel.itemProperty.isNotNull,
                        profileModel.valid.not()
                    )
                )
                cellFormat { simulationProfile ->
                    textProperty().bind(simulationProfile.name)
                }

                // Autosave on selection change
                selectionModel.selectedItemProperty().addListener(ChangeListener { observable, oldValue, newValue ->
                    // Autosave on selection change
                    if (oldValue != null && oldValue != simulationConfigurationViewController.defaultSimulationProfile) {
                        saveProfile()
                    }
                })
                bindSelected(profileModel)

                // This will update the active profile (but only unidirectional)
                bindSelected(simulationConfigurationViewController.activeProfileProperty)
            }
        }

        right = form {
            label {
                graphic = imageview("icons/baseline_info_black_18dp.png") {
                    fitHeight = 18.0
                    isPreserveRatio = true
                }
                text = "The default profile can't be modified."
                visibleWhen {
                    Bindings.and(
                        profileModel.itemProperty.isEqualTo(simulationConfigurationViewController.defaultSimulationProfile),
                        profileModel.itemProperty.isNotNull
                    )
                }
            }
            fieldset {
                enableWhen(
                    Bindings.and(
                        profileModel.itemProperty.isNotEqualTo(simulationConfigurationViewController.defaultSimulationProfile),
                        profileModel.itemProperty.isNotNull
                    )
                )
                fieldset("Profile name") {
                    textfield(profileModel.name) {
                        required()
                    }
                }
                fieldset {
                    fieldset("Limit Simulation by steps") {
                        field("Enable Condition") {
                            checkbox {
                                selectedProperty().bindBidirectional(profileModel.stepConditionEnabled)
                                selectedProperty().onChange { profileModel.validate() }
                            }
                        }
                        field("Number of steps") {
                            textfield(profileModel.stepConditionSteps, NumberStringConverter()) {
                                enableWhen(profileModel.stepConditionEnabled)
                                filterInput { it.controlNewText.isInt() }
                                requiredWhen(profileModel.stepConditionEnabled)
                            }
                        }
                    }
                    fieldset("Run for # duration") {
                        field("Enable Condition") {
                            checkbox {
                                selectedProperty().bindBidirectional(profileModel.timeConditionEnabled)
                                selectedProperty().onChange { profileModel.validate() }
                            }
                        }
                        field("Simulation Duration") {
                            textfield(profileModel.timeConditionTime, DurationConverter()) {
                                enableWhen(profileModel.timeConditionEnabled)
                                validator {
                                    if (!profileModel.timeConditionEnabled.value) {
                                        return@validator null
                                    }
                                    if (it == null || it.isEmpty()) {
                                        return@validator error("Please enter a Duration in the Format (PT0H0M0S)")
                                    }
                                    try {
                                        Duration.parse(it)
                                    } catch (ex: DateTimeParseException) {
                                        return@validator error(ex.localizedMessage)
                                    }
                                    return@validator null
                                }
                            }
                        }
                    }

                    buttonbar {
                        button("Save Profile", type = ButtonBar.ButtonData.APPLY) {
                            enableWhen(
                                Bindings.and(
                                    profileModel.valid,
                                    profileModel.dirty
                                )
                            )
                            action { saveProfile() }
                        }
                        button("Reset Profile", type = ButtonBar.ButtonData.CANCEL_CLOSE) {
                            enableWhen(profileModel.dirty)
                            action { profileModel.rollback() }

                        }
                    }


                }
            }
        }

    }

    fun saveProfile() {
        if (profileModel.isDirty) {
            profileModel.commit()
        }
    }

    /**
     *  Preselect the currently active profile.
     */
    override fun onBeforeShow() {
        super.onBeforeShow()
        profileListView.selectionModel.select(simulationConfigurationViewController.activeProfileProperty.value)
    }

    /**
     * Save on close
     */
    override fun onUndock() {
        saveProfile()
        super.onUndock()
    }

}

class DurationConverter : StringConverter<Duration?>() {
    override fun toString(duration: Duration?): String = duration?.toString() ?: ""

    override fun fromString(str: String?): Duration? {
        return try {
            Duration.parse(str)
        } catch (ex: DateTimeParseException) {
            Duration.ZERO
        }
    }
}
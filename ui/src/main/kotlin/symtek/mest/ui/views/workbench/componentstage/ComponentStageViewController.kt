package symtek.mest.ui.views.workbench.componentstage

import javafx.geometry.Point2D
import javafx.scene.Node
import javafx.scene.Parent
import javafx.scene.input.*
import javafx.scene.shape.Line
import symtek.mest.api.component.Component
import symtek.mest.api.component.Link
import symtek.mest.ui.util.InteroperabilityHelper
import symtek.mest.ui.util.JavaFXHelper
import symtek.mest.ui.scope.ProjectScope
import symtek.mest.ui.views.workbench.componentchooser.COMPONENT_CLASS_DATAFORMAT
import tornadofx.Controller
import tornadofx.find
import java.lang.Exception
import javafx.scene.input.KeyCode
import symtek.mest.ui.commands.*
import symtek.mest.ui.views.workbench.componentstage.fragments.*
import tornadofx.runLater

class ComponentStageViewController : Controller(){
    override val scope = super.scope as ProjectScope

    val componentStage: ComponentStage by param()

    val addComponentHandler = AddComponentHandler(this, componentStage)
    val selectAndDragHandler = SelectAndDragHandler(this, componentStage)
    val connectPortHandler = ConnectPortsHandler(this, componentStage)

    val handler = mutableListOf<ModeHandler>(
        selectAndDragHandler,
        addComponentHandler,
        connectPortHandler
    )

    val commandManager = find<CommandManager>(scope)

    var mode: Mode =
        Mode.SELECT_AND_DRAG
    var previousMode = Mode.SELECT_AND_DRAG

    fun changeMode(to: Mode) {
        this.previousMode = mode
        this.mode = to
        updateHandler()
    }

    fun revertMode() {
        val newPrevious = mode
        mode = previousMode
        previousMode = newPrevious
        updateHandler()
    }

    private fun updateHandler() {
        handler.filter { it.mode == previousMode }.forEach { it.deactivate() }
        handler.filter { it.mode == this.mode }.forEach { it.activate() }
    }

}

abstract class ModeHandler(val controller: ComponentStageViewController, val componentStage: ComponentStage, val mode: Mode) {
    fun isMyModeActive(): Boolean {
        return controller.mode == mode
    }

    abstract fun activate()
    abstract fun deactivate()
}

/**
 * This handler is responsible for two actions:
 * - Selecting components
 * - Moving/Dragging components
 *
 * =====================================================================================================================
 * if the mouse is pressed down
 * - if the ctrl key & primary mouse button is pressed
 *   - if somewhere between the event target and the content is a selectable node
 *     - toggle the selectable node (remove/deselect if already selected, deselect otherwise)
 * - if only the primary mouse button is pressed
 *   - if somewhere between the event target and the content is a selectable node
 *     - if selectable node is not already selected
 *       - clear the selection
 *       - add the selectable node
 *     - the position of all selected nodes will be saved (starting location)
 *     - and the current mouse location is saved as the start location
 *   - else deselect everything
 *
 * =====================================================================================================================
 * if a mouse drag was detected
 * - if at least one object is selected
 *   - enable dragging mode
 * - else nothing
 *
 * =====================================================================================================================
 * if mouse dragged
 * - if dragging active
 *   - move every selected objects (visually)
 *
 * =====================================================================================================================
 * if mouse relesed
 * - if dragging active
 *   - disable dragging
 *   - commit movement for every selected object
 *
 */
class SelectAndDragHandler(controller: ComponentStageViewController, componentStage: ComponentStage) :
    ModeHandler(controller = controller, componentStage = componentStage, mode = Mode.SELECT_AND_DRAG) {

    class NodeWithPosition(val node: MovableNode) {
        var startLayoutX: Double = 0.0
        var startLayoutY: Double = 0.0

        fun updateStartPosition() {
            startLayoutX = node.getPositionX()
            startLayoutY = node.getPositionY()
        }
    }

    val selectedObjects = mutableMapOf<SelectableNode, NodeWithPosition?>()
    @Suppress("UNCHECKED_CAST")
    val movableObjects: Map<MovableNode, NodeWithPosition>
        get() = selectedObjects.filter { it.key is MovableNode } as Map<MovableNode, NodeWithPosition>

    var dragData = DragData()
    var dragging = false

    fun selectNode(node: SelectableNode) {
        if (!selectedObjects.contains(node)) {
            selectedObjects[node] = if (node is MovableNode) {
                NodeWithPosition(node)
            } else null
            node.select()
        }
    }

    fun deselectNode(node: SelectableNode) {
        if (selectedObjects.containsKey(node)) {
            node.deselect()
            selectedObjects.remove(node)
        }
    }

    fun deselectAll() {
        selectedObjects.forEach { it.key.deselect() }
        selectedObjects.clear()
    }

    fun toggleNode(node: SelectableNode) {
        if (!selectedObjects.contains(node)) {
            selectNode(node)
        } else {
            deselectNode(node)
        }
    }

    override fun deactivate() {
        deselectAll()
    }

    override fun activate() {
    }

    init {
        runLater {
            componentStage.primaryStage.scene.addEventHandler(KeyEvent.KEY_PRESSED) {
                if (it.code == KeyCode.DELETE && isMyModeActive()) {
                    val toRemoveLinkFragments = mutableSetOf<LinkFragment>()
                    val toRemoveComponentFragments = mutableSetOf<ComponentFragment>()
                    selectedObjects.forEach { key, _ ->
                        when (key) {
                            is LinkFragment -> toRemoveLinkFragments.add(key)
                            is ComponentFragment -> toRemoveComponentFragments.add(key)
                            else -> throw NotImplementedError()
                        }
                    }
                    toRemoveComponentFragments.forEach {
                        deselectNode(it)
                    }
                    toRemoveLinkFragments.forEach {
                        deselectNode(it)
                    }

                    if (toRemoveComponentFragments.size > 0 || toRemoveLinkFragments.size > 0) {
                        val removeComponentCommand = RemoveComponentsAndLinksCommand(
                            controller.scope,
                            toRemoveComponentFragments,
                            toRemoveLinkFragments
                        )
                        controller.commandManager.execute(removeComponentCommand)
                    }
                }
            }
        }

        with(componentStage.scrollpaneContent) {
            setOnMousePressed {
                componentOnMousePressed(it, null) // when no selectable node is found -> call the handler with null to deselect everything
            }
        }
    }

    fun containsNode(node: SelectableNode): Boolean {
        return selectedObjects.containsKey(node)
    }

    fun registerSelectableNode(selectableNode: SelectableNode) {
        val node = selectableNode.getNode()
        registerSelectableNodeForEventNode(node, selectableNode)
    }
    fun registerSelectableNodeForEventNode(eventNode: Node, selectableNode: SelectableNode) {
        eventNode.setOnMousePressed { componentOnMousePressed(it, selectableNode) }
        eventNode.setOnMouseReleased { componentOnMouseReleased(it, selectableNode) }
        eventNode.setOnDragDetected { componentOnDragDetected(it, selectableNode) }
        eventNode.setOnMouseDragged{ componentOnMouseDragged(it, selectableNode) }
    }

    private fun componentOnMousePressed(it: MouseEvent, target: SelectableNode?) {
        if (isMyModeActive()) {
            // if the mouse is pressed down
            val mouseDownWithModifier =
                it.isPrimaryButtonDown && ((!InteroperabilityHelper.isMacOS() && it.isControlDown) || (InteroperabilityHelper.isMacOS() && it.isMetaDown))
            if (mouseDownWithModifier) {
                if (target != null) {
                    toggleNode((target))
                    it.consume()
                }
            } else if (it.isPrimaryButtonDown) {
                // - if only the primary mouse button is pressed

                // - if somewhere between the event target and the content is a selectable node
                if (target != null) {

                    // - if selectable node is not already selected
                    if (!containsNode(target)) {
                        // - clear the selection
                        deselectAll()
                        // - add the selectable node
                        selectNode(target)
                    }

                    // - the position of all selected nodes will be saved (starting location)
                    selectedObjects.forEach {
                        if (it.key is MovableNode) it.value!!.updateStartPosition()
                    }

                    // - and the current mouse location is saved as the start location
                    dragData.startX = it.sceneX
                    dragData.startY = it.sceneY
                    it.consume()
                } else {
                    // - else deselect everything
                    deselectAll()
                }
            }
        }
    }
    private fun componentOnMouseReleased(it: MouseEvent, target: SelectableNode?) {
        if (isMyModeActive()) {
            // - if dragging active
            if (dragging) {
                // - disable dragging
                dragging = false
                // - commit movement for every selected object
                val componentsWithPosition = mutableListOf<MoveComponentsCommand.ComponentWithPosition>()
                movableObjects.forEach {
                    with(it.value) {
                        componentsWithPosition.add(
                            MoveComponentsCommand.ComponentWithPosition(
                                component = node,
                                startX = startLayoutX,
                                startY = startLayoutY,
                                destinationX = node.getPositionX(),
                                destinationY = node.getPositionY()
                            )
                        )
                    }
                }
                controller.commandManager.execute(
                    MoveComponentsCommand(
                        componentsWithPosition = componentsWithPosition
                    )
                )

                componentStage.calculateBufferSize(true) // now allow to shrink
            }
        }
    }
    private fun componentOnDragDetected(it: MouseEvent, target: SelectableNode?) {
        if (isMyModeActive() && it.isPrimaryButtonDown) {
            if (selectedObjects.size > 0) {
                dragging = true
                it.consume()
            }
        }
    }
    private fun componentOnMouseDragged(it: MouseEvent, target: SelectableNode?) {
        // - if dragging active
        if (isMyModeActive() && dragging) {
            val sc = componentStage.getScaleFactor()
            val event = it

            // - move every selected objects (visually)
            movableObjects.forEach {
                it.key.moveTo(
                    x = ((event.sceneX - dragData.startX) * sc + it.value.startLayoutX),
                    y = ((event.sceneY - dragData.startY) * sc + it.value.startLayoutY)
                )
            }
            it.consume()
        }
    }
}

class AddComponentHandler(controller: ComponentStageViewController, componentStage: ComponentStage) :
    ModeHandler(controller, componentStage, Mode.ADD_COMPONENT) {
    override fun activate() {

    }

    override fun deactivate() {

    }

    fun isDragboardHoldingComponentClass(dragboard: Dragboard) = dragboard.hasContent(COMPONENT_CLASS_DATAFORMAT)

    init {
        with (componentStage.scrollpaneContent) {
            setOnDragOver {
                if (isDragboardHoldingComponentClass(it.dragboard)) {
                    it.acceptTransferModes(TransferMode.COPY)
                }
            }
            setOnDragDropped {
                val content = it.dragboard.getContent(COMPONENT_CLASS_DATAFORMAT)
                @Suppress("UNCHECKED_CAST") val componentClass = content as Class<Component>
                val local = componentStage.componentLayer.sceneToLocal(it.sceneX, it.sceneY)
                val addComponentCommand = AddComponentCommand(
                    controller.scope,
                    componentClass.kotlin,
                    Point2D(local.x, local.y)
                )
                controller.commandManager.execute(addComponentCommand)
            }
        }
    }
}

class ConnectPortsHandler(controller: ComponentStageViewController, componentStage: ComponentStage) :
    ModeHandler(controller, componentStage, Mode.CONNECT_PORTS) {
    override fun activate() {
        componentStage.addTemporaryLinkFragment(line)
        // activate focus to receive key events
        componentStage.scrollpaneContent.requestFocus()
    }

    override fun deactivate() {
        firstPort?.markConnactability(null)
        secondPort?.markConnactability(null)

        firstPort = null
        secondPort = null

        componentStage.removeTemporaryLinkFragment(line)
    }

    var firstPort: PortNode? = null
    var secondPort: PortNode? = null
    val line: Line = Line()

    fun registerPortNode(portNode: PortNode) {
        val node = portNode.getNode()

        node.setOnMouseMoved { portOnMouseMoved(it, portNode) }
        node.setOnMouseClicked { portOnMouseClicked(it, portNode) }
        node.setOnMouseEntered { portOnMouseEntered(it, portNode) }
        node.setOnMouseExited { portOnMouseExited(it, portNode) }
    }

    fun portOnMouseMoved(it: MouseEvent, target: PortNode) {
        if (isMyModeActive()) {
            // consume event in order to let the mouse enter set the position.
            it.consume()
        }
    }
    fun portOnMouseClicked(it: MouseEvent, target: PortNode) {
        if (it.button == MouseButton.PRIMARY) {
            if (isMyModeActive()) {
                secondPort = target
                val link = Link(firstPort!!.getPort(), secondPort!!.getPort())
                try {
                    controller.scope.project.simulationManager.validateLinkConnectability(link)
                    controller.commandManager.execute(ConnectPortsCommand(controller.scope, link))
                    controller.revertMode()
                } catch (ex: Exception) {
                    target.markConnactability(false)
                    ex.printStackTrace()
                }

            } else {
                val position = JavaFXHelper.getMiddlePositionOfNodeInsideComponentStage(
                    target.getConnectorNode() as Parent,
                    componentStage
                )
                line.startX = position.x
                line.startY = position.y
                line.endX = position.x
                line.endY = position.y
                firstPort = target
                controller.changeMode(Mode.CONNECT_PORTS)
            }
        }
    }

    fun portOnMouseEntered(it: MouseEvent, target: PortNode?) {
        if (isMyModeActive()) {
            if (target != null) {
                secondPort = target

                val position = JavaFXHelper.getMiddlePositionOfNodeInsideComponentStage(target.getConnectorNode() as Parent, componentStage)

                line.endX = position.x
                line.endY = position.y

                val link = Link(firstPort!!.getPort(), secondPort!!.getPort())

                try {
                    controller.scope.project.simulationManager.validateLinkConnectability(link)
                    target.markConnactability(true)
                } catch (ex: Exception) {
                    target.markConnactability(false)
                }
            }
        }
    }
    fun portOnMouseExited(it: MouseEvent, target: PortNode?) {
        if (isMyModeActive()) {
            if (target != null) {
                target.markConnactability(null)
            }
        }
    }

    init {
        with(componentStage.scrollpaneContent) {
            setOnMouseMoved {
                if (isMyModeActive()) {

                    val x = it.x * componentStage.getScaleFactor()
                    val y = it.y * componentStage.getScaleFactor()

                    line.endX = clipLeft(x)
                    line.endY = clipTop(y)
                }
            }
        }

        runLater {
            componentStage.primaryStage.scene.addEventHandler(KeyEvent.KEY_PRESSED) {
                if (it.code == KeyCode.ESCAPE && isMyModeActive()) {
                    controller.revertMode()
                }
            }
        }
    }
}

enum class Mode {
    ADD_COMPONENT, SELECT_AND_DRAG, CONNECT_PORTS
}

class DragData(
    var startX: Double = 0.0,
    var startY: Double = 0.0
)
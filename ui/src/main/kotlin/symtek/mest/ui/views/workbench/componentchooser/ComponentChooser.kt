package symtek.mest.ui.views.workbench.componentchooser

import javafx.scene.control.TreeItem
import javafx.scene.input.ClipboardContent
import javafx.scene.input.DataFormat
import javafx.scene.input.TransferMode
import symtek.mest.api.component.Component
import symtek.mest.api.component.examples.LogicAndComponent
import symtek.mest.api.component.examples.LogicOrComponent
import symtek.mest.ui.scope.ProjectScope
import tornadofx.*
import kotlin.reflect.KClass

val treeContent = PackageTreeItem("Components",
    listOf(
        ComponentTreeItem(LogicAndComponent::class),
        ComponentTreeItem(LogicOrComponent::class)
    )
)

val COMPONENT_CLASS_DATAFORMAT = DataFormat("COMPONENT_CLASS")

class ComponentTreeFragment : Fragment() {

    override val scope = super.scope as ProjectScope

    override val root = treeview<CommonTreeItem> {
        root = TreeItem(treeContent)
        root.isExpanded = true
        cellFormat {
            text = it.getName()
            setOnDragDetected {mouseEvent ->
                if (it is ComponentTreeItem<*>) {
                    val dragboard = this.startDragAndDrop(TransferMode.COPY)
                    val clipboardContent = ClipboardContent()
                    clipboardContent.put(COMPONENT_CLASS_DATAFORMAT, it.componentClass.java)
                    dragboard.setContent(clipboardContent)
                    mouseEvent.consume()
                }
            }
        }

        populate {
            when (it.value) {
                is PackageTreeItem -> (it.value as PackageTreeItem).children
                else -> emptyList()
            }
        }
    }
}

interface CommonTreeItem {
    fun getName(): String
}

data class PackageTreeItem(val packageName: String, val children: List<CommonTreeItem>) : CommonTreeItem {
    override fun getName(): String = packageName
}
data class ComponentTreeItem<T : Component>(val componentClass: KClass<T>) : CommonTreeItem {
    override fun getName(): String = componentClass.simpleName!!
}
package symtek.mest.ui.views.workbench.componentstage

import javafx.geometry.Insets
import javafx.geometry.Point2D
import javafx.geometry.Pos
import javafx.scene.Group
import javafx.scene.Node
import javafx.scene.control.ScrollPane
import javafx.scene.input.MouseEvent
import javafx.scene.layout.Pane
import javafx.scene.layout.StackPane
import javafx.scene.shape.Line
import javafx.scene.shape.Rectangle
import symtek.mest.api.component.Component
import symtek.mest.api.simulation.ComponentCandidate
import symtek.mest.api.component.CommonPort
import symtek.mest.api.component.Link
import symtek.mest.ui.*
import symtek.mest.ui.commands.CommandManager
import symtek.mest.ui.scope.ProjectScope
import symtek.mest.ui.views.workbench.SimulationController
import symtek.mest.ui.views.workbench.componentstage.fragments.ComponentFragment
import symtek.mest.ui.views.workbench.componentstage.fragments.LinkFragment
import symtek.mest.ui.views.workbench.componentstage.fragments.PortDirection
import symtek.mest.ui.views.workbench.componentstage.fragments.PortFragment
import tornadofx.*



val COMPONENT_STAGE_MARGIN = Insets(
    50.0, // top
    300.0, // right
    300.0, // bottom
    50.0) // left

fun clipLeft(x: Double) = if (x < COMPONENT_STAGE_MARGIN.left) COMPONENT_STAGE_MARGIN.left  else x
fun clipTop(y: Double) = if (y < COMPONENT_STAGE_MARGIN.top) COMPONENT_STAGE_MARGIN.top  else y


/**
 * The [ComponentStage] is a fragment that can be used to represent a [ScrollPane] that contains [symtek.mest.api.component.Component]s.
 *
 * The basic structure of the [ComponentStage] is as follows:
 *
 * - [ComponentStage] ([ScrollPane])
 *   - [scrollpaneContent] ([StackPane])
 *     - [zoomGroup] ([javafx.scene.Group])
 *       - [zoomTarget] ([javafx.scene.Group])
 *         - [layers] ([StackPane])
 *           - [buffer] ([Rectangle]) > This node will supply a padding to all sides. This way, a component is never directly at an edge.
 *           - [componentLayer] ([Group]) > This will hold the componentContainer and will be used to calculate the size of the buffer.
 *             - [componentContainer] ([Pane]) > This will hold all the nodes for [symtek.mest.symtek.mest.api.component.Component]s.
 *           -
 */
class ComponentStage : View() {

    override val scope = super.scope as ProjectScope

    var zoomTarget: Node by singleAssign()
    val zoomGroup = group()

    val layers = stackpane { alignment = Pos.TOP_LEFT }
    private val buffer = Rectangle()
    var componentLayer: Group by singleAssign()
    val portConnectionLayer: Pane

    private val componentContainer: Pane = pane()

    override val root: ScrollPane

    val scrollpaneContent = stackpane()

    val simulationController: SimulationController = find(scope)
    private val viewControllerParams = ComponentStageViewController::componentStage.name to this
    val viewController : ComponentStageViewController
    private val zoomControllerParams = ComponentStageZoomController::componentStage.name to this
    val zoomController : ComponentStageZoomController

    private val commandManager = find<CommandManager>()

    fun getScaleFactor() = 1 / zoomTarget.scaleX

    private val minimumContentSize = Point2D(800.0, 600.0)

    init {
        buffer.addClass(MestStyles.backgroundPanel)

        componentLayer = group {
            this += componentContainer
        }
        portConnectionLayer = pane {
            isPickOnBounds = false
        }

        zoomTarget = group {
            this += layers
            with(layers) {
                this += buffer
                this += componentLayer
                this += portConnectionLayer
            }
        }

        root = scrollpane {
            hbarPolicy = ScrollPane.ScrollBarPolicy.ALWAYS
            vbarPolicy = ScrollPane.ScrollBarPolicy.ALWAYS

            isPannable = true

            content = scrollpaneContent.apply {
                alignment = Pos.TOP_LEFT
                addEventHandler(MouseEvent.ANY) {
                    if (!it.isSecondaryButtonDown) {
                        it.consume()
                    }
                }
                this += zoomGroup.apply {
                    this += zoomTarget
                }
            }
            boundsInLocalProperty().addListener(ChangeListener { observable, oldValue, newValue ->
                calculateBufferSize(true)
            })
        }

        with(componentLayer){
            layoutBoundsProperty().addListener { _, _, t1 ->
                // make the buffer bigger as the componentContainer group to keep some space around objects.
                calculateBufferSize()
            }
        }

        viewController = find(scope, viewControllerParams)
        zoomController = find(scope, zoomControllerParams)

        runLater { createUIFromProject() }
    }

    fun calculateBufferSize(allowToShrink: Boolean = false) {
        with(buffer) {
            val containerGroupBounds = componentLayer.layoutBounds

            val prevWidth = width
            val prevHeight = height

            // calculate the new height and width including padding
            var newWidth = COMPONENT_STAGE_MARGIN.left + containerGroupBounds.width +  COMPONENT_STAGE_MARGIN.right
            var newHeight = COMPONENT_STAGE_MARGIN.top + containerGroupBounds.height + COMPONENT_STAGE_MARGIN.bottom

            // if the new width is smaller than the minimum size, make it as big as the minimum size
            if (newWidth < minimumContentSize.x) {
                newWidth = minimumContentSize.x
            }
            if (newHeight < minimumContentSize.y) {
                newHeight = minimumContentSize.y
            }

            // get the size of the viewport (the thing you can see)
            val viewportWidth = root.viewportBounds.width * getScaleFactor()
            val viewportHeight = root.viewportBounds.height * getScaleFactor()

            if (!allowToShrink) {
                // if the buffer is not allowed to shrink (when still moving things around), do not make the buffer smaller than it was before.
                // This way, there will be no strange movement.
                newWidth = if (newWidth < prevWidth) prevWidth else newWidth
                newHeight = if (newHeight < prevHeight) prevHeight else newHeight
            } else {
                val scrollpaneContentWidth = scrollpaneContent.width * getScaleFactor()
                val scrollpaneContentHeight = scrollpaneContent.height * getScaleFactor()


                /* If the buffer is allowed to shrink (mouse released), calculate the currently visible position at the bottom right.
                 This can be done by gathering the width/height of the whole scrollpane content,
                 then subtract the width/height of the viewport.
                 The subtraction is needed, because we want to use the hvalue/vvalue (that is between 0 and 1).
                 After that, the viewport height/width can be readded again, because we want the full height/width.
                 */
                val heightUntilViewportRight = ((scrollpaneContentWidth - viewportWidth) * root.hvalue) + viewportWidth
                val heightUntilViewportBottom = ((scrollpaneContentHeight - viewportHeight) * root.vvalue) + viewportHeight

                // if the new width is smaller than the currently visible bottom right position, use the bottom right position instead.
                if (newWidth < heightUntilViewportRight) {
                    newWidth = heightUntilViewportRight
                    root.hvalue = 1.0
                }
                if (newHeight < heightUntilViewportBottom) {
                    newHeight = heightUntilViewportBottom
                    root.vvalue = 1.0
                }
            }

            // if smaller than the visible space, make it as big as the viewport.
            if (newWidth < (viewportWidth - 1)) {
                newWidth = viewportWidth - 1
                root.hvalue = 0.0
            }
            if (newHeight < (viewportHeight - 1)) {
                newHeight = viewportHeight -1
                root.vvalue = 0.0
            }

            width = newWidth
            height = newHeight
        }
    }

    fun createComponentFragment(componentCandidate: ComponentCandidate<*>) : ComponentFragment {
        val params = ComponentFragment::component.name to componentCandidate
        return find(scope, params)
    }

    fun createPortFragment(port: CommonPort<*>, direction: PortDirection) : PortFragment {
        val param1 = PortFragment::portObject.name to port
        val param2 = PortFragment::direction.name to direction
        return find(scope, param1, param2)
    }

    fun createLinkFragment(link: Link) : LinkFragment {
        val params = LinkFragment::link.name to link
        return find(scope, params)
    }

    fun addComponentFragment(componentFragment: ComponentFragment) {
        componentContainer.children.add(componentFragment.root)
        viewController.selectAndDragHandler.registerSelectableNode(componentFragment)
        componentFragment.portFragments.forEach {
            viewController.connectPortHandler.registerPortNode(it)
        }
    }
    fun removeComponentFragment(componentFragment: ComponentFragment) {
        componentContainer.children.remove(componentFragment.root)
    }

    fun addLinkFragment(linkFragment: LinkFragment) {
        portConnectionLayer.children.add(linkFragment.root)
        linkFragment.lines.forEach {
            viewController.selectAndDragHandler.registerSelectableNodeForEventNode(it,linkFragment)
        }
        runLater { linkFragment.activate() } // update the position after the UI has been generated
    }
    fun removeLinkFragment(linkFragment: LinkFragment) {
        portConnectionLayer.children.remove(linkFragment.root)
        linkFragment.deactivate()
    }

    fun addTemporaryLinkFragment(line: Line) {
        portConnectionLayer.children.add(line)
    }
    fun removeTemporaryLinkFragment(line: Line) {
        portConnectionLayer.children.remove(line)
    }

    fun findPortFragment(port: CommonPort<*>): PortFragment {
        val componentRoot = findComponentFragment(port.component).root
        return findById(componentRoot, port.id).uiComponent()!!
    }
    fun findComponentFragment(component: Component): ComponentFragment {
        return findById(componentContainer, component.name).uiComponent()!!
    }
    fun findComponentFragment(componentCandidate: ComponentCandidate<*>): ComponentFragment {
        return findById(componentContainer, componentCandidate.id).uiComponent()!!
    }
    fun findLinkFragment(link: Link) : LinkFragment {
        return findById(portConnectionLayer, link.id).uiComponent()!!
    }
    fun findById(from: Node, id: String) : Node {
        return from.lookup("#$id")
    }

    fun createUIFromProject() {
        val schemaProject = scope.schemaProject ?: return

        val simulationManager = scope.project.simulationManager
        schemaProject.components.forEach { schemaComponent ->
            val componentCandidate = simulationManager.componentCandidates.find { it.id == schemaComponent.id }!!
            val componentFragment = createComponentFragment(componentCandidate)
            addComponentFragment(componentFragment)
            componentFragment.moveTo(schemaComponent.position.x, schemaComponent.position.y)
            componentFragment.hideUnusedPorts = schemaComponent.minimized
        }

        simulationManager.getLinkCandidates().forEach {
            val linkFragment = createLinkFragment(link = it)
            runLater { addLinkFragment(linkFragment) }
        }
    }
}



package symtek.mest.ui.views.workbench

import javafx.beans.binding.Bindings
import javafx.geometry.Pos
import javafx.scene.image.ImageView
import javafx.stage.Modality
import symtek.mest.api.simulation.runtime.State
import symtek.mest.ui.commands.CommandManager
import symtek.mest.ui.events.SchedulerStateChanged
import symtek.mest.ui.scope.ProjectScope
import symtek.mest.ui.views.workbench.simulationconfiguration.SimulationConfigurationController
import symtek.mest.ui.views.workbench.simulationconfiguration.SimulationConfigurationView
import tornadofx.*

class Toolbar() : View() {
    override val scope = super.scope as ProjectScope
    val commandManager = find<CommandManager>(scope)
    val simulationController = find<SimulationController>(scope)
    val simulationConfigurationController = find<SimulationConfigurationController>(scope)

    val toolbarIconHeight = 16.0
    fun createImageView(url: String): ImageView {
        return imageview(url) {
            isPreserveRatio = true
            fitHeight = toolbarIconHeight
        }
    }
    override val root = toolbar {



        button {
            tooltip("Undo")
            graphic = createImageView("icons/baseline_undo_black_18dp.png")
            action {
                commandManager.undo()
            }
            disableProperty().bind(Bindings.size(commandManager.commandHistory).lessThanOrEqualTo(0))
        }


        button {
            tooltip("Redo")
            graphic = createImageView("icons/baseline_redo_black_18dp.png")
            action {
                commandManager.redo()
            }
            disableProperty().bind(Bindings.size(commandManager.redoHistory).lessThanOrEqualTo(0))
        }
        button {
            text = "Save"
            action {
                scope.save()
            }
        }

        separator()

        hbox {
            text("Simulation Profile") {
                alignment = Pos.CENTER
            }
            combobox(
                property = simulationConfigurationController.activeProfileProperty,
                values = simulationConfigurationController.profilesProperty
            ) {
                cellFormat { simulationProfile ->
                    textProperty().bind(simulationProfile.name)
                }
            }
            button("Edit") {
                action {
                    find<SimulationConfigurationView>(scope).openWindow(modality = Modality.WINDOW_MODAL)
                }
            }

            button {
                val playImage = createImageView("icons/baseline_play_arrow_black_18dp.png")
                val replayImage = createImageView("icons/baseline_replay_black_18dp.png")
                //text = "Start Simulation"
                graphic = playImage
                action {
                    val scheduler = scope.simulationScheduler
                    if (scheduler != null) {
                        if (scheduler.stateMachine.state.isChangePossible(State.STOPPED)) {
                            scheduler.stop()
                        }
                    }

                    val s2 = simulationController.createSimulationScheduler()
                    s2.run()
                }
                subscribe<SchedulerStateChanged> {
                    if (it.newState.isFinal()) {
                        //text = "Start Simulation"
                        graphic = playImage
                    } else {
                        //text = "Restart Simulation"
                        graphic = replayImage
                    }
                }
            }
            button {
                val playImage = createImageView("icons/baseline_play_arrow_black_18dp.png")
                val pauseImage = createImageView("icons/baseline_pause_black_18dp.png")

                fun mode(state: State, pauseBlock: () -> Unit, resumeBlock: () -> Unit, elseBlock: () -> Unit = {}) {
                    if (state == State.RUNNING) pauseBlock.invoke()
                    else if (state == State.PAUSED) resumeBlock.invoke()
                    else elseBlock.invoke()
                }
                //text = "Pause"
                graphic = pauseImage
                isDisable = true
                action {
                    val scheduler = scope.simulationScheduler
                    if (scheduler != null) {
                        mode(scheduler.stateMachine.state, pauseBlock = {
                            scheduler.pause()
                        }, resumeBlock = {
                            scheduler.resume()
                        })
                    }
                }
                subscribe<SchedulerStateChanged> {
                    mode(it.newState, pauseBlock = {
                        isDisable = !it.newState.isChangePossible(State.PAUSED)
                        //text = "Pause"
                        graphic = pauseImage
                    }, resumeBlock = {
                        isDisable = !it.newState.isChangePossible(State.RUNNING)
                        //text = "Resume"
                        graphic = playImage
                    }, elseBlock = {
                        isDisable = true
                        //text = "Pause"
                        graphic = pauseImage
                    })
                }
            }
            button {
                graphic = createImageView("icons/baseline_step_black_18dp.png")
                isDisable = true
                action {
                    scope.simulationScheduler?.step()
                }
                subscribe<SchedulerStateChanged> {
                    isDisable = !it.newState.isChangePossible(State.RUNNING)
                }
            }
            button {
                //text = "Stop"
                graphic = createImageView("icons/baseline_stop_black_18dp.png")
                isDisable = true
                action {
                    scope.simulationScheduler?.stop()
                }
                subscribe<SchedulerStateChanged> {
                    isDisable = !it.newState.isChangePossible(State.STOPPED)
                }
            }
        }
    }
}
package symtek.mest.ui.views.workbench.simulationconfiguration

import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import symtek.mest.ui.models.simulation.SimulationProfile
import tornadofx.Controller
import tornadofx.asObservable

class SimulationConfigurationController : Controller() {
    val defaultSimulationProfile =
        SimulationProfile(SimpleStringProperty("default"))

    val profilesProperty = mutableListOf(defaultSimulationProfile).asObservable()
    val activeProfileProperty = SimpleObjectProperty(defaultSimulationProfile)

    fun addProfile() {
        val name = "New Profile "
        var index = 1

        while (profilesProperty.count { it.name.value == "$name$index" } != 0) {
            index += 1
        }

        profilesProperty.add(
            SimulationProfile(
                name = SimpleStringProperty(
                    "$name$index"
                )
            )
        )
    }

    fun removeProfilte(profile: SimulationProfile) {
        if (profile != defaultSimulationProfile) {
            if (profile == activeProfileProperty.value) {
                activeProfileProperty.set(defaultSimulationProfile)
            }
            profilesProperty.remove(profile)
        }
    }
}
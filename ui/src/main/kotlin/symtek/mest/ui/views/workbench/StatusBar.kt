package symtek.mest.ui.views.workbench

import javafx.beans.property.SimpleObjectProperty
import symtek.mest.api.simulation.runtime.State
import symtek.mest.ui.commands.CommandExecuted
import symtek.mest.ui.events.BaseEvent
import symtek.mest.ui.events.SchedulerCreated
import symtek.mest.ui.events.SchedulerStateChanged
import symtek.mest.ui.scope.ProjectScope
import tornadofx.*
import tornadofx.controlsfx.statusbar

data class EventItem(val text: String)

data class SetLastEventItem(override var eventScope: ProjectScope?, val eventItem: EventItem) : BaseEvent()
data class RemoveLastEventItem(override var eventScope: ProjectScope?, val eventItem: EventItem) : BaseEvent()


class StatusBar : View() {
    override val scope = super.scope as ProjectScope

    val progressbar = progressbar {
        progress = 0.0
        isDisable = true
        subscribe<SchedulerStateChanged> {
            if (it.newState == State.PAUSED){
                isDisable = true
                progress = 0.0
            } else {
                if (it.newState.isFinal()) {
                    isDisable = true
                    progress = 0.0
                } else {
                    isDisable = false
                    progress = -1.0
                }
            }
        }
    }
    val progressBarText = text {
        subscribe<SchedulerStateChanged> {
            text = it.newState.toString()
        }
    }

    val progressStackPane = stackpane {
        this.children.addAll(progressbar, progressBarText)
    }

    override val root = statusbar {
        text = ""
    }

    val lastEventItemProperty = SimpleObjectProperty<EventItem>()
    var lastEventItem by lastEventItemProperty

    fun setLastEvent(eventItem: EventItem) {
        this.lastEventItem = eventItem
    }
    fun removeLastEvent(eventItem: EventItem) {
        if (this.lastEventItem === eventItem) {
            this.lastEventItem = EventItem("")
        }
    }

    // build UI
    init {
        lastEventItemProperty.onChange {
            root.text = it?.text
        }
        root.rightItems.add(progressStackPane)
    }

    // register event handler
    init {
        // general
        subscribe<SetLastEventItem> {
            setLastEvent(it.eventItem)
        }
        subscribe<RemoveLastEventItem> {
            removeLastEvent(it.eventItem)
        }

        // specific
        subscribe<SchedulerStateChanged> {
            setLastEvent(EventItem("State changed to ${it.newState}"))
        }
        subscribe<SchedulerCreated> {
            setLastEvent(EventItem("New scheduler created"))
        }
        subscribe<CommandExecuted> {
            setLastEvent(EventItem("${it.action}: ${it.command.name}"))
        }
    }
}
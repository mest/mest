package symtek.mest.ui.views.workbench

import javafx.geometry.Orientation
import symtek.mest.ui.views.workbench.componentchooser.ComponentTreeFragment
import symtek.mest.ui.views.workbench.componentstage.ComponentStage
import symtek.mest.ui.events.ModelToUIScopeEventRouter
import symtek.mest.ui.scope.ProjectScope
import tornadofx.*

class Workbench : View(title = "Simulation Workbench") {

    override val scope = super.scope as ProjectScope
    val modelToUIScopeEventRouter: ModelToUIScopeEventRouter = find(scope)

    override val root = borderpane {
        top = find<Toolbar>(scope).root

        center = splitpane {
            orientation = Orientation.HORIZONTAL
            this += borderpane {
                center = find<ComponentTreeFragment>(scope).root
            }
            this += borderpane {
                center = find<ComponentStage>(scope).root
            }
            setDividerPositions(0.2, 0.4)
        }
        bottom = find<StatusBar>(scope).root
    }


}
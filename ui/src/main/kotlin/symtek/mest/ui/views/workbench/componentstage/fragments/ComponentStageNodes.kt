package symtek.mest.ui.views.workbench.componentstage.fragments

import javafx.scene.Node
import symtek.mest.ui.views.workbench.componentstage.clipLeft
import symtek.mest.ui.views.workbench.componentstage.clipTop

interface SelectableNode {
    fun getNode() : Node
    fun select()
    fun deselect()
}

interface MovableNode : SelectableNode {
    fun moveTo(x: Double, y: Double) {
        val node = getNode()
        node.translateX = clipLeft(x)
        node.translateY = clipTop(y)
    }
    fun getPositionX() = getNode().translateX
    fun getPositionY() = getNode().translateY
}
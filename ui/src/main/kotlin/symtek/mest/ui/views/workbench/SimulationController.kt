package symtek.mest.ui.views.workbench

import symtek.mest.api.component.Component
import symtek.mest.api.simulation.*
import symtek.mest.api.simulation.runtime.Scheduler
import symtek.mest.api.simulation.runtime.SchedulerConfiguration
import symtek.mest.ui.events.*
import symtek.mest.ui.scope.ProjectScope
import symtek.mest.ui.views.workbench.simulationconfiguration.SimulationConfigurationController
import tornadofx.*
import kotlin.reflect.KClass


class SimulationController : Controller() {
    override val scope = super.scope as ProjectScope
    val simulationManager: SimulationManager = scope.project.simulationManager
    val simulationConfigurationController = find<SimulationConfigurationController>(scope)

    init {
        // handle scheduler state queries
        subscribe<SchedulerStateQuery> { fire(SchedulerStateResponse(scope, it.caller, scope.simulationScheduler?.stateMachine?.state)) }
    }

    fun createSimulationScheduler() : Scheduler {
        val config = simulationConfigurationController.activeProfileProperty.value.compileProfile()
        val scheduler = scope.project.simulationManager.createSimulationScheduler(config)
        scope.simulationScheduler = scheduler
        fire(SchedulerCreated(eventScope = scope, scheduler = scheduler))
        fire(SchedulerStateChanged(scope, scheduler.stateMachine.state, scheduler.stateMachine.state))
        return scheduler
    }

    fun <T: Component> createComponentCandidate(componentClass: KClass<T>) : ComponentCandidate<T> {
        val name = "Component "
        var index = 1
        while( simulationManager.componentCandidates.count { it.name == "$name$index" } != 0) {
            index += 1
        }
        val componentCandidate = ComponentCandidate(componentClass, "$name$index")
        return componentCandidate
    }
}
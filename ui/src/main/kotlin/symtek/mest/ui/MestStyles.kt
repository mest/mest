package symtek.mest.ui

import javafx.scene.paint.Color
import javafx.scene.paint.Paint
import javafx.scene.shape.StrokeType
import tornadofx.*

class MestStyles : Stylesheet() {
    companion object {

        private val panelColor = Color.LIGHTYELLOW

        private val unselectedColor = Color.GRAY
        private val selectedColor = Color.BLUE

        val backgroundPanel by cssclass()

        val componentFragment by cssclass()
        val componentFragmentTitle by cssclass("title")
        val componentFragmentContent by cssclass("content")

        val componentPortMinimizerButton by cssclass()
        val componentPortMinimizerButtonImageSize = 16.px

        val portFragment by cssclass()
        val portFragmentConnector by cssclass()
        val portFragmentConnectorInnerCircle by cssclass()
        val portFragmentConnectorOuterCircle by cssclass()

        val portValid by csspseudoclass()
        val portInvalid by csspseudoclass()

        val linkLine by cssclass()

        private val green = Color.GREEN
        private val red = Color.RED
        private val gray = Color.GRAY
    }

    init {
        backgroundPanel {
            fill = panelColor
        }

        componentFragment {
            borderColor = multi(box(unselectedColor))

            and(selected) {
                borderColor = multi(box(selectedColor))
            }
        }

        portFragment {
            borderColor = multi(box(Color.TRANSPARENT))
            and(portValid) {
                borderColor = multi(box(green))
            }
            and(portInvalid) {
                borderColor = multi(box(red))
            }
        }
        portFragmentConnectorInnerCircle {
        }
        portFragmentConnectorOuterCircle {
            fill = Color.TRANSPARENT
            stroke = Color.BLACK
            strokeWidth = 1.px
            strokeType = StrokeType.OUTSIDE
        }

        componentPortMinimizerButton {
            padding = box(2.0.px)
            prefHeight = componentPortMinimizerButtonImageSize + padding.top + padding.bottom
            prefWidth = componentPortMinimizerButtonImageSize + padding.left + padding.right
        }

        linkLine {
            stroke = unselectedColor
            strokeWidth = 1.25.px

            and(selected) {
                stroke = selectedColor
                strokeWidth = 1.5.px
            }
        }

    }
}

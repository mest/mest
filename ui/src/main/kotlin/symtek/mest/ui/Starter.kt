package symtek.mest.ui

import symtek.mest.api.common.ImplementationRegistry
import symtek.mest.api.simulation.runtime.SchedulerInterface
import symtek.mest.api.simulation.runtime.StateMachineInterface
import symtek.mest.ui.implementations.SchedulerInterceptor
import symtek.mest.ui.implementations.StateMachineObservableImpl
import symtek.mest.ui.scope.ProjectScope
import symtek.mest.ui.views.workbench.Workbench
import tornadofx.*
import java.io.File

class Starter : App(Workbench::class, MestStyles::class, scope = ProjectScope.fromFile(File("project.mestp"))) {
    init {
        reloadStylesheetsOnFocus()
        applyUIImplementations()
    }

    fun applyUIImplementations() {
        with(ImplementationRegistry) {
            registerImplementation(StateMachineInterface::class) {
                StateMachineObservableImpl()
            }

            registerImplementation(SchedulerInterface::class) {
                SchedulerInterceptor(it as SchedulerInterface.Arguments)
            }
        }
    }
}
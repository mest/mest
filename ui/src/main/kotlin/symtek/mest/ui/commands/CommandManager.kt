package symtek.mest.ui.commands

import symtek.mest.ui.scope.ProjectScope
import tornadofx.Controller
import tornadofx.FXEvent
import tornadofx.observableListOf

data class CommandExecuted(override val scope: ProjectScope, val command: Command, val action: CommandAction) : FXEvent()

enum class CommandAction {
    EXECUTE,
    UNDO,
    REDO
}

class CommandManager : Controller() {

    override val scope = super.scope as ProjectScope

    val commandHistory = observableListOf<Command>()
    val redoHistory = observableListOf<Command>()

    fun execute(command: Command) {
        command.execute()
        commandHistory.add(command)
        redoHistory.clear()
        fire(CommandExecuted(scope, command, CommandAction.EXECUTE))
    }

    fun undo() {
        if (commandHistory.isEmpty()) {
            return
        }
        val index = commandHistory.size-1
        val command = commandHistory[index]
        command.revert()
        commandHistory.removeAt(index)
        redoHistory.add(command)
        fire(CommandExecuted(scope, command, CommandAction.UNDO))
    }

    fun redo() {
        if (redoHistory.isEmpty()) {
            return
        }
        val index = redoHistory.size-1
        val command = redoHistory[index]
        command.execute()
        redoHistory.removeAt(index)
        commandHistory.add(command)
        fire(CommandExecuted(scope, command, CommandAction.REDO))
    }
}

abstract class Command(var name: String) {
    abstract fun execute(redo: Boolean = false)
    abstract fun revert()
}
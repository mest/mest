package symtek.mest.ui.commands

import javafx.geometry.Point2D
import symtek.mest.api.component.Component
import symtek.mest.api.simulation.ComponentCandidate
import symtek.mest.ui.util.JavaFXHelper
import symtek.mest.ui.views.workbench.componentstage.fragments.ComponentFragment
import symtek.mest.ui.views.workbench.componentstage.ComponentStage
import symtek.mest.ui.scope.ProjectScope
import tornadofx.find
import kotlin.reflect.KClass

class AddComponentCommand<T : Component>(
    projectScope: ProjectScope,
    componentClass: KClass<T>,
    val position: Point2D
) : Command("Added component '${componentClass.simpleName}'"){

    var componentFragment : ComponentFragment
    val componentCandidate : ComponentCandidate<T>
    val componentStage = find<ComponentStage>(projectScope)

    init {
        componentCandidate = componentStage.simulationController.createComponentCandidate(componentClass)
        componentFragment = componentStage.createComponentFragment(componentCandidate = componentCandidate)
    }

    override fun execute(redo: Boolean) {
        // Add the component to the Simulation Manager
        componentStage.simulationController.simulationManager.addComponentCandidate(componentCandidate)
        // Add component to the UI
        componentStage.addComponentFragment(componentFragment)
        // Move component in the UI
        val middlePosition = JavaFXHelper.getMiddlePositionOfNode(componentFragment.root)
        componentFragment.moveTo(x = position.x - middlePosition.x, y = position.y - middlePosition.y)
    }

    override fun revert() {
        // Remove the component from the Simulation Manager
        componentStage.simulationController.simulationManager.removeComponentCandidate(componentCandidate)
        // Remove the component from the UI
        componentStage.removeComponentFragment(componentFragment)
    }
}
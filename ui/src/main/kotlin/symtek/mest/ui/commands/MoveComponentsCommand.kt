package symtek.mest.ui.commands

import symtek.mest.ui.views.workbench.componentstage.fragments.MovableNode

class MoveComponentsCommand(val componentsWithPosition: List<ComponentWithPosition>) : Command("Moved ${componentsWithPosition.size} components") {

    data class ComponentWithPosition(val component: MovableNode, val startX: Double, val startY: Double, val destinationX: Double, val destinationY: Double)

    override fun revert() {
        componentsWithPosition.forEach {
            it.component as MovableNode
            it.component.moveTo(it.startX, it.startY)
        }
    }

    override fun execute(redo: Boolean) {
        componentsWithPosition.forEach {
            it.component as MovableNode
            it.component.moveTo(it.destinationX, it.destinationY)
        }
    }
}
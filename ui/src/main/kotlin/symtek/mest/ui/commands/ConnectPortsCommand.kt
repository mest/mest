package symtek.mest.ui.commands

import symtek.mest.api.component.Link
import symtek.mest.ui.views.workbench.componentstage.ComponentStage
import symtek.mest.ui.views.workbench.componentstage.fragments.LinkFragment
import symtek.mest.ui.scope.ProjectScope
import tornadofx.find

class ConnectPortsCommand(val projectScope: ProjectScope, val link: Link) : Command("Connected port '${link.portA.name}' to port '${link.portB.name}'."){

    var linkFragment: LinkFragment
    val componentStage = find<ComponentStage>(projectScope)

    init {
        linkFragment = componentStage.createLinkFragment(link)
    }

    override fun execute(redo: Boolean) {
        // Connect the Link in the Simulation Manager
        projectScope.project.simulationManager.connectLink(link)
        // Add the LinkFragment to the UI
        componentStage.addLinkFragment(linkFragment)
    }

    override fun revert() {
        // Disconnect the Link from the Simulation Manager
        projectScope.project.simulationManager.disconnectLink(link)
        // Remove the LinkFragment from the UI
        componentStage.removeLinkFragment(linkFragment)
    }

}
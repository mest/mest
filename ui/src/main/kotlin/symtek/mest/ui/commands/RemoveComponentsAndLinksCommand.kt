package symtek.mest.ui.commands

import symtek.mest.ui.views.workbench.SimulationController
import symtek.mest.ui.views.workbench.componentstage.fragments.ComponentFragment
import symtek.mest.ui.views.workbench.componentstage.ComponentStage
import symtek.mest.ui.views.workbench.componentstage.fragments.LinkFragment
import symtek.mest.ui.scope.ProjectScope
import tornadofx.find

class RemoveComponentsAndLinksCommand(val projectScope: ProjectScope, val componentFragments: Set<ComponentFragment>, val linksToBeRemoved: MutableSet<LinkFragment>) : Command("Removed ${componentFragments.size} components") {
    val componentStage = find<ComponentStage>(projectScope)
    val simulationController = find<SimulationController>(projectScope)

    init {
        componentFragments.forEach {
            it.component.initComponentInstance.links.forEach {
                linksToBeRemoved.add(componentStage.findLinkFragment(it))
            }
        }
    }

    override fun execute(redo: Boolean) {
        linksToBeRemoved.forEach {
            componentStage.removeLinkFragment(it)
            simulationController.simulationManager.disconnectLink(it.link)
        }
        componentFragments.forEach {
            componentStage.removeComponentFragment(it)
            simulationController.simulationManager.removeComponentCandidate(it.component)
        }
    }

    override fun revert() {
        componentFragments.forEach {
            simulationController.simulationManager.addComponentCandidate(it.component)
            componentStage.addComponentFragment(it)
        }
        linksToBeRemoved.forEach {
            simulationController.simulationManager.connectLink(it.link)
            componentStage.addLinkFragment(it)
        }
    }
}
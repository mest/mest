package symtek.mest.ui.implementations

import symtek.mest.api.common.defaultImplementation
import symtek.mest.api.simulation.runtime.State
import symtek.mest.api.simulation.runtime.StateMachineInterface
import symtek.mest.ui.events.EntityTypes
import symtek.mest.ui.events.SchedulerStateChanged
import symtek.mest.ui.events.fireWithUnscopedWrapper
import kotlin.properties.Delegates

class StateMachineObservableImpl(baseImpl: StateMachineInterface = defaultImplementation()) :
    StateMachineInterface {
    override val id: String = baseImpl.id
    override var state: State by Delegates.observable(baseImpl.state, { property, oldValue, newValue ->
        fireWithUnscopedWrapper(EntityTypes.SchedulerStateMachine, id, SchedulerStateChanged(eventScope = null, oldState = oldValue, newState = newValue))
    })
}
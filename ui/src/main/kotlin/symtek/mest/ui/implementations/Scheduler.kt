package symtek.mest.ui.implementations

import symtek.mest.api.common.defaultImplementation
import symtek.mest.api.simulation.runtime.SchedulerInterface
import symtek.mest.ui.events.EntityTypes
import symtek.mest.ui.events.fireWithUnscopedWrapper
import symtek.mest.ui.views.workbench.EventItem
import symtek.mest.ui.views.workbench.SetLastEventItem

class SchedulerInterceptor(
    args: SchedulerInterface.Arguments,
    val baseImpl: SchedulerInterface = defaultImplementation(args)
) : SchedulerInterface by baseImpl {
    override fun pause() {
        fireWithUnscopedWrapper(EntityTypes.Scheduler, id, SetLastEventItem(null, EventItem("Pausing Simulation...")))
        baseImpl.pause()
    }

    override fun stop() {
        fireWithUnscopedWrapper(EntityTypes.Scheduler, id, SetLastEventItem(null, EventItem("Stopping Simulation...")))
        baseImpl.stop()
    }

    override fun terminate() {
        fireWithUnscopedWrapper(EntityTypes.Scheduler, id, SetLastEventItem(null, EventItem("Forcefully stopping Simulation...")))
        baseImpl.terminate()
    }
}
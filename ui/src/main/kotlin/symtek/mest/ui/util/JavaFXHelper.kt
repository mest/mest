package symtek.mest.ui.util

import javafx.geometry.Point2D
import javafx.scene.Parent
import symtek.mest.ui.views.workbench.componentstage.ComponentStage

object JavaFXHelper {
    fun getPositionRelativeToInstance(from: Parent, to: Parent) : Point2D {
        var x = 0.0
        var y = 0.0
        var currentNode = from
        while (currentNode != to) {
            val boundsInParent = currentNode.boundsInParent
            x += boundsInParent.minX
            y += boundsInParent.minY
            currentNode = currentNode.parent
        }
        return Point2D(x, y)
    }

    fun getPositionInsideComponentStage(node: Parent, componentStage: ComponentStage) : Point2D {
        return getPositionRelativeToInstance(node, componentStage.layers)
    }

    fun getMiddlePositionOfNodeInsideComponentStage(node: Parent, componentStage: ComponentStage) : Point2D {
        val pos = getPositionRelativeToInstance(node, componentStage.layers)
        val mid = getMiddlePositionOfNode(node)
        return Point2D(pos.x + mid.x, pos.y + mid.y)
    }

    inline fun <reified T : Parent> getPositionRelativeToClass(from: Parent) : Point2D {
        var x = 0.0
        var y = 0.0
        var currentNode = from
        while (currentNode !is T) {
            val boundsInParent = currentNode.boundsInParent
            x += boundsInParent.minX
            y += boundsInParent.minY
            currentNode = currentNode.parent
        }
        return Point2D(x, y)
    }

    fun getMiddlePositionOfNode(node: Parent) : Point2D {
        return Point2D(
            (node.boundsInLocal.width)/2.0,
            (node.boundsInLocal.height)/2.0
        )
    }
}
package symtek.mest.ui.util

enum class OperatingSystem {
    Windows,
    MacOS,
    Linux,
    Other
}

object InteroperabilityHelper {
    private var os: OperatingSystem? = null

    fun getOperatingSystem(): OperatingSystem {
        if (os == null) {
            val operatingSystemName = System.getProperty("os.name")
            os = when {
                operatingSystemName.contains("Windows") -> OperatingSystem.Windows // TODO: untested
                operatingSystemName.contains("Mac OS X") -> OperatingSystem.MacOS
                operatingSystemName.contains("Linux") -> OperatingSystem.Linux // TODO: untested

                else -> OperatingSystem.Other
            }
        }

        return os!!
    }

    fun isWindows(): Boolean {
        return getOperatingSystem() == OperatingSystem.Windows
    }

    fun isMacOS(): Boolean {
        return getOperatingSystem() == OperatingSystem.MacOS
    }

    fun isLinux(): Boolean {
        return getOperatingSystem() == OperatingSystem.Linux
    }

    fun isOther(): Boolean {
        return getOperatingSystem() == OperatingSystem.Other
    }
}
package symtek.mest.ui.models.simulation

import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleLongProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import symtek.mest.api.simulation.runtime.Condition
import symtek.mest.api.simulation.runtime.SchedulerConfiguration
import symtek.mest.api.simulation.runtime.StepCondition
import symtek.mest.api.simulation.runtime.TimeCondition
import tornadofx.ItemViewModel
import java.time.Duration
import java.util.*


data class SimulationProfile(
    val name: SimpleStringProperty,
    val id: String = UUID.randomUUID().toString(),
    val stepConditionEnabled: SimpleBooleanProperty = SimpleBooleanProperty(false),
    val stepConditionSteps: SimpleLongProperty = SimpleLongProperty(0),
    val timeConditionEnabled: SimpleBooleanProperty = SimpleBooleanProperty(false),
    val timeConditionTime: SimpleObjectProperty<Duration> = SimpleObjectProperty(Duration.ZERO)
) {
    fun compileProfile(): SchedulerConfiguration {
        val conditions = mutableListOf<Condition>()
        if (stepConditionEnabled.value) {
            conditions.add(StepCondition(stepConditionSteps.value))
        }
        if (timeConditionEnabled.value) {
            conditions.add(TimeCondition(timeConditionTime.value))
        }
        return SchedulerConfiguration(conditions = conditions)
    }
}

class SimulationProfileModel : ItemViewModel<SimulationProfile>() {
    val name = bind(SimulationProfile::name)

    val stepConditionEnabled = bind(SimulationProfile::stepConditionEnabled)
    val stepConditionSteps = bind(SimulationProfile::stepConditionSteps)

    val timeConditionEnabled = bind(SimulationProfile::timeConditionEnabled)
    val timeConditionTime = bind(SimulationProfile::timeConditionTime)
}